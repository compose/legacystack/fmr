﻿# export modules
include(cmake/export-cpptools)

# export fmr
install(TARGETS ${PROJECT_NAME}
  EXPORT ${PROJECT_NAME}Targets
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

include(CMakePackageConfigHelpers)
# write a ConfigVersion file to be installed
write_basic_package_version_file(${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
  VERSION ${${PROJECT_NAME}_VERSION}
  COMPATIBILITY SameMajorVersion)

# give the prefix paths used during fmr configuration for users to help them
# find our dependencies through find_package/find_dependency
set(PREFIX_PATH "")
if (CMAKE_PREFIX_PATH)
  list(APPEND PREFIX_PATH ${CMAKE_PREFIX_PATH})
endif()
if (DEFINED ENV{CMAKE_PREFIX_PATH})
  set(ENV_PREFIX_PATH $ENV{CMAKE_PREFIX_PATH})
  string(REGEX REPLACE ":" ";" ENV_PREFIX_PATH "${ENV_PREFIX_PATH}" )
  list(APPEND PREFIX_PATH ${ENV_PREFIX_PATH})
endif()

# write a Config file to be installed
configure_package_config_file(
  "${PROJECT_SOURCE_DIR}/CMakeModules/${PROJECT_NAME}Config.cmake.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  INSTALL_DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME}/cmake
  PATH_VARS PREFIX_PATH
  )

# Select files and targets to install
install(EXPORT ${PROJECT_NAME}Targets
  FILE ${PROJECT_NAME}Targets.cmake
  NAMESPACE ${PROJECT_NAME}::
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME}/cmake)

install(FILES "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_NAME}/cmake)

# Install the lib of includes
install(DIRECTORY ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME} DESTINATION include)