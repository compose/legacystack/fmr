if( FMR_USE_MPI )
  find_package(MPI QUIET REQUIRED)
  if (MPI_FOUND)
    target_compile_definitions(fmr INTERFACE FMR_USE_MPI)
    # link fmr target to mpi libraries
    target_link_libraries(fmr INTERFACE MPI::MPI_CXX)
    if (MPI_CXX_LIBRARIES)
      list(GET MPI_CXX_LIBRARIES 0 _first_lib)
      get_filename_component(path_to_first_lib ${_first_lib} DIRECTORY)
      list(APPEND CMAKE_INSTALL_RPATH "${path_to_first_lib}")
    endif()
  endif()
endif()

