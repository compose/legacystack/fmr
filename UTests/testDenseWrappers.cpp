/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iostream>
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#include "fmr/Utils/Display.hpp"
#include "fmr/Utils/MatrixNorms.hpp"
#include "fmr/Utils/MatrixTranspose.hpp"

using namespace fmr;

template<typename DenseWrapper>
int testDenseWrapper(int);

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  using FReal = float;
  using FSize = int;
  int mpiRank = 0;

#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon(2, 0, 320);
  mpiRank = chameleon.getMpiRank();
#endif

  // Call Tests
  if (mpiRank == 0) {
      std::cout << "----------------------------------" << std::endl;
      std::cout << "[FMR] Test Dense Wrappers         " << std::endl;
      std::cout << "----------------------------------" << std::endl;
  }

  if (mpiRank == 0) {
      std::cout << "[FMR] Test BlasDenseWrapper" << std::endl;
  }
  int ret = testDenseWrapper<BlasDenseWrapper<FSize,FReal>>(mpiRank);
  if (ret != EXIT_SUCCESS){
      std::cout << "[FMR] Test BlasDenseWrapper error" << std::endl;
  }

#if defined(FMR_CHAMELEON)
  if (mpiRank == 0) {
      std::cout << "[FMR] Test ChameleonDenseWrapper" << std::endl;
  }
  ret = testDenseWrapper<ChameleonDenseWrapper<FSize,FReal>>(mpiRank);
  if (ret != EXIT_SUCCESS){
      std::cout << "[FMR] Test CHameleonDenseWrapper error" << std::endl;
  }
#endif

  return EXIT_SUCCESS;
}

template<typename DenseWrapper>
int testDenseWrapper(int mpiRank){

  using FReal = typename DenseWrapper::value_type;
  using FSize = typename DenseWrapper::int_type;
  using FMat  = typename DenseWrapper::matrix_type;

  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper constructor with size" << std::endl;
  }
  DenseWrapper mat_size(10, 10);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper setSymmetric" << std::endl;
  }
  mat_size.setSymmetric(true);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper free" << std::endl;
  }
  mat_size.free();
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper constructor with size and fill with one initial value" << std::endl;
  }
  DenseWrapper mat_size_value(10, 10, (FReal)1.0);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper constructor copy class" << std::endl;
  }
  DenseWrapper mat_copyclass(mat_size_value);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getLeadingDim" << std::endl;
  }
  std::cout << "Matrix ldd " << mat_size_value.getLeadingDim() << std::endl;
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getNbRows" << std::endl;
  }
  std::cout << "Matrix nbrows " << mat_size_value.getNbRows() << std::endl;
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getNbCols" << std::endl;
  }
  std::cout << "Matrix nbcols " << mat_size_value.getNbCols() << std::endl;
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getVal" << std::endl;
  }
  std::cout << "Matrix value (1,1) " << mat_size_value.getVal( 1, 1 ) << std::endl;
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper setVal" << std::endl;
  }
  mat_size_value.setVal( 1, 1, (FReal)0.0 );
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getID" << std::endl;
  }
  mat_size_value.getID();
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper isSymmetric" << std::endl;
  }
  mat_size_value.isSymmetric();
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getMatrix" << std::endl;
  }
  FMat* mat_pointer = mat_size_value.getMatrix();
  if (mpiRank == 0) {
      std::cout << "Matrix pointer " << mat_pointer << std::endl;
  }
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getBlasMatrix" << std::endl;
  }
  FReal* mat_blas_pointer = mat_size_value.getBlasMatrix();
  if (mpiRank == 0) {
      std::cout << "Matrix pointer blas " << mat_blas_pointer << std::endl;
  }
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper constructor with size and copy matrix" << std::endl;
  }
  FReal *matrix = new FReal[100];
  std::memset(matrix, 0, 100*sizeof(FReal));
  DenseWrapper mat_copymat(10, 10, matrix);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper copyMatrix" << std::endl;
  }
  mat_copymat.free();
  mat_copyclass.copyMatrix(matrix);
  delete [] matrix;
  std::vector<FSize> listCols { 3, 5, 7};
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper extractCols in vector" << std::endl;
  }
  std::vector<FReal> vec_columns(30);
  mat_size_value.extractCols(listCols, vec_columns);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper extractCols in dense wrapper" << std::endl;
  }
  DenseWrapper mat_columns(10, 3);
  mat_size_value.extractCols(listCols, mat_columns);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper getSubMatrix" << std::endl;
  }
  mat_copyclass.getSubMatrix(3, 3, 3, 3, mat_blas_pointer);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper setSubMatrix" << std::endl;
  }
  FReal* mat_copy_blas_pointer = mat_copyclass.getBlasMatrix();
  mat_size_value.setSubMatrix(3, 3, 3, 3, mat_copy_blas_pointer);
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper scaleCols" << std::endl;
  }
  std::vector<FReal> scaleValues { (FReal)1.0, (FReal)2.0, (FReal)3.0, (FReal)4.0, (FReal)5.0, (FReal)6.0, (FReal)7.0, (FReal)8.0, (FReal)9.0, (FReal)10.0};
  mat_size_value.scaleCols(10, scaleValues.data());
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper computeFrobenius" << std::endl;
  }
  FReal frob = computeFrobenius(mat_size_value);
  if (mpiRank == 0) {
      std::cout << "Frob = " << frob << std::endl;
  }
  if (mpiRank == 0) {
      std::cout << "Starting test " << ": DenseWrapper computeTranspose" << std::endl;
  }
  mat_size_value.reset(mat_size_value.getNbCols(), mat_size_value.getNbRows(), 0.);
  computeTranspose( mat_copyclass, mat_size_value );

  return EXIT_SUCCESS;
}