/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef RANDEVD_HPP
#define RANDEVD_HPP

#include <array>
// Utilities
#include "fmr/Utils/Tic.hpp"

#ifdef FMR_USE_MPI
#include "mpi.h"
#endif

#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/StandardLRA/EVD.hpp"

namespace fmr{
  /**
   * @brief The RandEVD class
   *
   * Perform the randomized EVD  \f$A ~ U L U^T\f$
   *
   *
   */
  template <class Real, class MatrixWrapper, class RandomizedRangeFinderClass>
  class RandEVD {
    using Size = typename MatrixWrapper::int_type;
  private:
    const Size _nbRows;
    // Range finder based on random projection
    RandomizedRangeFinderClass *_RandRangeFinder;
    std::array<Size, 2> _shapeU;
    MatrixWrapper *_ApproxU;     ///< the matrix U
    Real *_ApproxEigenValues;    ///< an array on the eigenvalues
  public:
    /*
     * Constructor
     */
    explicit RandEVD(const Size& inNbRows,
                     RandomizedRangeFinderClass *const inRandRangeFinder)
      : _nbRows(inNbRows),
        _RandRangeFinder(inRandRangeFinder), _shapeU({0, 0}), _ApproxU(nullptr),
        _ApproxEigenValues(nullptr) {}

    /*
     * Destructor
     */
    ~RandEVD() {
      if ( _ApproxU != nullptr ) {
        delete _ApproxU;
        _ApproxU = nullptr;
      }
      if ( _ApproxEigenValues != nullptr ) {
        delete[] _ApproxEigenValues;
        _ApproxEigenValues = nullptr;
      }
    }

    std::array<Size, 2> &getShapeU() { return _shapeU; }
    MatrixWrapper *getApproxU() { return _ApproxU; }
    Real *getApproxEigenValues() { return _ApproxEigenValues; }

    /**
     * @brief factorize compute a randomized EVD on symmetric matrix A (n by n)
     * for a given rank.
     * Based on Martinsson "Randomized methods for matrix computations" paper,
     * the algorithm is described page 11.
     *        * From A, n x n
     *        * find a range k and an orthogonal matrix Q (algorithm 4.2 or 4.4),
     * n x k
     *        * Compute the EVD factors of matrix C = Q^t A Q  (algorithm 5.1)
     *        * Form U = Q U_C
     *        * The Eigenvalues are then sorted by decreasing order
     * @return _prescribedRank
     */
    Size factorize(const int verbose = 0) throw() {
      tools::Tic time;
      double timee, gflops;
      int mpirank = 0;
#if defined(FMR_USE_MPI)
      int mpiinit;
      MPI_Initialized(&mpiinit);
      if (mpiinit) {
        MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
      }
#endif
      bool istimed = false;
      const char *envTime_s = getenv("FMR_TIME");
      if (envTime_s != NULL) {
        int envTime_i = std::stoi(envTime_s);
        if (envTime_i == 1) {
          istimed = true;
        }
      }
      ////////////////////////////////////////////////////////////////////
      ///
      /// Stage A
      ///     Find the range(rank) of A and the matrix Q  (n x rank)
      ///
      ////////////////////////////////////////////////////////////////////
      QRF<MatrixWrapper> *matQ = nullptr;
      const auto rank = _RandRangeFinder->findRange(matQ, verbose > 0);
      const Size computedRank = _RandRangeFinder->getRangeSize();

      if (verbose > 0) {
        std::cout << "[fmr]  rank: " << rank << " computedRank: " << computedRank
                  << std::endl;
        std::cout << "[fmr]  matQ->getNbRows(): " << matQ->getNbRows()
                  << " matQ->getNbCols(): " << matQ->getNbCols() << std::endl;
      }
      // A is the matrix given by the user on which we perform REVD
      auto &A = *(_RandRangeFinder->getMatrixWrapper());

      const Size n = matQ->getNbRows(); // should be equal to p
      const Size l = matQ->getNbCols(); // prescribed rank + oversampling

      /// The algorithm for step B
      ///   step 1 compute
      ///             C = Q^t A Q,
      ///   step 2 compute the evd of C
      ///           C = Uc L Uc^t -> EVD(Rc)
      ///   step 3 compute the final U
      ///          Ua = Q Uc

      // step 1
      MatrixWrapper AQ(n, l, Real(0.0));
      MatrixWrapper C(l, l, Real(0.0));
      MatrixWrapper &Q = matQ->getQ();


      time.tic();

      // Compute Q^t A Q
      time.tic();
      fmrGemm<Size, Real>("N", "N", 1., A, Q, 0., AQ);
      time.tac();
      timee = time.elapsed();
      if (istimed && mpirank == 0) {
        std::cout << "[fmr] TIME:REVD:GEMM(A,Q)=" << timee << " s"
                  << std::endl;
        gflops = (1e-9 * FLOPS_GEMM(n, n, l)) / timee;
        std::cout << "[fmr] PERF:REVD:GEMM(A,Q)=" << gflops << " gflop/s"
                  << std::endl;
      }
      time.tic();
      fmrGemm<Size, Real>("T", "N", 1., Q, AQ, 0., C);
      time.tac();
      timee = time.elapsed();
      if (istimed && mpirank == 0) {
        std::cout << "[fmr] TIME:REVD:GEMM(Q^t,AQ)=" << timee << " s"
                  << std::endl;
        gflops = (1e-9 * FLOPS_GEMM(l, n, l)) / timee;
        std::cout << "[fmr] PERF:REVD:GEMM(Q^t,AQ)=" << gflops << " gflop/s"
                  << std::endl;
      }
      // step 2

      _ApproxEigenValues = new Real[l];
      MatrixWrapper U_C(l, l);

      time.tic();
      EVD<Real, Size>::computeEVD(C, _ApproxEigenValues, U_C, true);
      time.tac();
      timee = time.elapsed();
      if (istimed && mpirank == 0) {
        std::cout << "[fmr] TIME:REVD:EVD(C)=" << timee << " s" << std::endl;
      }
      // step 3 compute U_A = Q U_C
      _ApproxU = new MatrixWrapper(n, l, Real(0));
      time.tic();
      fmrGemm<Size, Real>(Q, U_C, *_ApproxU);
      time.tac();
      timee = time.elapsed();
      if (istimed && mpirank == 0) {
        std::cout << "[fmr] TIME:REVD:GEMM(Q,U_C)=" << timee << " s"
                  << std::endl;
        gflops = (1e-9 * FLOPS_GEMM(n, l, l)) / timee;
        std::cout << "[fmr] PERF:REVD:GEMM(Q,U_C)=" << gflops << " gflop/s"
                  << std::endl;
      }
      _shapeU[0] = n;
      _shapeU[1] = l;

      delete matQ;
      delete &Q;

      // TODO Error estimation

      return rank;
    }

  }; /* RandEVD */
} /* namespace fmr */

#endif /* RANDEVD_HPP */
