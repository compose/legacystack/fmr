/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_RANDOMIZEDLRA_COMMONALGORITHM_HPP
#define FMR_RANDOMIZEDLRA_COMMONALGORITHM_HPP
#include <iostream>

#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/Utils/Display.hpp" // To display the matrices in verbose mode
#include "fmr/Utils/Tic.hpp"

namespace fmr {
namespace rla {

/**
 *   \brief perform the subspace Iteration Method
 *
 *    Here we perform the subspace iterations described in
 * Algo 4.4 of Halko (page 244) Only the iterations lines 3-6
 * \f$A\f$ is a  m × n matrix,  \f$Q\f$ is a  m × l orthogonal matrix
 *
 * initilization \f$Q_{0}=Q\f$
 *
 * For each iteration
 *
 *   1- build  \f$ \tilde{Y}_j= A^T Q_{j-1}\f$ and
 *
 *   2- factorize \f$\tilde{Y}_j =  \tilde{Q}_j\tilde{R}_j\f$
 *
 *   3- build \f${Y}_j = A \tilde{Q}_j\f$
 *
 *   4- factorize \f$Y_j= Q_j R_j\f$
 *
 * @param[in] q the number Of Subspace Iteration
 * @param[in]  A the  m × n matrix   (WrapperMatrix class)
 * @param[inout] Y = (A A^T) ^q Q (WrapperMatrix class)
 * @param[inout] Q the  m × l  orthogonal matrix or the
 * @param[in] formQmatrix if true we construct the orthogonal matrix
 *             otherwise Q contains the elementary projectors
 *
 */
template <typename MatrixWrapperClass, typename QRMatrixWrapperClass>
void subspaceIterationMethod(const int &q, MatrixWrapperClass &A,
                                MatrixWrapperClass &Yo, QRMatrixWrapperClass &Q,
                                const bool formQmatrix, const bool verbose) {
  bool istimed = false;
  int mpirank = 0;
  const char *envTime_s = getenv("FMR_TIME");
  if (envTime_s != NULL) {
    int envTime_i = std::stoi(envTime_s);
    if (envTime_i == 1) {
      istimed = true;
    }
  }
  tools::Tic time;
  time.tic();

  //
  //  std::clog << "   perform Optimized Subspace Interation method nbIter= " <<
  //  q << "\n";
  using int_type = typename MatrixWrapperClass::int_type;
  using value_type = typename MatrixWrapperClass::value_type;
  //
  MatrixWrapperClass *YY = new MatrixWrapperClass(Yo);
  MatrixWrapperClass &Y = *YY;
  if (verbose) {
    std::cout
        << "[fmr]    perform Optimized Subspace Interation method nbIter= " << q
        << "\n";
  }

  //  std::cout << "A shape: " << A.getNbRows() << " " << A.getNbCols()
  //            << " Q shape: " << Q.getNbRows() << " " << Q.getNbCols()
  //            << " Y shape: " << Y.getNbRows() << " " << Y.getNbCols()
  //            << std::endl;
  //const int_type &nbRows = Y.getNbRows();
  const int_type &rank = Y.getNbCols();
  const int_type &nbCols = A.getNbCols();
  //const int_type displaySize = 10;

  // loop over power iterations
  value_type one = 1.0, zero = 0.0;
  // tmpY = A
  // tildeQ = tildeY
  MatrixWrapperClass tildeY(nbCols, rank); // tilde Y
  QRMatrixWrapperClass tildeQ(tildeY);     // tildeQ.Q is a reference on tildeY
                                           // tildeY.allocate();
  tildeY.allocate();                       // Allocate the matrix if necessary
  for (int j = 0; j < q; ++j) {
    //
    // Form tildeY_{j} = A^T x Q_{j-1};
    //  We construct the orthogonal matrix Q_{j-1} from the elementary projectors
    auto &Qmat = Q.getQ();
    fmrGemm("T", "N", one, A, Qmat, zero, tildeY);
    //
    // Decompose tildeY_{j} in tildeQ_{j} tildeR_{j}
    // tildeQ contains the elementary projectors.
    // We force the factorization
    tildeQ.factorize(true);
    //
    // Form Y_{q} = A tildeQ_{j} = A A^T Q_{j-1};
    // We copy A in Y which points on Q
    // commented for now because get a bug with starpu mpi tags if configured with chameleon+mpi
    //Y.reset(A);
    // tildeQ contains the projectors faster multiplication
    //tildeQ.multiplyQ("N", Y, "R"); // Y = AQ
    auto &tildeQmat = tildeQ.getQ();
    fmrGemm("N", "N", one, A, tildeQmat, zero, Y);
    //
    // Decompose Y_{j} in Q_{j}R_{j}
    // Q.Q is an alias on Y and we delete the previous alias on Q.Q = tildeY
    Q.setMatrix(Y);
    // std::clog << " Q.factorize(Y);\n";
    Q.factorize(true);
  }
  if (formQmatrix) {
    Q.makeQ();
  }
  time.tac();
  if (istimed && mpirank == 0) {
    std::cout << "[fmr] TIME:RSVD:SubIterMethod=" << time.elapsed() << " s"
              << std::endl;
  }
  // Warning Q.Q points on Y So e shouudn't have to delete the YY pointer
  YY = nullptr;
  //  std::clog << "A shape: " << A.getNbRows() << " " << A.getNbCols()
  //            << " Q shape: " << Q.getNbRows() << " " << Q.getNbCols()
  //            << " Y shape: " << Y.getNbRows() << " " << Y.getNbCols()
  //            << std::endl;
  // std::clog << "END subspaceIterationMethodNew;\n";
}

} // namespace rla
} // namespace fmr
#endif
