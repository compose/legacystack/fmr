/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @author Florent Pruvost (florent.pruvost@inria.fr)
 * @date December 4th, 2018
 *
 */
#ifndef CHAM_DENSEWRAPPER_HPP
#define CHAM_DENSEWRAPPER_HPP

// standard includes
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdexcept>

// Utilities
#include "fmr/MatrixWrappers/MatrixWrapperTraits.hpp"
#include "fmr/Utils/Blas.hpp"
#include "fmr/Utils/Chameleon.hpp"
#include "fmr/Utils/Starpu.hpp"
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
#include "mpi.h"
#endif

namespace fmr {

/**
 * @brief Chameleon Dense Wrapper class
 *
 * The matrix is stored in a Chameleon specific structure to work on tiles that
 * can be distributed over several MPI processus.
 *
 * [TODO] Define wrapper for kernel matrices in different wrapper, eg,
 * KernelWrapper [TODO] Define subsampling in this/each wrapper?
 *
 */
template <class FSize, class FReal> class ChameleonDenseWrapper {
public:
  using value_type = FReal;
  using int_type = FSize;
  using Int_type = FSize;
  using matrix_type = CHAM_desc_t;
  using Tau_type = CHAM_desc_t *;
  using Tree_type = libhqr_tree_t *;

protected:
  // Matrix Dimensions
  FSize _nbRows;
  FSize _nbCols;
  bool _matrixIsSymmetric; // Symmetric square matrix ?
  // Matrix storage
  FReal *_matrix; // matrix as a centralized 1D array column major (standard
                  // lapack format)
  bool _isExternalArray; // true if the data comes from a user's array
  bool _isAllocatedArray; // true if the wrapper has allocated the 1D array or
                          // not
  CHAM_desc_t *_desc;     // structure to wrap the matrix by tiles
  bool _isAllocated; // true if the wrapper has allocated the descriptor or not
  std::array<FSize, 2> _shape; ///< The shape of the matrix
  Hdf5Blocks
      *_h5blocks; // struct used for handling hdf5 files and panels to read
  void *_WorkspaceCesca; // workspace used for calling the async interface
  void *_WorkspaceGram;  // workspace used for calling the async interface
  void *_WorkspaceGemm;  // workspace used for calling the async interface
  void *_WorkspaceSymm;  // workspace used for calling the async interface
  // Chameleon specific parameters
  cham_flttype_t _chamDataType; // float, double, etc
  cham_uplo_t _chamUpperLower;
  int _tileSize;       // size of blocks = granularity of tasks
  int _2dBlockCyclicP; // 2D block cyclic parameter P for tiles distribution
  int _2dBlockCyclicQ; // 2D block cyclic parameter Q for tiles distribution

private:
  FSize _nbVals;
  FReal _value;
  int _mpiRank;
  int _mpiNP;
  int _nbTileRows;
  int _nbTileCols;

public:
  /*
   *  @brief Constructor
   *
   *   Constructor to wrap a dense matrix with a chameleon descriptor. The
   *   memory is handled directly by chameleon. This can be used to copy later
   *   an array in the descriptor with an init method.
   *
   *   @param[in] inNbRows number of lines
   *   @param[in] inNbCols number of columns
   *   @param[in] inMatrixIsSymmetric if the matrix is symmetric or not (false
   * by default)
   */
  explicit ChameleonDenseWrapper(const FSize inNbRows = 0,
                                 const FSize inNbCols = 0,
                                 const bool inMatrixIsSymmetric = false)
      : _nbRows(inNbRows), _nbCols(inNbCols),
        _matrixIsSymmetric(inMatrixIsSymmetric), _matrix(nullptr),
        _isExternalArray(false),_isAllocatedArray(false), _desc(nullptr), _isAllocated(false),
        _shape({inNbRows, inNbCols}), _h5blocks(nullptr),
        _WorkspaceCesca(nullptr), _WorkspaceGram(nullptr),
        _WorkspaceGemm(nullptr), _WorkspaceSymm(nullptr),
        _chamDataType(ChamRealFloat), _chamUpperLower(ChamUpperLower),
        _tileSize(0), _2dBlockCyclicP(0), _2dBlockCyclicQ(0), _nbVals(0),
        _value(0), _mpiRank(0), _mpiNP(1), _nbTileRows(0), _nbTileCols(0) {
    this->initChameleonParameters(true);
    this->allocate();
  }
  /*
   *  @brief Constructor
   *
   *   Constructor to wrap a dense matrix with a chameleon descriptor. The
   *   memory is handled by the user as a centralized 1d array. We do not
   *   distribute it over MPI processus.
   *
   *   @param[in] inNbRows number of lines
   *   @param[in] inNbCols number of columns
   *   @param[in] inMatrix 1d array of floating points
   *
   */
  explicit ChameleonDenseWrapper(const FSize inNbRows, const FSize inNbCols,
                                 FReal *inMatrix,
                                 const bool inMatrixIsSymmetric = false)
      : _nbRows(inNbRows), _nbCols(inNbCols),
        _matrixIsSymmetric(inMatrixIsSymmetric), _matrix(inMatrix),
        _isExternalArray(true),_isAllocatedArray(false), _desc(nullptr), _isAllocated(false),
        _shape({inNbRows, inNbCols}), _h5blocks(nullptr),
        _WorkspaceCesca(nullptr), _WorkspaceGram(nullptr),
        _WorkspaceGemm(nullptr), _WorkspaceSymm(nullptr),
        _chamDataType(ChamRealFloat), _chamUpperLower(ChamUpperLower),
        _tileSize(0), _2dBlockCyclicP(0), _2dBlockCyclicQ(0), _nbVals(0),
        _value(0), _mpiRank(0), _mpiNP(1), _nbTileRows(0), _nbTileCols(0) {
    int nbprocs = 1;
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    MPI_Comm_size(MPI_COMM_WORLD, &nbprocs);
#endif
    if (nbprocs > 1) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Constructor to wrap a Blas type object (float*, "
                   "double*) is not allowed in MPI with NP > 1"
                << std::endl;
      std::cerr << "[fmr] Please use the constructor with nbRows and nbCols "
                   "and call the init function to copy your Blas array in a "
                   "distributed Chameleon object"
                << std::endl;
      exit(EXIT_FAILURE);
    }

    initChameleonParameters(false);
    // we fix the 2d block cyclic parameters to 1 to keep the matrix tiles
    // on the MPI rank 0
    _2dBlockCyclicP = 1;
    _2dBlockCyclicQ = 1;
    // create the descriptor such that it just wraps the already allocated
    // Lapack vector
    Chameleon::desc_create_user(
        &_desc, inMatrix, _chamDataType, _tileSize, _tileSize,
        _tileSize * _tileSize, _nbRows, _nbCols, 0, 0, _nbRows, _nbCols,
        _2dBlockCyclicP, _2dBlockCyclicQ, chameleon_getaddr_cm,
        chameleon_getblkldd_cm, NULL);
    _desc->styp = ChamCM;
    _isAllocated = true;
  }
  /*
   *  @brief Constructor
   *
   *   Constructor to wrap a dense matrix with a chameleon descriptor filled
   *   with a specific value. The memory is handled directly by chameleon.
   *   This can be used to copy later an array in the descriptor with an init
   *   method.
   *
   *   @param[in] inNbRows number of lines
   *   @param[in] inNbCols number of columns
   *   @param[in] inValue floating point value given to initialize all the
   * matrix
   *   @param[in] inMatrixIsSymmetric if the matrix is symmetric or
   *   not (false by default)
   *
   */
  explicit ChameleonDenseWrapper(const FSize inNbRows, const FSize inNbCols,
                                 FReal inValue,
                                 const bool inMatrixIsSymmetric = false)
      : _nbRows(inNbRows), _nbCols(inNbCols),
        _matrixIsSymmetric(inMatrixIsSymmetric), _matrix(nullptr),
        _isExternalArray(false),_isAllocatedArray(false), _desc(nullptr), _isAllocated(false),
        _shape({inNbRows, inNbCols}), _h5blocks(nullptr),
        _WorkspaceCesca(nullptr), _WorkspaceGram(nullptr),
        _WorkspaceGemm(nullptr), _WorkspaceSymm(nullptr),
        _chamDataType(ChamRealFloat), _chamUpperLower(ChamUpperLower),
        _tileSize(0), _2dBlockCyclicP(0), _2dBlockCyclicQ(0), _nbVals(0),
        _value(0), _mpiRank(0), _mpiNP(1), _nbTileRows(0), _nbTileCols(0) {
    initChameleonParameters(true);
    init(inValue);
  }
  /*
   * @brief Constructor
   *
   * Constructor by copy.
   *
   * @param[in] other a ChameleonDenseWrapper matrix to copy
   */
  explicit ChameleonDenseWrapper(const ChameleonDenseWrapper &other)
      : _nbRows(other._nbRows), _nbCols(other._nbCols),
        _matrixIsSymmetric(other._matrixIsSymmetric), _matrix(nullptr),
        _isExternalArray(false),_isAllocatedArray(false), _desc(nullptr), _isAllocated(false),
        _shape(other._shape), _h5blocks(other._h5blocks),
        _WorkspaceCesca(nullptr), _WorkspaceGram(nullptr),
        _WorkspaceGemm(nullptr), _WorkspaceSymm(nullptr),
        _chamDataType(other._chamDataType),
        _chamUpperLower(other._chamUpperLower),
        _tileSize(other._tileSize),
        _2dBlockCyclicP(other._2dBlockCyclicP),
        _2dBlockCyclicQ(other._2dBlockCyclicQ),
        _nbVals(0), _value(0), _mpiRank(other._mpiRank), _mpiNP(other._mpiNP),
        _nbTileRows(other._nbTileRows), _nbTileCols(other._nbTileCols) {
    init(other._desc);
  }

  /*
   * @brief Destructor
   */
  ~ChameleonDenseWrapper() { free(); }

  /*
   * @brief Get matrix leading dimension (see LAPACK)
   */
  FSize getLeadingDim() const {
    return _nbRows; // since chameleon uses column major convention
  }

  /*
   * @brief Get matrix number of rows
   */
  FSize getNbRows() const { return _nbRows; }

  /*
   * @brief Get matrix number of columns
   */
  FSize getNbCols() const { return _nbCols; }
  /**
   * @brief return the shape of the matrix
   */
  auto getShape() const { return _shape; }
  /*
   * @brief Get 2D block-cyclic chameleon parameters P useful for calling some
   * algorithms
   */
  FSize get2DBlockCyclicP() const { return _2dBlockCyclicP; }

  /*
   * @brief Get 2D block-cyclic chameleon parameters Q useful for calling some
   * algorithms
   */
  FSize get2DBlockCyclicQ() const { return _2dBlockCyclicQ; }

  /*
   * @brief Get Chameleon Data type, useful for calling some algorithms
   */
  cham_flttype_t getDataType() const { return _chamDataType; }

  /**
   * @brief check if the matrix is allocated
   * @return true if the matrix is allocated
   */
  bool isAllocated() const { return _isAllocated; }

private:
  bool isAsync() const {
      bool async;
      RUNTIME_sequence_t *sequence = NULL;
      RUNTIME_request_t *request = NULL;
      Chameleon::async_manager(false, false, false, &async, &sequence, &request);
      return async;
  }
public:
  /**
   * @brief allocates the matrix
   * @param[in] chamDistribNocyclic true for 1D nocyclic distribution of tiles
   *            over MPI processes
   * @param[in] h5DistribNocyclic true for specific distribution of tiles over
   *            MPI processes depending on hdf5 files
   */
  void allocate(const bool chamDistribNocyclic = false,
                const bool h5DistribNocyclic = false) {
    if (_matrixIsSymmetric && _nbRows != _nbCols) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] This matrix is declared as symmetric but nb Rows is "
                   "different from nb Cols"
                << std::endl;
      exit(EXIT_FAILURE);
    }
    _nbVals =
        _matrixIsSymmetric ? _nbRows * (_nbRows + 1) / 2 : _nbRows * _nbCols;
    _nbTileRows = _nbRows % _tileSize == 0 ? _nbRows / _tileSize
                                           : _nbRows / _tileSize + 1;
    _nbTileCols = _nbCols % _tileSize == 0 ? _nbCols / _tileSize
                                           : _nbCols / _tileSize + 1;

    int sbcr = Chameleon::mapping_sbc_r();
    int mappingP;
    int mappingQ;
    if ( _matrixIsSymmetric && sbcr > 0) {
      mappingP = sbcr;
      // mappingQ not used in sbc but needed because rank is tested against P*Q
      mappingQ = _mpiNP / sbcr;
      if (_mpiRank == 0) {
          std::cout << "[fmr] chameleon matrix distribution : sbc "
                    << mappingP << " " << mappingQ << std::endl;
      }
    } else {
      mappingP = _2dBlockCyclicP;
      mappingQ = _2dBlockCyclicQ;
      // if (_mpiRank == 0) {
      //     std::cout << "[fmr] chameleon matrix distribution : 2dcyclic "
      //               << mappingP << " " << mappingQ << std::endl;
      // }
    }

    bool async = isAsync();
    if (!_isAllocated && _nbRows * _nbCols > 0) {
      if (h5DistribNocyclic) {
#if defined(FMR_HDF5)
        Chameleon::desc_create_user(
            &_desc, CHAMELEON_MAT_ALLOC_TILE, _chamDataType, _tileSize,
            _tileSize, _tileSize * _tileSize, _nbRows, _nbCols, 0, 0, _nbRows,
            _nbCols, _2dBlockCyclicP, _2dBlockCyclicQ, NULL, NULL,
            chameleon_getrankof_h5);
        if (_mpiRank == 0) {
          std::cout << "[fmr] chameleon matrix distribution : 2dnocyclich5"
                    << "\n";
        }
#else
        std::cerr << "[fmr] h5DistribNocyclic must be set to false because "
                     "HDF5 is not enabled in the configuration (exit)."
                  << std::endl;
        exit(EXIT_FAILURE);
#endif
      } else if (chamDistribNocyclic && async) {
        Chameleon::desc_create_user(
            &_desc, CHAMELEON_MAT_ALLOC_TILE, _chamDataType, _tileSize,
            _tileSize, _tileSize * _tileSize, _nbRows, _nbCols, 0, 0, _nbRows,
            _nbCols, _2dBlockCyclicP, _2dBlockCyclicQ, NULL, NULL,
            chameleon_getrankof_2dnocyclic);
        // if (_mpiRank == 0){
        //    std::cout << "[fmr] chameleon matrix distribution : 2dnocyclic" <<
        //    "\n";
        //}
        // set to 0 to have a purely write mode first access
        Chameleon::laset(_chamUpperLower, (FReal)0., (FReal)0., _desc);
      } else {
        Chameleon::desc_create(&_desc, CHAMELEON_MAT_ALLOC_TILE, _chamDataType,
                               _tileSize, _tileSize, _tileSize * _tileSize,
                               _nbRows, _nbCols, 0, 0, _nbRows, _nbCols,
                               _2dBlockCyclicP, _2dBlockCyclicQ);
        // if (_mpiRank == 0){
        //    std::cout << "[fmr] chameleon matrix distribution : 2dcyclic" <<
        //    "\n";
        //}
        // set to 0 to have a purely write mode first access
        Chameleon::laset(_chamUpperLower, (FReal)0., (FReal)0., _desc);
      }
      // if (_mpiRank == 0){
      //    std::cout << "[fmr] chameleon is symmetric : " << _matrixIsSymmetric
      //    << "\n";
      //}
      _isAllocated = true;
    }
  }
  /**
   * @brief init and allocate the matrix with given shape
   *
   * @param[in] nbRows the number of rows
   * @param[in] nbCols the number of columns
   * @param[in] isSymmetric (not used)
   */
  void initAndAllocate(int_type nbRows, int_type nbCols,
                       bool isSymmetric = false) {
    (void)isSymmetric;
    _shape[0] = nbRows;
    _shape[1] = nbCols;
    _nbRows = nbRows;
    _nbCols = nbCols;
    this->initChameleonParameters(true);
    this->allocate();
  }
  /**
   * @brief deallocate the matrix (free the memory)
   */
  void free() {
    if (_isAllocated) {
      // we need to synchronize before to destroy the descriptor
      Chameleon::barrier();
      Chameleon::desc_destroy(&_desc);
      _isAllocated = false;
    }
    if (_isAllocatedArray) {
      delete[] _matrix;
      _isAllocatedArray = false;
    }
    if (_WorkspaceCesca != nullptr) {
      Chameleon::cesca_ws_free(_WorkspaceCesca);
      _WorkspaceCesca = nullptr;
    }
    if (_WorkspaceGram != nullptr) {
      Chameleon::gram_ws_free(_WorkspaceGram);
      _WorkspaceGram = nullptr;
    }
    if (_WorkspaceGemm != nullptr) {
      Chameleon::gemm_ws_free(_WorkspaceGemm);
      _WorkspaceGemm = nullptr;
    }
    if (_WorkspaceSymm != nullptr) {
      Chameleon::gemm_ws_free(_WorkspaceSymm);
      _WorkspaceSymm = nullptr;
    }
#if defined(FMR_HDF5)
    if (_h5blocks != nullptr) {
      delete _h5blocks;
      _h5blocks = nullptr;
    }
#endif
  }

  /**
   * @brief init copy the elements of a descriptor in the descriptor
   *
   * @param[in] inDesc descriptor supplied by the user
   */
  void init(CHAM_desc_t *inDesc) {
    // create Chameleon descriptor if destroyed before
    if (!_isAllocated) {
      allocate();
    }
    // copy from inMatrix into _desc
    Chameleon::lacpy(_chamUpperLower, inDesc, _desc);
  }

  /**
   * @brief init reshape the matrix and copy the elements of a descriptor in
   *        the descriptor
   * @param[in] inMatrix
   */
  void init(const FSize &nbRows, const FSize &nbCols, const ChameleonDenseWrapper &inMatrix) {
    init(nbRows, nbCols, inMatrix.getMatrix());
  }

  /**
   * @brief init reshape the matrix and copy the elements of a descriptor in
   *        the descriptor
   * @param[in] inMatrix
   */
  void init(const FSize &nbRows, const FSize &nbCols, CHAM_desc_t *inMatrix) {
    if (inMatrix != _desc) {
      reset(nbRows, nbCols);
      initChameleonParameters(true);
      if ((int)nbRows == inMatrix->m && (int)nbCols == inMatrix->n) {
          init(inMatrix);
      } else if ((int)nbRows > inMatrix->m || (int)nbCols > inMatrix->n ) {
          std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                    << std::endl;
          std::cerr << "[fmr] The matrix to copy must not be larger than the "
                       "input matrix"
                    << std::endl;
          exit(EXIT_FAILURE);
      } else {
          // chameleon structure wrapping the submatrix (respecting tiles structure)
          CHAM_desc_t *desc_submatrix = Chameleon::desc_submatrix(inMatrix, 0, 0, nbRows, nbCols);
          init(desc_submatrix);
          std::free(desc_submatrix);
      }
    } else {
      _shape = {nbRows, nbCols};
      _nbRows = nbRows;
      _nbCols = nbCols;
      _desc->m = nbRows;
      _desc->n = nbCols;
      _desc->lm = nbRows;
      _desc->ln = nbCols;
      _desc->mt  = chameleon_ceil( _desc->m, _desc->mb );
      _desc->nt  = chameleon_ceil( _desc->n, _desc->nb );
      _desc->lmt = _desc->mt;
      _desc->lnt = _desc->nt;
      int p = chameleon_desc_datadist_get_iparam(_desc, 0);
      int q = chameleon_desc_datadist_get_iparam(_desc, 1);
      if ( _desc->myrank < (p*q) ) {
          int gmt, gnt;

          /* Compute the fictive full number of tiles to derivate the local leading dimension */
          gmt = chameleon_ceil( _desc->lm, _desc->mb );
          gnt = chameleon_ceil( _desc->ln, _desc->nb );

          _desc->llmt = chameleon_ceil( gmt, p );
          _desc->llnt = chameleon_ceil( gnt, p );

          // Local dimensions
          if ( ((_desc->lmt-1) % p) == (_desc->myrank / q) ) {
              _desc->llm  = ( _desc->llmt - 1 ) * _desc->mb + ((_desc->lm%_desc->mb==0) ? _desc->mb : (_desc->lm%_desc->mb));
          } else {
              _desc->llm  =  _desc->llmt * _desc->mb;
          }

          if ( ((_desc->lnt-1) % q) == (_desc->myrank % q) ) {
              _desc->lln  = ( _desc->llnt - 1 ) * _desc->nb + ((_desc->ln%_desc->nb==0) ? _desc->nb : (_desc->ln%_desc->nb));
          } else {
              _desc->lln  =  _desc->llnt * _desc->nb;
          }

          _desc->llm1 = _desc->llm / _desc->mb;
          _desc->lln1 = _desc->lln / _desc->nb;
      } else {
          _desc->llmt = 0;
          _desc->llnt = 0;
          _desc->llm  = 0;
          _desc->lln  = 0;
          _desc->llm1 = 0;
          _desc->lln1 = 0;
      }
    }
  }

  /**
   * @brief init copy the elements of a centralized 1d array in the descriptor
   *
   * @param[in] inMatrix 1d array supplied by the user
   */
  void init(const FSize &nbRows, const FSize &nbCols, FReal *inMatrix) {
    reset(nbRows, nbCols);
    initChameleonParameters(true);
    init(inMatrix);
  }

  /**
   * @brief init copy the elements of a centralized 1d array in the descriptor
   *
   * @param[in] inMatrix 1d array supplied by the user
   */
  void init(FReal *inMatrix) {
    // create Chameleon descriptor if destroyed before
    if (!_isAllocated) {
      allocate();
    }
    // copy from inMatrix into _desc
    struct cham_map_data_s map_data = {
        .access = ChamRW,
        .desc   = _desc,
    };
    struct cham_map_operator_s map_op = {
        .name = "LapackTile",
        .cpufunc = chamMapLapackTile,
        .cudafunc = NULL,
        .hipfunc = NULL,
    };
    Chameleon::map(_chamUpperLower, 1, &map_data, &map_op, inMatrix);
    // need to synchronize because we do not control inMatrix and it can be
    // deallocated at any time
    Chameleon::barrier();
  }

  /**
   * @brief init the elements of the descriptor with a value
   *
   * @param[in] inValue floating point value supplied by the user
   */
  void init(const FReal inValue) {
    // create Chameleon descriptor if destroyed before
    if (!_isAllocated) {
      allocate();
    }
    // set the value into _desc
    Chameleon::laset(_chamUpperLower, inValue, inValue, _desc);
  }

  /*
   * @brief Set 2d block cyclic parameters (P, Q) in the constructor.
   */
  inline int compute2dBlockCyclicParameters( int p = 0 ) {
    _2dBlockCyclicP = Chameleon::mapping_2dbc_p( p, _nbRows, _nbCols );
    _2dBlockCyclicQ = Chameleon::mapping_2dbc_q( p, _nbRows, _nbCols );
    return 0;
  }

private:
  static int min(int x, int y) { return (x < y) ? x : y; }
  static int max(int x, int y) { return (x < y) ? y : x; }

  /**
   * @brief init chameleon parameters
   * @param[in] set2dbcp true to compute 2D-block cyclic parameter of
   * chameleon
   */
  void initChameleonParameters(bool set2dbcp) {

#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
        _mpiRank = Chameleon::mpi_rank();
        _mpiNP = Chameleon::mpi_nprocs();
#endif

    _chamDataType = sizeof(FReal) == 4 ? ChamRealFloat : ChamRealDouble;
    _chamUpperLower = _matrixIsSymmetric ? ChamUpper : ChamUpperLower;

    if (_matrixIsSymmetric && _nbRows != _nbCols) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] This matrix is declared as symmetric but nb Rows is "
                   "different from nb Cols"
                << std::endl;
      exit(EXIT_FAILURE);
    }

    Chameleon::get(CHAMELEON_TILE_SIZE, &_tileSize);

    if (set2dbcp) {
      compute2dBlockCyclicParameters();
    }
  }

  /**
   * @brief function to copy 1d array in chameleon tiles
   */
  static int chamMapLapackTile(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                               const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
    FSize nrows, ncols, ldam, lda, row_min, col_min;

    // address of the tile to fill
    FReal *T = (FReal *)cham_tile->mat;
    // metadata to copy the lapack array (pointer to the 1d array)
    FReal *A = (FReal *)user_data;

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile
    uplo = (m == n) ? uplo : ChamUpperLower;

    // sizes of the tile
    nrows = (m == (descA->mt - 1)) ? (descA->m - m * descA->mb) : descA->mb;
    ncols = (n == (descA->nt - 1)) ? (descA->n - n * descA->nb) : descA->nb;
    ldam = descA->get_blkldd(descA, m);

    // we consider that the user's 1d array is contiguous in memory so that
    // leading dimension of A is the number of rows of A (get it with descA->m)
    lda = descA->m;
    // starting row index of the block to copy from A
    row_min = m * descA->mb;
    // starting column index of the block to copy from A
    col_min = n * descA->nb;

    // perform the copy
    for (FSize j = 0; j < ncols; j++) {
      FSize imin = (uplo == ChamLower) ? j : 0;
      FSize imax = (uplo == ChamUpper) ? min(j + 1, nrows) : nrows;
      for (FSize i = imin; i < imax; i++) {
        T[i + j * ldam] = A[(col_min + j) * lda + (row_min + i)];
      }
    }
    return 0;
  }

public:
  /**
   * @brief reset matrix parameters such as dimensions and reallocate memory
   * @param[in] nRows new number of rows
   * @param[in] nCols new number of columns
   * @param[in] inMatrixIsSymmetric true if new matrix considered as symmetric
   */
  void reset(FSize nRows, FSize nCols, const bool inMatrixIsSymmetric = false) {
    free();
    _nbRows = nRows;
    _nbCols = nCols;
    _shape[0] = nRows;
    _shape[1] = nCols;
    _matrixIsSymmetric = inMatrixIsSymmetric;
    _chamUpperLower = _matrixIsSymmetric ? ChamUpper : ChamUpperLower;
    _desc = nullptr;
    _matrix = nullptr;
    compute2dBlockCyclicParameters();
  }
  /**
   * @brief reset matrix parameters such as dimensions and reallocate memory,
   * initialize the data with the value in
   * @param[in] nRows new number of rows
   * @param[in] nCols new number of columns
   * @param[in] in real value to initialize the matrix
   * @param[in] inMatrixIsSymmetric true if new matrix considered as symmetric
   */
  template <typename InitType>
  void reset(FSize nRows, FSize nCols, InitType in,
             const bool inMatrixIsSymmetric = false) {
    reset(nRows, nCols, inMatrixIsSymmetric);
    init(in);
  }
  /**
   * @brief reset matrix parameters such as dimensions, reallocate memory and
   *        copy the matrix Mat in the class
   * @param[in] Mat The matrix to copy in the class
   */
  int reset(const ChameleonDenseWrapper &Mat) {
    free();
    _nbRows = Mat._nbRows;
    _nbCols = Mat._nbCols;
    _shape[0] = _nbRows;
    _shape[1] = _nbCols;
    _matrixIsSymmetric = Mat._matrixIsSymmetric;
    initChameleonParameters(true);
    init(Mat._desc);
    return 0;
  }
  /**
   * @brief return a pointer on the Chameleon Descriptor.
   *
   * It should be used only with the classes fully compatible with
   * ChameleonDenseWrapper
   *
   */
  CHAM_desc_t *getMatrix() const { return _desc; }
  CHAM_desc_t *&getMatrix() { return _desc; }

  /**
   * @brief return a pointer on the Chameleon Descriptor for Workspace Cesca.
   *
   * It should be used only with the classes fully compatible with
   * ChameleonDenseWrapper
   *
   */
  void *getMatrixWorkspaceCesca() {
      if (isAsync() && _WorkspaceCesca == nullptr) {
          _WorkspaceCesca = Chameleon::cesca_ws_alloc(_desc);
      }
      return _WorkspaceCesca;
  }
  /**
   * @brief return a pointer on the Chameleon Descriptor for Workspace Gram.
   *
   * It should be used only with the classes fully compatible with
   * ChameleonDenseWrapper
   *
   */
  void *getMatrixWorkspaceGram() {
      if (isAsync() && _WorkspaceGram == nullptr) {
          _WorkspaceGram = Chameleon::gram_ws_alloc(_desc);
      }
      return _WorkspaceGram;
  }
  /**
   * @brief return a pointer on the Chameleon Descriptor for Workspace Gemm.
   *
   * It should be used only with the classes fully compatible with
   * ChameleonDenseWrapper
   *
   */
  void *getMatrixWorkspaceGemm(const CHAM_desc_t *A, const CHAM_desc_t *B) {
      if (isAsync() && _WorkspaceGemm == nullptr) {
          _WorkspaceGemm = Chameleon::gemm_ws_alloc(ChamNoTrans, ChamNoTrans, A, B, _desc);
      }
      return _WorkspaceGemm;
  }
  /**
   * @brief return a pointer on the Chameleon Descriptor for Workspace Symm.
   *
   * It should be used only with the classes fully compatible with
   * ChameleonDenseWrapper
   *
   */
  void *getMatrixWorkspaceSymm(const CHAM_desc_t *A, const CHAM_desc_t *B) {
      if (isAsync() && _WorkspaceSymm == nullptr) {
          _WorkspaceSymm = Chameleon::symm_ws_alloc(ChamLeft, _chamUpperLower, A, B, _desc);
      }
      return _WorkspaceSymm;
  }

  /**
   * @brief return a pointer on a centralized 1D array column major
   * (blas/lapack data type) containing up-to-date data.
   */
  FReal *getBlasMatrix() {
    if ( !_isExternalArray ){
      if (_matrix == nullptr) {
        _matrix = new FReal[_nbVals];
        if (_matrix != NULL) {
          _isAllocatedArray = true;
          // initialize it on all processus
          std::memset(_matrix, 0, _nbVals * sizeof(FReal));
        }
      }
      // we need to ensure that _matrix is up-to-date
      if (_isAllocated) {
        Chameleon::barrier();
        Chameleon::desc_to_lapack(_chamUpperLower, _desc, _matrix, _nbRows);
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
        MPI_Datatype dtype = sizeof(FReal) == 4 ? MPI_FLOAT : MPI_DOUBLE;
        MPI_Bcast(_matrix, _nbVals, dtype, 0, MPI_COMM_WORLD);
#endif
      }
    }
    return _matrix;
  }

  /**
   *  @brief copy the matrix in an array whose memory is handle by the caller
   *  @param[out] outputMatrix matrix to fill
   */
  int copyMatrix(FReal *outputMatrix) {
    if (!_isAllocated) {
      std::cerr << "ChameleonDenseWrapper: the matrix has not been "
                   "allocated yet, we can't copy values."
                << std::endl;
      exit(EXIT_FAILURE);
    }
    // we need to ensure that _matrix is up-to-date
    Chameleon::barrier();
    if ( _isExternalArray ) {
      memcpy(outputMatrix, _matrix, _nbRows * _nbCols * sizeof(FReal));
    } else {
      Chameleon::desc_to_lapack(_chamUpperLower, _desc, outputMatrix, _nbRows);
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
      MPI_Datatype dtype = sizeof(FReal) == 4 ? MPI_FLOAT : MPI_DOUBLE;
      MPI_Bcast(outputMatrix, _nbVals, dtype, 0, MPI_COMM_WORLD);
#endif
    }
    return 0;
  }
  /**
   * @brief Get wrapper ID
   */
  int getID() { return 1; }

  /**
   * @brief Get symmetric flag
   */
  bool isSymmetric() const { return _matrixIsSymmetric; }

  /**
   * @brief Set symmetric flag
   * @param[in] sym to set if the matrir should be considered as symmetric or
   * not
   */
  void setSymmetric(bool sym) { _matrixIsSymmetric = sym; }

  /**
   * @brief Get UpperLower flag when the matrix is symmetric
   */
  cham_uplo_t getUpperLower() { return _chamUpperLower; }

  /**
   * @brief Set h5blocks to keep trace of this structure pointer in order to
   * be able to free memory later.
   *
   * @param[in] h5blocks pointer to save
   *
   * The Dense wrapper is sometimes the only object to be passed in some
   * function arguments like for reading HDF5 files. And for Chameleon,
   * because of the asynchronous algorithms feature, we need to keep trace of
   * some objects such as file/dataset ids and specific structures to handle
   * multiple files reading by panel. Saving the pointer to the Hdf5Blocks
   * structure allows to free memory. Because reading hdf5 files is done to
   * fill a dense wrapper matrix, freeing Hdf5Blocks can be done when freeing
   * the dense wrapper, see function free().
   *
   */
  void setH5Blocks(Hdf5Blocks *h5blocks) { _h5blocks = h5blocks; }

private:
  typedef struct {
    // index row where to start the copy in the input matrix
    int offset_row;
    // index column where to start the copy in the input matrix
    int offset_col;
    // number of rows of the submatrix
    int nrows;
    // number of cols of the submatrix
    int ncols;
    // whether or not to consider transpose
    bool trans;
    // pointer to the output submatrix
    FReal *mat;
  } SubMat;

  /**
   * @brief To get a subblock of the matrix
   *
   * This function will be called by chameleon on every tile of the matrix
   * through the Chameleon map function.
   *
   * @param[inout] descA chameleon structure representing the matrix
   * @param[in] uplo pattern: ChamUpperLower, ChamUpper or ChamLower
   * @param[in] m position index of the tile in rows
   * @param[in] n position index of the tile in columns
   * @param[in] cham_tile the tile address given by chameleon
   * @param[in] user_data user's data structure that helps filling the user's
   * submatrix.
   *
   */
  static int chamMapGetSubMatrix(const CHAM_desc_t *descA, cham_uplo_t uplo,
                                 int m, int n, CHAM_tile_t *cham_tile,
                                 void *user_data) {
    // parameters for tile dimension and position in the global matrix
    int tile_ldam;

    // address of the tile
    FReal *tile_cham = (FReal *)cham_tile->mat;
    // the submatrix where to copy
    SubMat *submat = (SubMat *)user_data;

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile
    uplo = (m == n) ? uplo : ChamUpperLower;

    // leading dimension of the tile
    tile_ldam = descA->get_blkldd(descA, m);

    // starting row index of the tile in the global matrix
    int tile_row_min = m * descA->mb;
    // starting column index of the tile in the global matrix
    int tile_col_min = n * descA->nb;
    // ending row index of the tile in the global matrix
    int tile_row_max = (m == (descA->mt - 1)) ? descA->m : (m + 1) * descA->mb;
    // ending column index of the tile in the global matrix
    int tile_col_max = (n == (descA->nt - 1)) ? descA->n : (n + 1) * descA->nb;

    // global indexes of corners of the submatrix (top left and bottom right)
    int submat_row_min = submat->offset_row;
    int submat_row_max = submat->offset_row + submat->nrows;
    int submat_col_min = submat->offset_col;
    int submat_col_max = submat->offset_col + submat->ncols;
    // global indexes of the intersection between the submatrix and the tile
    int64_t int_row_min = -1, int_row_max = -1, int_col_min = -1,
            int_col_max = -1;
    if (tile_row_max >= submat_row_min && tile_row_min <= submat_row_max) {
      int_row_min = std::max(tile_row_min, submat_row_min);
      int_row_max = std::min(tile_row_max, submat_row_max);
    }
    if (tile_col_max >= submat_col_min && tile_col_min <= submat_col_max) {
      int_col_min = std::max(tile_col_min, submat_col_min);
      int_col_max = std::min(tile_col_max, submat_col_max);
    }
    // is the intersection non empty
    int64_t int_min =
        std::min({int_row_min, int_row_max, int_col_min, int_col_max});
    // if int_min==-1 then it is empty, else copy the intersection
    if (int_min >= 0) {
      // compute the starting index of the intersection relative to the
      // submatrix
      int intsubmat_row_min = int_row_min - submat_row_min;
      int intsubmat_col_min = int_col_min - submat_col_min;
      // size of the intersection
      int intsubmat_nrows = int_row_max - int_row_min;
      int intsubmat_ncols = int_col_max - int_col_min;
      // compute the indexes where to start in the chameleon tile in
      // order to copy the data in the proper part which represents the
      // intersection
      int zero = 0;
      int inttile_row_min = std::max(zero, submat_row_min - tile_row_min);
      int inttile_col_min = std::max(zero, submat_col_min - tile_col_min);
      // copy in the submatrix
      for (int j = 0; j < intsubmat_ncols; ++j) {
        int imin = (uplo == ChamLower) ? j + inttile_row_min : inttile_row_min;
        int imax = (uplo == ChamUpper)
                       ? std::min(j + 1, inttile_row_min + intsubmat_nrows)
                       : inttile_row_min + intsubmat_nrows;
        for (int i = imin; i < imax; ++i) {
          int index_cham = (inttile_col_min + j) * tile_ldam + i;
          int index_submat = (intsubmat_col_min + j) * submat->nrows +
                             (intsubmat_row_min + i - imin);
          submat->mat[index_submat] = tile_cham[index_cham];
        }
      }
    }
    return 0;
  }

  /**
   * @brief To set a subblock of the matrix
   *
   * This function will be called by chameleon on every tile of the matrix
   * through the Chameleon map function.
   *
   * @param[inout] descA chameleon structure representing the matrix to update
   * @param[in] uplo where to apply the update: ChamUpperLower, ChamUpper or
   * ChamLower
   * @param[in] m position index of the tile in rows
   * @param[in] n position index of the tile in columns
   * @param[in] cham_tile the tile address given by chameleon
   * @param[in] user_data user's data structure that helps filling the
   * submatrix with user's values.
   *
   */
  static int chamMapSetSubMatrix(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                                 const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
    // parameters for tile dimension and position in the global matrix
    int tile_ldam;

    // address of the tile
    FReal *tile_cham = (FReal *)cham_tile->mat;
    // the submatrix where to copy
    SubMat *submat = (SubMat *)user_data;

    // uplo is meaningfull only for tiles on the diagonal, else we need to copy
    // the entire tile
    uplo = (m == n) ? uplo : ChamUpperLower;

    // leading dimension of the tile
    tile_ldam = descA->get_blkldd(descA, m);

    // starting row index of the tile in the global matrix
    int tile_row_min = m * descA->mb;
    // starting column index of the tile in the global matrix
    int tile_col_min = n * descA->nb;
    // ending row index of the tile in the global matrix
    int tile_row_max = (m == (descA->mt - 1)) ? descA->m : (m + 1) * descA->mb;
    // ending column index of the tile in the global matrix
    int tile_col_max = (n == (descA->nt - 1)) ? descA->n : (n + 1) * descA->nb;

    // global indexes of corners of the submatrix (top left and bottom right)
    int submat_row_min = submat->offset_row;
    int submat_row_max = submat->offset_row + submat->nrows;
    int submat_col_min = submat->offset_col;
    int submat_col_max = submat->offset_col + submat->ncols;

    // global indexes of the intersection between the submatrix and the tile
    int64_t int_row_min = -1, int_row_max = -1, int_col_min = -1,
            int_col_max = -1;
    if (tile_row_max >= submat_row_min && tile_row_min <= submat_row_max) {
      int_row_min = std::max(tile_row_min, submat_row_min);
      int_row_max = std::min(tile_row_max, submat_row_max);
    }
    if (tile_col_max >= submat_col_min && tile_col_min <= submat_col_max) {
      int_col_min = std::max(tile_col_min, submat_col_min);
      int_col_max = std::min(tile_col_max, submat_col_max);
    }

    // is the intersection non empty
    int64_t int_min =
        std::min({int_row_min, int_row_max, int_col_min, int_col_max});

    // if int_min==-1 then it is empty, else copy the intersection
    if (int_min >= 0) {
      // compute the starting index of the intersection relative to the
      // submatrix
      int intsubmat_row_min = int_row_min - submat_row_min;
      int intsubmat_col_min = int_col_min - submat_col_min;
      // size of the intersection
      int intsubmat_nrows = int_row_max - int_row_min;
      int intsubmat_ncols = int_col_max - int_col_min;

      // compute the indexes where to start in the chameleon tile in
      // order to copy the data in the proper part which represents the
      // intersection
      int zero = 0;
      int inttile_row_min = std::max(zero, submat_row_min - tile_row_min);
      int inttile_col_min = std::max(zero, submat_col_min - tile_col_min);

      // copy in the submatrix
      for (int j = 0; j < intsubmat_ncols; ++j) {
        int imin = (uplo == ChamLower) ? j + inttile_row_min : inttile_row_min;
        int imax = (uplo == ChamUpper)
                       ? std::min(j + 1, inttile_row_min + intsubmat_nrows)
                       : inttile_row_min + intsubmat_nrows;

        for (int i = imin; i < imax; ++i) {
          int index_cham = (inttile_col_min + j) * tile_ldam + i;
          int index_submat;
          if (submat->trans) {
            index_submat = (intsubmat_row_min + i - imin) * submat->ncols +
                           (intsubmat_col_min + j);
          } else {
            index_submat = (intsubmat_col_min + j) * submat->nrows +
                           (intsubmat_row_min + i - imin);
          }
          tile_cham[index_cham] = submat->mat[index_submat];
        }
      }
    }
    return 0;
  }

public:
  /**
   * @brief To get a subblock of the matrix
   *
   * @param[in] idxRowStart global row index where to start copying
   * @param[in] idxColStart global column index where to start copying
   * @param[in] nrows number of rows to copy
   * @param[in] ncols number of columns to copy
   * @param[out] submatrix output array to fill with the dense wrapper subblock
   * @param[in] bcast whether or not to broadcast the resulting submatrix on
   * all the MPI processes
   *
   */
  void getSubMatrix(const FSize idxRowStart, const FSize idxColStart,
                    const FSize nrows, const FSize ncols, FReal *submatrix,
                    const bool bcast = true) const {
    if (_matrixIsSymmetric) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] getSubMatrix, extractCol[s] not available for "
                   "symmetric matrices yet"
                << std::endl;
      exit(EXIT_FAILURE);
    }
    /* compute the offset to reach the first tile owning the submatrix */
    FSize smtiles_offset_rows = (idxRowStart / _tileSize) * _tileSize;
    FSize smtiles_offset_cols = (idxColStart / _tileSize) * _tileSize;
    /* compute the size of the submatrix, a little bit larger to fit inside
        the existing tiles structure of _desc */
    FSize smtiles_nrows = nrows + (idxRowStart - smtiles_offset_rows);
    FSize smtiles_ncols = ncols + (idxColStart - smtiles_offset_cols);

    // chameleon structure wrapping the submatrix (respecting tiles structure)
    CHAM_desc_t *desc_submatrix = Chameleon::desc_submatrix(
        _desc, smtiles_offset_rows, smtiles_offset_cols, smtiles_nrows,
        smtiles_ncols);

    // column major matrix of tiles wrapping the submatrix
    FReal *submattile = new FReal[smtiles_nrows * smtiles_ncols];
    // centralize submatrix with tiles on MPI rank 0
    Chameleon::desc_to_lapack(_chamUpperLower, desc_submatrix, submattile,
                              smtiles_nrows);
    // copy the necessary part in the submatrix (without useless piece of
    // tiles not representing the submatrix)
    if (_mpiRank == 0) {
      smtiles_offset_rows = smtiles_nrows - nrows;
      smtiles_offset_cols = smtiles_ncols - ncols;
      for (FSize j(smtiles_offset_cols); j < smtiles_ncols; ++j) {
        for (FSize i(smtiles_offset_rows); i < smtiles_nrows; ++i) {
          submatrix[(j - smtiles_offset_cols) * nrows +
                    (i - smtiles_offset_rows)] =
              submattile[j * smtiles_nrows + i];
        }
      }
    }
    // Broadcast submatrix on all processus
    if (bcast) {
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
      MPI_Datatype dtype = sizeof(FReal) == 4 ? MPI_FLOAT : MPI_DOUBLE;
      MPI_Bcast(submatrix, nrows * ncols, dtype, 0, MPI_COMM_WORLD);
#endif
    }
    delete[] submattile;
    std::free(desc_submatrix);
  }

  /**
   * @brief To set a subblock of the matrix
   *
   * @param[in] idxRowStart global row index where to start copying
   * @param[in] idxColStart global column index where to start copying
   * @param[in] nrows number of rows to copy
   * @param[in] ncols number of columns to copy
   * @param[in] submatrix input array to copy into the dense wrapper subblock
   * @param[in] transpose whether or not to consider the transpose of
   * submatrix ('N' none, 'T' transpose)
   * @param[in] bcast whether or not to broadcast the resulting submatrix on
   * all the MPI processes
   *
   */
  void setSubMatrix(const FSize idxRowStart, const FSize idxColStart,
                    const FSize nrows, const FSize ncols, FReal *submatrix,
                    const std::string &transpose = "N",
                    const bool bcast = true) {
    // structure that wraps the submatrix to extract the proper values from
    // the tiles with the chamMapGetSubMatrix function
    SubMat submat;
    // define submat
    submat.offset_row = idxRowStart;
    submat.offset_col = idxColStart;
    submat.nrows = nrows;
    submat.ncols = ncols;
    if (transpose[0] == std::string("T")[0]) {
      submat.trans = true;
    } else {
      submat.trans = false;
    }
    submat.mat = submatrix;
    if (bcast) {
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
      MPI_Datatype dtype = sizeof(FReal) == 4 ? MPI_FLOAT : MPI_DOUBLE;
      MPI_Bcast(submatrix, nrows * ncols, dtype, 0, MPI_COMM_WORLD);
#endif
    }
    // copy from _desc into submatrix
    struct cham_map_data_s map_data = {
        .access = ChamRW,
        .desc   = _desc,
    };
    struct cham_map_operator_s map_op = {
        .name = "SetSubMatrix",
        .cpufunc = chamMapSetSubMatrix,
        .cudafunc = NULL,
        .hipfunc = NULL,
    };
    Chameleon::map(_chamUpperLower, 1, &map_data, &map_op, &submat);
    Chameleon::barrier();
  }

  /**
   * @brief To copy one column into a vector
   *
   * @param[in] colIdxIn column index to copy into vector
   * @param[out] columns vector to fill with column from the dense wrapper
   *
   */
  void extractCol(FSize colIdxIn, std::vector<FReal> &columns) const {
    if (colIdxIn > _nbCols) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy column " << colIdxIn
                << " from a matrix with " << _nbCols << " columns" << std::endl;
      exit(EXIT_FAILURE);
    }
    FReal *column = new FReal[_nbRows];
    getSubMatrix(0, colIdxIn, _nbRows, 1, column);
    columns.insert(columns.end(), &column[0], &column[_nbRows]);
    delete[] column;
  }

  /**
   * @brief To copy overall columns into a vector
   *
   * @param[in] listCols column indexes to copy into vector
   * @param[out] columns vector to fill with columns from the dense wrapper
   *
   */
  void extractCols(std::vector<FSize> listCols,
                   std::vector<FReal> &columns) const {
    for (typename std::vector<FSize>::iterator it = listCols.begin();
         it != listCols.end(); ++it) {
      extractCol(*it, columns);
    }
  }

  /**
   * @brief To copy one column into another dense wrapper
   *
   * @param[in] colIdxIn column index to copy into vector
   * @param[out] columns dense wrapper to fill with column from the dense
   * wrapper
   *
   */
  void extractCol(FSize colIdxIn, FSize colIdxOut,
                  ChameleonDenseWrapper &columns) const {
    if (colIdxIn > _nbCols) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy column " << colIdxIn
                << " from a matrix with " << _nbCols << " columns" << std::endl;
      exit(EXIT_FAILURE);
    }
    if (colIdxOut > columns.getNbCols()) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy column " << colIdxOut
                << " in a matrix with " << columns.getNbCols() << " columns"
                << std::endl;
      exit(EXIT_FAILURE);
    }
    if (_nbRows > columns.getNbRows()) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Tried to copy a column of " << _nbRows
                << " values in a matrix with " << columns.getNbRows() << " rows"
                << std::endl;
      exit(EXIT_FAILURE);
    }
    FReal *column = new FReal[_nbRows];
    getSubMatrix(0, colIdxIn, _nbRows, 1, column, false);
    columns.setSubMatrix(0, colIdxOut, _nbRows, 1, column);
    delete[] column;
  }

  /**
   * @brief To copy overall columns into a dense wrapper
   *
   * @param[in] listCols column indexes to copy into vector
   * @param[out] columns dense wrapper to fill with columns from the dense
   * wrapper
   *
   */
  void extractCols(std::vector<FSize> listCols,
                   ChameleonDenseWrapper &columns) {
    if (!columns.isAllocated()) {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__
                << std::endl;
      std::cerr << "[fmr] Output matrix is not allocated" << std::endl;
      exit(EXIT_FAILURE);
    }

    struct starpu_codelet cl_copycols1;
    starpu_codelet_init(&cl_copycols1);
    cl_copycols1.where = STARPU_CPU;
    cl_copycols1.cpu_funcs[0] = cl_copycols1_cpu_func<FSize, FReal>;
    cl_copycols1.nbuffers = 2;
    cl_copycols1.name = "copycols1";
    struct starpu_codelet cl_copycols2;
    starpu_codelet_init(&cl_copycols2);
    cl_copycols2.where = STARPU_CPU;
    cl_copycols2.cpu_funcs[0] = cl_copycols2_cpu_func<FSize, FReal>;
    cl_copycols2.nbuffers = 3;
    cl_copycols2.name = "copycols2";

    /* main loop over tiles */
    FSize colIdxW = 0;

    for (int jtr = 0; jtr < _nbTileCols; ++jtr) {

      int jtw1 = colIdxW / _tileSize;
      int ofw1 = colIdxW % _tileSize;
      int jtw2 = jtw1 + 1;

      FSize colIdxR = jtr * _tileSize;
      FSize ncolr = (jtr == _desc->lnt - 1) ? _nbCols - colIdxR : _tileSize;

      while (((size_t)colIdxW < listCols.size()) &&
             (listCols[colIdxW] < colIdxR + ncolr)) {
        colIdxW++;
      }

      for (int itr = 0; itr < _nbTileRows; ++itr) {

        /* get the chameleon tile starpu handle to read from */
        starpu_data_handle_t thr =
            (starpu_data_handle_t)Chameleon::runtime_data_getaddr(_desc, itr,
                                                                  jtr);
        /* get the chameleon tile starpu handles to write to */
        starpu_data_handle_t thw1 =
            (starpu_data_handle_t)Chameleon::runtime_data_getaddr(
                columns.getMatrix(), itr, jtw1);

        /* submit task to copy subset of cols from _desc to fill columns tiles
         */
        if ((FSize)jtw2 * _tileSize < columns.getNbCols()) {
          starpu_data_handle_t thw2 =
              (starpu_data_handle_t)Chameleon::runtime_data_getaddr(
                  columns.getMatrix(), itr, jtw2);
          insert_copycols<FSize>(&cl_copycols2, listCols.data(),
                                 _chamUpperLower, _desc, itr, jtr, thr,
                                 columns.getMatrix(), jtw1, ofw1, thw1, thw2);
        } else {
          insert_copycols<FSize>(&cl_copycols1, listCols.data(),
                                 _chamUpperLower, _desc, itr, jtr, thr,
                                 columns.getMatrix(), jtw1, ofw1, thw1);
        }
      }
    }

#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    starpu_mpi_wait_for_all(MPI_COMM_WORLD);
#else
    starpu_task_wait_for_all();
#endif
  }

private:

    /**
     * @brief To scale columns of the matrix
     *
     * This function will be called by chameleon on every tile of the matrix
     * through the Chameleon map function.
     *
     * @param[inout] descA chameleon structure representing the matrix to update
     * @param[in] uplo where to apply the update: ChamUpperLower, ChamUpper or
     * ChamLower
     * @param[in] m position index of the tile in rows
     * @param[in] n position index of the tile in columns
     * @param[in] cham_tile the tile address given by chameleon
     * @param[in] user_data user's data structure that helps scaling the
     * columns with user's values.
     *
     */
    static int chamMapScaleCols(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                                const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
        int nrows, ncols, ldam, col_min;

        // address of the tile to fill
        FReal *T = (FReal *)cham_tile->mat;
        // metadata to copy the lapack array (pointer to the 1d array)
        FReal *S = (FReal *)user_data;

        // uplo is meaningfull only for tiles on the diagonal, else we need to copy
        // the entire tile
        uplo = ( m == n ) ? uplo : ChamUpperLower;

        // sizes of the tile
        nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
        ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
        ldam  = descA->get_blkldd( descA, m );

        // starting index of the scaling vector
        col_min = n * descA->nb;

        // perform the copy
        for (int j = 0; j < ncols; j++)
        {
            int imin = ( uplo == ChamLower ) ? j               : 0;
            int imax = ( uplo == ChamUpper ) ? min(j+1, nrows) : nrows;
            for (int i = imin; i < imax; i++)
            {
                T[i + j * ldam] *= S[col_min + j];
            }
        }
        return 0;
    }

    /**
     * @brief To scale rows of the matrix
     *
     * This function will be called by chameleon on every tile of the matrix
     * through the Chameleon map function.
     *
     * @param[inout] descA chameleon structure representing the matrix to update
     * @param[in] uplo where to apply the update: ChamUpperLower, ChamUpper or
     * ChamLower
     * @param[in] m position index of the tile in rows
     * @param[in] n position index of the tile in columns
     * @param[in] cham_tile the tile address given by chameleon
     * @param[in] user_data user's data structure that helps scaling the
     * columns with user's values.
     *
     */
    static int chamMapScaleRows(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                                const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
        int nrows, ncols, ldam, row_min;

        // address of the tile to fill
        FReal *T = (FReal *)cham_tile->mat;
        // metadata to copy the lapack array (pointer to the 1d array)
        FReal *S = (FReal *)user_data;

        // uplo is meaningfull only for tiles on the diagonal, else we need to copy
        // the entire tile
        uplo = ( m == n ) ? uplo : ChamUpperLower;

        // sizes of the tile
        nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
        ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
        ldam  = descA->get_blkldd( descA, m );

        // starting index of the scaling vector
        row_min = m * descA->mb;

        // perform the copy
        for (int j = 0; j < ncols; j++)
        {
            int imin = ( uplo == ChamLower ) ? j               : 0;
            int imax = ( uplo == ChamUpper ) ? min(j+1, nrows) : nrows;
            for (int i = imin; i < imax; i++)
            {
                T[i + j * ldam] *= S[row_min + i];
            }
        }
        return 0;
    }

public:
  /**
   * @brief Scale matrix columns with a value for each column picked in
   * scaleValues
   * @param[in] nbColsToScale number of columns to scale
   * @param[in] scaleValues scaling factors
   */
    void scaleCols(const FSize &nbColsToScale, FReal* scaleValues)
    {
        if (nbColsToScale != _nbCols){
            std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
            std::cerr << "[fmr] Tried to scale " << nbColsToScale << "columns in a matrix with " << _nbCols << " columns" << std::endl;
            exit(EXIT_FAILURE);
        }
        struct cham_map_data_s map_data = {
            .access = ChamRW,
            .desc   = _desc,
        };
        struct cham_map_operator_s map_op = {
            .name = "ScaleCols",
            .cpufunc = chamMapScaleCols,
            .cudafunc = NULL,
            .hipfunc = NULL,
        };
        Chameleon::map( _chamUpperLower, 1, &map_data, &map_op, scaleValues );
        Chameleon::barrier();
    }

  /**
   * @brief Scale matrix rows with a value for each row picked in scaleValues
   * @param[in] nbRowsToScale number of rows to scale
   * @param[in] scaleValues scaling factors
   */
    void scaleRows(const FSize &nbRowsToScale, FReal* scaleValues)
    {
        if (nbRowsToScale != _nbRows){
            std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
            std::cerr << "[fmr] Tried to scale " << nbRowsToScale << "rows in a matrix with " << _nbRows << " rows" << std::endl;
            exit(EXIT_FAILURE);
        }
        struct cham_map_data_s map_data = {
            .access = ChamRW,
            .desc   = _desc,
        };
        struct cham_map_operator_s map_op = {
            .name = "ScaleRows",
            .cpufunc = chamMapScaleRows,
            .cudafunc = NULL,
            .hipfunc = NULL,
        };
        Chameleon::map( _chamUpperLower, 1, &map_data, &map_op, scaleValues );
        Chameleon::barrier();
    }

  /**
   * @brief Get a single value from a specific position
   * @param[in] idxRow row index of the value to get
   * @param[in] idxCol column index of the value to get
   * @return a real value
   */
  const FReal &getVal(const FSize idxRow, const FSize idxCol) const {
    getSubMatrix(idxRow, idxCol, 1, 1, &_value);
    return _value;
  }

  FReal &getVal(const FSize idxRow, const FSize idxCol) {
    getSubMatrix(idxRow, idxCol, 1, 1, &_value);
    return _value;
  }

  /**
   * @brief Set a single value in a specific position
   * @param[in] idxRow row index of the value to set
   * @param[in] idxCol column index of the value to set
   * @param[in] val user's value to copy into the dense wrapper
   */
  void setVal(const FSize idxRow, const FSize idxCol, FReal val) {
    setSubMatrix(idxRow, idxCol, 1, 1, &val);
  }
};
namespace traits {

/// Define the blas wrapper traits
template <typename VALUE_T, typename INT_T>
struct is_chameleon_wrapper<ChameleonDenseWrapper<INT_T, VALUE_T>>
    : std::true_type {};
} // namespace traits
} // namespace fmr

#endif /* CHAM_DENSEWRAPPER_HPP */
