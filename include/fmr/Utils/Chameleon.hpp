/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef CHAMELEON_HPP
#define CHAMELEON_HPP

#include <algorithm>
#include <array>
#include <vector>
#ifdef FMR_USE_MKL_AS_BLAS
#include <mkl_service.h> // to ensure #threads = 1
#endif
#ifdef FMR_USE_OPENBLAS_AS_BLAS
#include "openblas_cblas.h" // to ensure #threads = 1
#endif
#include <chameleon.h> // we need to call the C Chameleon API
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
#include <mpi.h>
#endif
#if defined(FMR_HDF5)
#include <hdf5.h>
#endif
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
#include <starpu_mpi.h>
#else
#include <starpu.h>
#endif
#include "Starpu.hpp"

// This file interfaces the chameleon functions to enable a generic use.

//////////////////////////////////////////////////////////////////////

/**
 * @brief Internal function to print an error message
 * @param[in] func_name where this error has been thrown
 * @param[in] msg_text text to print
 */
static void chameleon_error(const char *func_name, const char *msg_text) {
  fprintf(stderr, "CHAMELEON ERROR: %s(): %s\n", func_name, msg_text);
}

/**
 * @brief Build regular sub-diagonals of a SBC pattern
 * @param[in] R dimension of the pattern
 * @param[in] var_id pattern variant (sub-diagonal) index
 * @param[in] pos position in the selected sub-diagonals
 */
int sub_diag_pos(int R, int var, int pos)
{
  // Coordinates in the pattern of the corresponding rank for the selected position and variant
  int k; int l;
  k = var+1+pos;
  l = pos;
  // Modify because patterns are replicated side by side
  k = k%R;
  l = l%R;
  // Modify because pattern is symmetric
  if ( k < l ) {
    int tmp = k;
    k = l;
    l = tmp;
  }
  // Return the corresponding rank
  return (k*(k-1)/2)+l;
}

/**
 * @brief Function to return MPI rank of element A(m,n) using 'extended SBC'
 * mapping
 * @param[in] A matrix
 * @param[in] m row index of tile to consider
 * @param[in] n column index of tile to consider
 */
int getrankof_sbc( const CHAM_desc_t *A, int m, int n )
{

  // Coordinates of the tile in A
  int mm = m + A->i / A->mb;
  int nn = n + A->j / A->nb;
  // Call function to symmetric position if above diagonal in A
  if ( mm < nn ) {
    return getrankof_sbc ( A, n, m );
  }

  // Do not Compute SBC pattern dimension from nb of MPI rank (P*Q):
  // the descriptor is created by setting A->p = R and A->q = 1.
  //  int R = (int)(0.5*(1.0+sqrt(1.0+8.0*A->p*A->q)));
  int R = chameleon_desc_datadist_get_iparam(A, 0);
  // Coordinates of the pattern in A
  int i = mm/R;
  int j = nn/R;
  // Coodinates of the tile in the pattern
  int k = mm%R;
  int l = nn%R;
  // Look at symmetric position in the pattern if above diagonal in the pattern
  int tmp;
  if ( k < l ) {
    tmp = k;
    k = l;
    l = tmp;
  }

  // Not on a pattern diagonal
  if ( k > l ) {
    return ( k > 1 ) ? (k*(k-1)/2)+l : 0;
  }
  // On a pattern diagonal -> need to select a variant
  else {
    int sub_nb; int var_id;
    // Number of sub-diagonals in the pattern
    sub_nb = ceil((R-1)/2.0);
    // Easy case -> variants are exactly the sub-diagonals (no repeated sequence of proc)
    if ((R-1)%2 == 0) {
      var_id = (i*(i+1)/2+j)%sub_nb;
      return sub_diag_pos(R, var_id, k);
    }
    // Hard case -> need to split sub-diagonals in two and apply cyclic permutation to half
    else {
      var_id = (i*(i+1)/2+j)%(2*sub_nb-1);
      // The variant is a sub-diagonal without repetition (ie. without the "bonus pack")
      if ( var_id < sub_nb-1 ) {
        return sub_diag_pos(R, var_id, k);
      }
      // The variant is two halves ("packs") of sub-diagonals bound together
      else {
        return sub_diag_pos(R, ( k < R/2 ) ? var_id%sub_nb : var_id-(sub_nb-1), k);
      }
    }
  }

}

/**
 * @brief Internal function to return MPI rank of block (m,n) in distribution
 * "1D over rows"
 * @param[in] A matrix
 * @param[in] m row index of tile to consider
 * @param[in] n column index of tile to consider
 */
static int chameleon_getrankof_2dnocyclic(const CHAM_desc_t *A, int m, int n) {
  int mm = m + A->i / A->mb;
  int nn = n + A->j / A->nb;
  assert((mm / A->llmt) < chameleon_desc_datadist_get_iparam(A, 0));
  return (mm / A->llmt) + (nn % chameleon_desc_datadist_get_iparam(A, 1));
}
//////////////////////////////////////////////////////////////////////

/** @brief 2d array that stores tile MPI ranks in case of specific distribution
 * with TBC. This is needed because "chameleon_getrankof_" functions (to
 * be given in "CHAMELEON_Desc_Create_" functions) API does not take arbitrary
 * user's data */
static int **chameleon_rankofTiles_tbc = NULL;
/** @brief whether or not chameleon_rankofTiles is allocated */
static bool chameleon_isAllocatedRankofTiles_tbc = false;

/**
 * @brief Internal function to return MPI rank of block (m,n) in distribution
 * "TBC files"
 * @param[in] A matrix
 * @param[in] m row index of tile to consider
 * @param[in] n column index of tile to consider
 */
static int getrankof_tbc(const CHAM_desc_t *A, int m, int n) {
  (void)A;
  //return chameleon_rankofTiles_tbc[ n * A->lmt + m ];
  return chameleon_rankofTiles_tbc[m][n];
}

void chameleon_freerankoftiles_tbc(int nbtilerows) {
  if (chameleon_isAllocatedRankofTiles_tbc) {
    for (int i = 0; i < nbtilerows; ++i) {
      delete[] chameleon_rankofTiles_tbc[i];
    }
    delete[] chameleon_rankofTiles_tbc;
  }
  chameleon_isAllocatedRankofTiles_tbc = false;
}

#if defined(FMR_HDF5)

/** @brief 2d array that stores tile MPI ranks in case of specific distribution
 * with hdf5 files. This is needed because "chameleon_getrankof_" functions (to
 * be given in "CHAMELEON_Desc_Create_" functions) API does not take arbitrary
 * user's data */
static int **chameleon_rankofTiles_h5 = NULL;
/** @brief whether or not chameleon_rankofTiles is allocated */
static bool chameleon_isAllocatedRankofTiles_h5 = false;

/**
 * @brief Internal function to return MPI rank of block (m,n) in distribution
 * "hdf5 files"
 * @param[in] A matrix
 * @param[in] m row index of tile to consider
 * @param[in] n column index of tile to consider
 */
static int chameleon_getrankof_h5(const CHAM_desc_t *A, int m,
                                            int n) {
  (void)A;
  return chameleon_rankofTiles_h5[m][n];
}

void chameleon_freerankoftiles_h5(int nbtilerows) {
  if (chameleon_isAllocatedRankofTiles_h5) {
    for (int i = 0; i < nbtilerows; ++i) {
      delete[] chameleon_rankofTiles_h5[i];
    }
    delete[] chameleon_rankofTiles_h5;
  }
  chameleon_isAllocatedRankofTiles_h5 = false;
}

typedef struct MpiRanks MpiRanks;
/**
 * @brief Mpi ranks handling H5 File.
 */
struct MpiRanks {
  int rank;
  MpiRanks *next;
};
typedef struct IdxInt IdxInt;
/**
 * @brief Indexes intervals (or rows/columns) handled by processes in H5 Files.
 */
struct IdxInt {
  size_t idx[2];
  IdxInt *next;
};
/**
 * @brief Struct containing all lists pointers and size of the lists.
 */
typedef struct MapH5MpiList MapH5MpiList;
struct MapH5MpiList {
  int nb;
  MpiRanks *firstmpiranks;
  IdxInt *firstrows;    // Idx Interval of rows in the current H5 file
  IdxInt *firstcols;    // Idx Interval of cols in the current H5 file
  IdxInt *firstrowsglo; // Idx Interval of rows in the global distributed matrix
  IdxInt *firstcolsglo; // Idx Interval of cols in the global distributed matrix
};
/**
 * @brief Define the subset of MPI processes handling the HDF5 file.
 */
typedef struct MapH5Mpi MapH5Mpi;
struct MapH5Mpi {
  std::string filename; // H5 filename, absolute path
  int npmax;            // number max of different processes handling the file
  MapH5MpiList *mpirr;  // list of MPI ranks handling the file and of rows
                        // interval to handle in the file for each MPI rank
  ~MapH5Mpi()
  {
    for (int i = 0; i < mpirr->nb; i++) {
      MpiRanks *mpiranks = mpirr->firstmpiranks;
      IdxInt *rows = mpirr->firstrows;
      IdxInt *cols = mpirr->firstcols;
      IdxInt *rowsglo = mpirr->firstrowsglo;
      IdxInt *colsglo = mpirr->firstcolsglo;
      mpirr->firstmpiranks = mpirr->firstmpiranks->next;
      mpirr->firstrows = mpirr->firstrows->next;
      mpirr->firstcols = mpirr->firstcols->next;
      mpirr->firstrowsglo = mpirr->firstrowsglo->next;
      mpirr->firstcolsglo = mpirr->firstcolsglo->next;
      std::free(mpiranks);
      std::free(rows);
      std::free(cols);
      std::free(rowsglo);
      std::free(colsglo);
    }
    std::free(mpirr);
  }
};

/**
 * @brief Insert element in the lists of kind: a subblock of rows in the hdf5
 * file when reading a global matrix from multiple hdf5 files.
 *
 * @param[in] list the chained list to fill
 * @param[in] rank MPI rank which handle this subblock
 * @param[in] rowmin starting row index in the hdf5 file matrix
 * @param[in] rowmax ending row index in the hdf5 file matrix
 * @param[in] colmin starting column index in the hdf5 file matrix
 * @param[in] colmax ending column index in the hdf5 file matrix
 * @param[in] rowminglo starting row index in the global matrix
 * @param[in] rowmaxglo ending row index in the global matrix
 * @param[in] colminglo starting column index in the global matrix
 * @param[in] colmaxglo ending column index in the global matrix
 */
void InsertMapH5Mpi(MapH5MpiList *list, int rank, size_t rowmin, size_t rowmax,
                    size_t colmin, size_t colmax, size_t rowminglo,
                    size_t rowmaxglo, size_t colminglo, size_t colmaxglo) {
  MpiRanks *mpiranks = (MpiRanks *)malloc(sizeof(MpiRanks));
  mpiranks->rank = rank;
  mpiranks->next = NULL;
  if (list->nb > 0) {
    MpiRanks *ptrrank = list->firstmpiranks;
    while (ptrrank->next != NULL) {
      ptrrank = ptrrank->next;
    }
    ptrrank->next = mpiranks;
  } else {
    list->firstmpiranks = mpiranks;
  }

  IdxInt *rows = (IdxInt *)malloc(sizeof(IdxInt));
  rows->idx[0] = rowmin;
  rows->idx[1] = rowmax;
  rows->next = NULL;
  if (list->nb > 0) {
    IdxInt *ptridx = list->firstrows;
    while (ptridx->next != NULL) {
      ptridx = ptridx->next;
    }
    ptridx->next = rows;
  } else {
    list->firstrows = rows;
  }

  IdxInt *cols = (IdxInt *)malloc(sizeof(IdxInt));
  cols->idx[0] = colmin;
  cols->idx[1] = colmax;
  cols->next = NULL;
  if (list->nb > 0) {
    IdxInt *ptridx = list->firstcols;
    while (ptridx->next != NULL) {
      ptridx = ptridx->next;
    }
    ptridx->next = cols;
  } else {
    list->firstcols = cols;
  }

  IdxInt *rowsglo = (IdxInt *)malloc(sizeof(IdxInt));
  rowsglo->idx[0] = rowminglo;
  rowsglo->idx[1] = rowmaxglo;
  rowsglo->next = NULL;
  if (list->nb > 0) {
    IdxInt *ptridx = list->firstrowsglo;
    while (ptridx->next != NULL) {
      ptridx = ptridx->next;
    }
    ptridx->next = rowsglo;
  } else {
    list->firstrowsglo = rowsglo;
  }

  IdxInt *colsglo = (IdxInt *)malloc(sizeof(IdxInt));
  colsglo->idx[0] = colminglo;
  colsglo->idx[1] = colmaxglo;
  colsglo->next = NULL;
  if (list->nb > 0) {
    IdxInt *ptridx = list->firstcolsglo;
    while (ptridx->next != NULL) {
      ptridx = ptridx->next;
    }
    ptridx->next = colsglo;
  } else {
    list->firstcolsglo = colsglo;
  }

  list->nb += 1;
}

/**
 * @brief Print all elements of the list.
 * @param[in] list the chained list to print
 */
void PrintMapH5Mpi(MapH5MpiList *list) {
  MpiRanks *ranks = list->firstmpiranks;
  IdxInt *rows = list->firstrows;
  IdxInt *cols = list->firstcols;
  IdxInt *rowsglo = list->firstrowsglo;
  IdxInt *colsglo = list->firstcolsglo;
  for (int i = 0; i < list->nb; i++) {
    printf("%d: idx H5file [%zu %zu], [%zu %zu] \n", ranks->rank, rows->idx[0],
           rows->idx[1], cols->idx[0], cols->idx[1]);
    printf("%d: idx matrix [%zu %zu], [%zu %zu] \n", ranks->rank,
           rowsglo->idx[0], rowsglo->idx[1], colsglo->idx[0], colsglo->idx[1]);
    ranks = ranks->next;
    rows = rows->next;
    cols = cols->next;
    rowsglo = rowsglo->next;
    colsglo = colsglo->next;
  }
}

/**
 * @brief Handles H5 files and blocks to read.
 */
typedef struct Hdf5Blocks Hdf5Blocks;
struct Hdf5Blocks {
  // number of blocks, in rows and columns, e.g. 3 means the global
  // matrix is made of 3x3 blocks
  int blockNumber1D;
  // upper/lower part of the matrix the blocks represent
  std::string format;
  // number of blocks to read
  // this depends on the format UPPER, LOWER, FULL
  // e.g. blockNumber1D = 3, format = UPPER then blockNumber = 6
  int blockNumber;
  // HDF5 filename of each block
  std::vector<std::string> h5FileNames;
  // HDF5 file identifier for each block
  std::vector<hid_t> Files;
  // HDF5 datasetname to consider for each block
  std::vector<std::string> dataSetNames;
  // HDF5 datasets to consider for each block
  std::vector<hid_t> dataSets;
  // HDF5 datasets to consider for each block
  std::vector<hid_t> dataSpaces;
  // dimensions (rows, columns) of the global matrix
  std::array<hsize_t, 2> dimsglob;
  // dimensions (rows, columns) of each submatrix
  std::vector<std::array<hsize_t, 2>> dims;
  // indexes of the block
  std::vector<std::pair<int, int>> index;
  // global indexes (i,j) of the first element of each block
  std::vector<std::pair<hsize_t, hsize_t>> indexStart;
  // global indexes (i,j) of the last element of each block
  std::vector<std::pair<hsize_t, hsize_t>> indexEnd;
  // the subset of MPI processes handling the HDF5 file
  MapH5Mpi *maph5mpi;
  // array of starpu handles for H5 sub blocks
  starpu_data_handle_t *handlesH5;
  // starpu codelets (functions on tasks)
  struct starpu_codelet *cl_inittile;
  struct starpu_codelet *cl_readh5;
  struct starpu_codelet *cl_endhdf5;
  struct starpu_codelet *cl_copypaneltotile;
  ~Hdf5Blocks()
  {
    #ifndef FMR_DLOPEN_HDF5
    for (std::vector<long int>::size_type i = 0; i < dataSpaces.size(); i++) {
      if ( dataSpaces[i] > 0 ) {
        H5Sclose(dataSpaces[i]);
      }
    }
    for (std::vector<long int>::size_type i = 0; i < dataSets.size(); i++) {
      if ( dataSets[i] > 0) {
        H5Dclose(dataSets[i]);
      }
    }
    for (std::vector<long int>::size_type i = 0; i < Files.size(); i++) {
      if ( Files[i] > 0 ) {
        H5Fclose(Files[i]);
      }
    }
    #endif
    delete[] maph5mpi;
    delete[] handlesH5;
  }
};

/**
 * @brief Initialize pointers members of Hdf5Blocks to nullptr
 * @param[in] h5blocks structure to initialize
 */
void InitHdf5Blocks(Hdf5Blocks *h5blocks) {
  h5blocks->maph5mpi = nullptr;
  h5blocks->handlesH5 = nullptr;
  h5blocks->cl_inittile = nullptr;
  h5blocks->cl_readh5 = nullptr;
  h5blocks->cl_copypaneltotile = nullptr;
}
#else
typedef int Hdf5Blocks;
void InitHdf5Blocks(int *h5blocks) { *h5blocks = 0; }
#endif // FMR_HDF5

// all chameleon algorithms are called ensuring #threads MKL is 1
// we also prevent from runtime active thread polling with Pause/Resume
#if defined(FMR_USE_MKL_AS_BLAS)
#define PASTE_CODE_CHAMELEON(_algo_)                                           \
  {                                                                            \
    int nt = mkl_get_max_threads();                                            \
    mkl_set_num_threads(1);                                                    \
    int ret = _algo_;                                                          \
    mkl_set_num_threads(nt);                                                   \
    return ret;                                                                \
  }
#elif defined(FMR_USE_OPENBLAS_AS_BLAS)
#define PASTE_CODE_CHAMELEON(_algo_)                                           \
  {                                                                            \
    int nt = openblas_get_num_threads();                                       \
    openblas_set_num_threads(1);                                               \
    int ret = _algo_;                                                          \
    openblas_set_num_threads(nt);                                              \
    return ret;                                                                \
  }
#else
#define PASTE_CODE_CHAMELEON(_algo_)                                           \
  {                                                                            \
    int ret = _algo_;                                                          \
    return ret;                                                                \
  }
#endif
#if defined(FMR_USE_MKL_AS_BLAS)
#define PASTE_CODE_CHAMELEON_FLOAT(_algo_)                                     \
  {                                                                            \
    int nt = mkl_get_max_threads();                                            \
    mkl_set_num_threads(1);                                                    \
    float ret = _algo_;                                                        \
    mkl_set_num_threads(nt);                                                   \
    return ret;                                                                \
  }
#define PASTE_CODE_CHAMELEON_DOUBLE(_algo_)                                    \
  {                                                                            \
    int nt = mkl_get_max_threads();                                            \
    mkl_set_num_threads(1);                                                    \
    double ret = _algo_;                                                       \
    mkl_set_num_threads(nt);                                                   \
    return ret;                                                                \
  }
#elif defined(FMR_USE_OPENBLAS_AS_BLAS)
#define PASTE_CODE_CHAMELEON_FLOAT(_algo_)                                     \
  {                                                                            \
    int nt = openblas_get_num_threads();                                       \
    openblas_set_num_threads(1);                                               \
    float ret = _algo_;                                                        \
    openblas_set_num_threads(nt);                                              \
    return ret;                                                                \
  }
#define PASTE_CODE_CHAMELEON_DOUBLE(_algo_)                                    \
  {                                                                            \
    int nt = openblas_get_num_threads();                                       \
    openblas_set_num_threads(1);                                               \
    double ret = _algo_;                                                       \
    openblas_set_num_threads(nt);                                              \
    return ret;                                                                \
  }
#else
#define PASTE_CODE_CHAMELEON_FLOAT(_algo_)                                     \
  {                                                                            \
    float ret = _algo_;                                                        \
    return ret;                                                                \
  }
#define PASTE_CODE_CHAMELEON_DOUBLE(_algo_)                                    \
  {                                                                            \
    double ret = _algo_;                                                       \
    return ret;                                                                \
  }
#endif

/** @brief Chameleon C++ functions */
namespace Chameleon {

/* Control */
inline int version(int *ver_major, int *ver_minor, int *ver_micro) {
  return CHAMELEON_Version(ver_major, ver_minor, ver_micro);
}
inline int initialized() { return CHAMELEON_Initialized(); }
inline int init(int nworkers, int ncudas) {
  return CHAMELEON_Init(nworkers, ncudas);
}
inline int finalize(void) {
  CHAMELEON_Resume();
  return CHAMELEON_Finalize();
}
inline int pause(void) { return CHAMELEON_Pause(); }
inline int resume(void) { return CHAMELEON_Resume(); }
inline int comm_size(void) { return CHAMELEON_Comm_size(); }
inline int comm_rank(void) { return CHAMELEON_Comm_rank(); }
inline int distributed_start(void) { return CHAMELEON_Distributed_start(); }
inline int distributed_stop(void) { return CHAMELEON_Distributed_stop(); }
inline int get_thread_nbr(void) { return CHAMELEON_GetThreadNbr(); }

/* Options */
inline int enable(int option) { return CHAMELEON_Enable(option); }
inline int disable(int option) { return CHAMELEON_Disable(option); }
inline int set(int param, int value) { return CHAMELEON_Set(param, value); }
inline int get(int param, int *value) { return CHAMELEON_Get(param, value); }
inline ssize_t element_size(cham_flttype_t type) { return CHAMELEON_Element_Size(type); }
inline int set_update_progress_callback(void (*p)(int, int)) {
  return CHAMELEON_Set_update_progress_callback(p);
}

/* Descriptor */
inline int desc_create(CHAM_desc_t **desc, void *mat, cham_flttype_t dtyp,
                       int mb, int nb, int bsiz, int lm, int ln, int i, int j,
                       int m, int n, int p, int q) {
  return CHAMELEON_Desc_Create(desc, mat, dtyp, mb, nb, bsiz, lm, ln, i, j, m,
                               n, p, q);
}
inline int desc_create_user(CHAM_desc_t **desc, void *mat, cham_flttype_t dtyp,
                            int mb, int nb, int bsiz, int lm, int ln, int i,
                            int j, int m, int n, int p, int q,
                            void *(*get_blkaddr)(const CHAM_desc_t *, int, int),
                            int (*get_blkldd)(const CHAM_desc_t *, int),
                            int (*get_rankof)(const CHAM_desc_t *, int, int)) {
  return CHAMELEON_Desc_Create_User(desc, mat, dtyp, mb, nb, bsiz, lm, ln, i, j,
                                    m, n, p, q, get_blkaddr, get_blkldd,
                                    get_rankof, NULL);
}
inline int desc_create_ooc(CHAM_desc_t **desc, cham_flttype_t dtyp, int mb,
                           int nb, int bsiz, int lm, int ln, int i, int j,
                           int m, int n, int p, int q) {
  return CHAMELEON_Desc_Create_OOC(desc, dtyp, mb, nb, bsiz, lm, ln, i, j, m, n,
                                   p, q);
}
inline int desc_create_ooc_user(CHAM_desc_t **desc, cham_flttype_t dtyp, int mb,
                                int nb, int bsiz, int lm, int ln, int i, int j,
                                int m, int n, int p, int q,
                                int (*get_rankof)(const CHAM_desc_t *, int,
                                                  int)) {
  return CHAMELEON_Desc_Create_OOC_User(desc, dtyp, mb, nb, bsiz, lm, ln, i, j,
                                        m, n, p, q, get_rankof, NULL);
}
inline int desc_destroy(CHAM_desc_t **desc) {
  return CHAMELEON_Desc_Destroy(desc);
}
inline int desc_acquire(CHAM_desc_t *desc) {
  return CHAMELEON_Desc_Acquire(desc);
}
inline int desc_release(CHAM_desc_t *desc) {
  return CHAMELEON_Desc_Release(desc);
}
inline int desc_flush(CHAM_desc_t *desc, RUNTIME_sequence_t *sequence) {
  return CHAMELEON_Desc_Flush(desc, sequence);
}
inline CHAM_desc_t *desc_submatrix(CHAM_desc_t *descA, int i, int j, int m,
                                   int n) {
  return CHAMELEON_Desc_SubMatrix(descA, i, j, m, n);
}
inline CHAM_desc_t *desc_copyonzero(CHAM_desc_t *descA, void *mat) {
  return CHAMELEON_Desc_CopyOnZero(descA, mat);
}
inline void CHAMELEON_user_tag_size(int user_tag_width, int user_tag_sep) {
  CHAMELEON_user_tag_size(user_tag_width, user_tag_sep);
}

/* Workspaces */
inline void *gemm_ws_alloc(cham_trans_t transA, cham_trans_t transB,
                           const CHAM_desc_t *A, const CHAM_desc_t *B,
                           const CHAM_desc_t *C) {
  switch (C->dtyp) {
  case ChamRealFloat:
    return CHAMELEON_sgemm_WS_Alloc(transA, transB, A, B, C);
  case ChamRealDouble:
    return CHAMELEON_dgemm_WS_Alloc(transA, transB, A, B, C);
  default:
    chameleon_error("gemm_ws_alloc", "data type not supported, must be float or double");
    return nullptr;
  }
}
inline void gemm_ws_free(void *ws) { CHAMELEON_sgemm_WS_Free(ws); }

inline void *symm_ws_alloc(cham_side_t side, cham_uplo_t uplo,
                           const CHAM_desc_t *A, const CHAM_desc_t *B,
                           const CHAM_desc_t *C) {
  switch (C->dtyp) {
  case ChamRealFloat:
    return CHAMELEON_ssymm_WS_Alloc(side, uplo, A, B, C);
  case ChamRealDouble:
    return CHAMELEON_dsymm_WS_Alloc(side, uplo, A, B, C);
  default:
    chameleon_error("symm_ws_alloc", "data type not supported, must be float or double");
    return nullptr;
  }
}
inline void symm_ws_free(void *ws) { CHAMELEON_ssymm_WS_Free(ws); }

inline void *cesca_ws_alloc(const CHAM_desc_t *A) {
  switch (A->dtyp) {
  case ChamRealFloat:
    return CHAMELEON_scesca_WS_Alloc(A);
  case ChamRealDouble:
    return CHAMELEON_dcesca_WS_Alloc(A);
  default:
    chameleon_error("cesca_ws_alloc", "data type not supported, must be float or double");
    return nullptr;
  }
}
inline void cesca_ws_free(void *ws) { CHAMELEON_scesca_WS_Free(ws); }

inline void *gram_ws_alloc(const CHAM_desc_t *A) {
  switch (A->dtyp) {
  case ChamRealFloat:
    return CHAMELEON_sgram_WS_Alloc(A);
  case ChamRealDouble:
    return CHAMELEON_dgram_WS_Alloc(A);
  default:
    chameleon_error("gram_ws_alloc", "data type not supported, must be float or double");
    return nullptr;
  }
}
inline void gram_ws_free(void *ws) { CHAMELEON_sgram_WS_Free(ws); }

inline int dealloc_workspace(CHAM_desc_t **desc) {
  return CHAMELEON_Dealloc_Workspace(desc);
}

/* Sequences */
inline int sequence_create(RUNTIME_sequence_t **sequence) {
  return CHAMELEON_Sequence_Create(sequence);
}
inline int sequence_destroy(RUNTIME_sequence_t *sequence) {
  return CHAMELEON_Sequence_Destroy(sequence);
}
inline int sequence_wait(RUNTIME_sequence_t *sequence) {
  return CHAMELEON_Sequence_Wait(sequence);
}

/* Requests */
inline int request_create(RUNTIME_request_t **request) {
  return CHAMELEON_Request_Create(request);
}
inline int request_destroy(RUNTIME_request_t *request) {
  return CHAMELEON_Request_Destroy(request);
}
inline int request_set(RUNTIME_request_t *request, int param, int value) {
  return CHAMELEON_Request_Set(request, param, value);
}

/* Runtime */
inline void start_profiling() { RUNTIME_start_profiling(); }
inline void stop_profiling() { RUNTIME_stop_profiling(); }
inline void *runtime_data_getaddr(const CHAM_desc_t *A, int Am, int An) {
  return RUNTIME_data_getaddr(A, Am, An);
}

/**
 * @brief To enable use of the asynchronous interface
 *
 * @param[in] async if true enables asynchronous
 * @param[in] sync synchronization i.e. runtime barrier, we wait for tasks
 completion
 * @param[in] destroy deallocate memory for sequence and request
 * @param[out] asyncout returns the async status parameter
 * @param[out] sequence returns the sequence pointer
 * @param[out] request returns the request pointer

 *
 * @returns success/failure status
 */
int async_manager(bool async, bool sync, bool destroy, bool *asyncout,
                  RUNTIME_sequence_t **sequence, RUNTIME_request_t **request) {
  static bool async_static = false;
  static RUNTIME_sequence_t *sequence_static = NULL;
  static RUNTIME_request_t *request_static = NULL;

  // user asks to enable the asynchronous chameleon interface
  if (async) {
    async_static = true;
  }

  // consider the env. var. if set, take precedance
  const char *cham_async_s = getenv("FMR_CHAMELEON_ASYNC");
  if (cham_async_s != NULL) {
    async_static = (bool)std::stoi(cham_async_s);
  }

  if (async_static) {
    // create sequence and request if not already done
    if (sequence_static == NULL) {
      sequence_create(&sequence_static);
      request_create(&request_static);
    }
    // user asks for synchronization
    if (sync) {
      sequence_wait(sequence_static);
    }
  }

  // user asks for deallocation
  if (destroy) {
    if (sequence_static != NULL) {
      sequence_wait(sequence_static);
      if (request_static != NULL) {
        request_destroy(request_static);
        request_static = NULL;
      }
      sequence_destroy(sequence_static);
      sequence_static = NULL;
    }
  }

  // return the static values
  if (asyncout != NULL) {
    *asyncout = async_static;
  }
  if (sequence != NULL && sequence_static != NULL) {
    *sequence = sequence_static;
  }
  if (request != NULL && request_static != NULL) {
    *request = request_static;
  }

  return EXIT_SUCCESS;
}

inline int mpi_rank()
{
  int rank = 0;
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
  int mpiinit;
  MPI_Initialized(&mpiinit);
  if ( !(mpiinit) ){
    MPI_Init(NULL, NULL);
  }
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif
  return rank;
}

inline int mpi_nprocs()
{
  int nprocs = 1;
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
  int mpiinit;
  MPI_Initialized(&mpiinit);
  if ( !(mpiinit) ){
    MPI_Init(NULL, NULL);
  }
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
#endif
  return nprocs;
}

inline bool mpi_isinit()
{
  int mpiinit = 0;
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
  MPI_Initialized(&mpiinit);
#endif
  if ( mpiinit == 0 ){
      return false;
  } else {
      return true;
  }
}

void barrier() {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    Chameleon::async_manager(false, true, false, NULL, NULL, NULL);
  }
}

inline bool is_divisible( int numerator, int denominator ){
  if ( numerator <= 0 || denominator <= 0 ) {
    chameleon_error("is_divisible", "numerator and denominator must be strictly positive.");
    return CHAMELEON_ERR_ILLEGAL_VALUE;
  }
  if ( denominator > numerator ) {
    return false;
  }
  if ( numerator % denominator == 0 ) {
    return true;
  } else {
    return false;
  }
}

inline int mapping_2dbc_p( int p_in, int m, int n ){
  int np = mpi_nprocs();
  if ( np == 1 || m == 0 || n == 0 ) {
      return np;
  }
  if ( p_in > 0) {
      return p_in;
  }
  int p;
  /* 2DBC mapping can be given with env. var. */
  const char *cham_2dbc_p_s = getenv("FMR_CHAMELEON_2DBC_P");
  if (cham_2dbc_p_s != NULL) {
    p = std::stoi(cham_2dbc_p_s);
    if ( np % p != 0 ) {
      chameleon_error("mapping_2dbc_p", "MPI np is not divisible by p");
      return CHAMELEON_ERR_ILLEGAL_VALUE;
    }
  } else {
    /* if p not given default value of p is computed in function of (mt,nt) */
    //int tilesize;
    //get(CHAMELEON_TILE_SIZE, &tilesize);
    //int mt = ( m + tilesize - 1) / tilesize ;
    //int nt = ( n + tilesize - 1) / tilesize ;
    //int tref = -1;
    //int pref = np;
    //int r1;
    //if ( mt >= nt ) {
    //  r1 = mt / nt;
    //} else {
    //  r1 = nt / mt;
    //}
    //for (int i=1; i<=np; i++) {
    //  if ( is_divisible( np, i ) ) {
    //    int q;
    //    int r2;
    //    if ( mt >= nt ) {
    //      p = np / i;
    //      q = np / p;
    //      r2 = p / q;
    //    } else {
    //      q = np / i;
    //      p = np / q;
    //      r2 = q / p;
    //    }
    //    int t = std::abs( r1 - r2 );
    //    if ( t < tref || tref == -1 ) {
    //      tref = t;
    //      pref = p;
    //    }
    //  }
    //}
    //p = pref;
    if (n > m){
      p = 1;
    } else {
      p = np;
    }
  }
  return p;
}

inline int mapping_2dbc_q( int p_in, int m, int n ){
  int np = mpi_nprocs();
  int p = mapping_2dbc_p( p_in, m, n );
  if ( np % p != 0 ) {
    chameleon_error("mapping_2dbc_q", "MPI np is not divisible by p");
    return CHAMELEON_ERR_ILLEGAL_VALUE;
  } else {
    return (np / p);
  }
}

inline int mapping_sbc_r(){
  int np = mpi_nprocs();
  if ( np == 1 ) {
      return 0;
  }
  const char *cham_sbc_s = getenv("FMR_CHAMELEON_SBC");
  if (cham_sbc_s != NULL) {
    if ( std::stoi(cham_sbc_s) == 0 ) {
      return 0;
    }
    /* mapping sbc enabled */
    int sbcr;
    int sbcr_check;
    const char *cham_sbc_r_s = getenv("FMR_CHAMELEON_SBC_R");
    if (cham_sbc_r_s != NULL) {
      sbcr = std::stoi(cham_sbc_r_s);
    } else {
      sbcr = (1+std::sqrt(8*np+1))/2;
    }
    sbcr_check = (sbcr*(sbcr-1))/2;
    if ( sbcr_check != np ) {
        fprintf(stderr, "ERROR: SBC mapping requires that MPI processes NP=R(R-1)/2."
        " Here NP=%d, but R=%d and R(R-1)/2=%d.\n", np, sbcr, sbcr_check);
        exit(1);
    }
    return sbcr;
  } else {
    return 0;
  }
}

inline bool mapping_tbc(){
  const char *cham_tbc_s = getenv("FMR_CHAMELEON_TBC");
  if (cham_tbc_s != NULL) {
    if ( std::stoi(cham_tbc_s) == 1 ) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

/**
 *  Function to restore the default distribution, well suited for chameleon
 *  algorithms, here 2d block cyclic.
 */
void restore_2dcyclic(cham_uplo_t uplo, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (!async) {
    Chameleon::sequence_create(&sequence);
  }
  int p = Chameleon::mapping_2dbc_p( 0, A->m, A->n );
  int q = Chameleon::mapping_2dbc_q( 0, A->m, A->n );
  cham_data_dist_t dist = {
      .get_distrib = (datadist_access_fct_t)chameleon_get_2d_block_cyclic,
      .distrib_array_size = 2,
      .distrib = {p, q} };
  chameleon_desc_set_datadist( A, &dist );
  for (int n = 0; n < A->nt; n++) {
    RUNTIME_data_migrate(sequence, A, n, n, chameleon_getrankof_2d(A, n, n));
    int mmin = (uplo == ChamLower) ? n + 1 : 0;
    int mmax = (uplo == ChamUpper) ? std::min(n + 1, A->mt) : A->mt;
    for (int m = mmin; m < mmax; m++) {
      RUNTIME_data_migrate(sequence, A, m, n, chameleon_getrankof_2d(A, m, n));
    }
  }
  A->get_rankof = chameleon_getrankof_2d;
  Chameleon::desc_flush(A, sequence);
  if (!async) {
    Chameleon::sequence_wait(sequence);
    Chameleon::sequence_destroy(sequence);
  }
}

/**
 *  Function to restore the default distribution, well suited for chameleon
 *  algorithms, here sbc in the symmetric case.
 */
void restore_sbc(cham_uplo_t uplo, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (!async) {
    Chameleon::sequence_create(&sequence);
  }
  int np = mpi_nprocs();
  int r = Chameleon::mapping_sbc_r();
  int p = r;
  int q = np / r;
  cham_data_dist_t dist = {
      .get_distrib = (datadist_access_fct_t)chameleon_get_2d_block_cyclic,
      .distrib_array_size = 2,
      .distrib = {p, q} };
  chameleon_desc_set_datadist( A, &dist );
  for (int n = 0; n < A->nt; n++) {
    RUNTIME_data_migrate(sequence, A, n, n, getrankof_sbc(A, n, n));
    int mmin = (uplo == ChamLower) ? n + 1 : 0;
    int mmax = (uplo == ChamUpper) ? std::min(n + 1, A->mt) : A->mt;
    for (int m = mmin; m < mmax; m++) {
      RUNTIME_data_migrate(sequence, A, m, n, getrankof_sbc(A, m, n));
    }
  }
  A->get_rankof = getrankof_sbc;
  Chameleon::desc_flush(A, sequence);
  if (!async) {
    Chameleon::sequence_wait(sequence);
    Chameleon::sequence_destroy(sequence);
  }
}

/* Load mapping (e.g. tbc) from file */
inline void load_mapping_from_file(int* mapping, const char* file){
  FILE * f = fopen ( file, "r" );
  int M, N;
  fscanf(f, "%d %d", &M, &N);
  for (int m=0; m < M; m++) {
    for (int n=0; n < N; n++) {
      fscanf(f, "%d", &mapping[n*M+m]);
    }
  }
  fclose(f);
}

/**
 *  Function to restore the default distribution, well suited for chameleon
 *  algorithms, here tbc in the symmetric case.
 */
void restore_tbc(cham_uplo_t uplo, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (!async) {
    Chameleon::sequence_create(&sequence);
  }

  chameleon_freerankoftiles_tbc(A->lmt);
  chameleon_rankofTiles_tbc = new int*[A->lmt];
  for (int i=0; i < A->lmt; i++){
      chameleon_rankofTiles_tbc[i] = new int[A->lnt];
      for (int j=0; j < A->lnt; j++){
          chameleon_rankofTiles_tbc[i][j] = -1;
      }
  }
  chameleon_isAllocatedRankofTiles_tbc = true;
  char *cham_tbc_file_s;
  if ( getenv("FMR_CHAMELEON_TBC_FILE") ) {
      cham_tbc_file_s = getenv("FMR_CHAMELEON_TBC_FILE");
  } else {
      std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
      std::cerr << "[fmr] FMR_CHAMELEON_TBC_FILE environment variable must be set for TBC matrix mapping" << std::endl;
      exit(EXIT_FAILURE);
  }
  if (mpi_rank() == 0) printf("[fmr] TBC mapping file used is %s\n", cham_tbc_file_s);
  FILE * f = fopen ( cham_tbc_file_s, "r" );
  int MT, NT;
  fscanf(f, "%d %d", &MT, &NT);
  for (int m=0; m < MT; m++) {
    for (int n=0; n < NT; n++) {
      fscanf(f, "%d", &chameleon_rankofTiles_tbc[m][n]);
    }
  }
  fclose(f);

  for (int n = 0; n < A->nt; n++) {
    RUNTIME_data_migrate(sequence, A, n, n, getrankof_tbc(A, n, n));
    int mmin = (uplo == ChamLower) ? n + 1 : 0;
    int mmax = (uplo == ChamUpper) ? std::min(n + 1, A->mt) : A->mt;
    for (int m = mmin; m < mmax; m++) {
      RUNTIME_data_migrate(sequence, A, m, n, getrankof_tbc(A, m, n));
    }
  }
  A->get_rankof = getrankof_tbc;
  Chameleon::desc_flush(A, sequence);
  if (!async) {
    Chameleon::sequence_wait(sequence);
    Chameleon::sequence_destroy(sequence);
  }
}

/* Tile */
inline int lapack_to_desc(cham_uplo_t uplo, float *Af77, int LDA,
                          CHAM_desc_t *A) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sLap2Desc(uplo, Af77, LDA, A))
}
inline int lapack_to_desc(cham_uplo_t uplo, double *Af77, int LDA,
                          CHAM_desc_t *A) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dLap2Desc(uplo, Af77, LDA, A))
}
inline int desc_to_lapack(cham_uplo_t uplo, CHAM_desc_t *A, float *Af77,
                          int LDA) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sDesc2Lap(uplo, A, Af77, LDA))
}
inline int desc_to_lapack(cham_uplo_t uplo, CHAM_desc_t *A, double *Af77,
                          int LDA) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dDesc2Lap(uplo, A, Af77, LDA))
}

/**
 *  Math functions
 */

/* LAPACK interface */
inline int lacpy(cham_uplo_t uplo, int M, int N, float *A, int LDA, float *B,
                 int LDB) {
  PASTE_CODE_CHAMELEON(CHAMELEON_slacpy(uplo, M, N, A, LDA, B, LDB))
}
inline int lacpy(cham_uplo_t uplo, int M, int N, double *A, int LDA, double *B,
                 int LDB) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dlacpy(uplo, M, N, A, LDA, B, LDB))
}
/* Chameleon Asynchronous interface */
inline int lacpy(cham_uplo_t uplo, CHAM_desc_t *A, CHAM_desc_t *B,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_slacpy_Tile_Async(uplo, A, B, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dlacpy_Tile_Async(uplo, A, B, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_lacpy_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int lacpy(cham_uplo_t uplo, CHAM_desc_t *A, CHAM_desc_t *B) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return lacpy(uplo, A, B, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_slacpy_Tile(uplo, A, B))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dlacpy_Tile(uplo, A, B))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_lacpy_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline int laset(cham_uplo_t uplo, int M, int N, float alpha, float beta,
                 float *A, int LDA) {
  PASTE_CODE_CHAMELEON(CHAMELEON_slaset(uplo, M, N, alpha, beta, A, LDA))
}
inline int laset(cham_uplo_t uplo, int M, int N, double alpha, double beta,
                 double *A, int LDA) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dlaset(uplo, M, N, alpha, beta, A, LDA))
}
/* Chameleon Asynchronous interface */
inline int laset(cham_uplo_t uplo, float alpha, float beta, CHAM_desc_t *A,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_slaset_Tile_Async(uplo, alpha, beta, A, sequence, request))
}
inline int laset(cham_uplo_t uplo, double alpha, double beta, CHAM_desc_t *A,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dlaset_Tile_Async(uplo, alpha, beta, A, sequence, request))
}
/* Chameleon interface */
inline int laset(cham_uplo_t uplo, float alpha, float beta, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return laset(uplo, alpha, beta, A, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(CHAMELEON_slaset_Tile(uplo, alpha, beta, A))
  }
}
inline int laset(cham_uplo_t uplo, double alpha, double beta, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return laset(uplo, alpha, beta, A, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(CHAMELEON_dlaset_Tile(uplo, alpha, beta, A))
  }
}

/* LAPACK interface */
inline int plrnt(int M, int N, float *A, int LDA, unsigned long long int seed) {
  PASTE_CODE_CHAMELEON(CHAMELEON_splrnt(M, N, A, LDA, seed))
}
inline int plrnt(int M, int N, double *A, int LDA,
                 unsigned long long int seed) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dplrnt(M, N, A, LDA, seed))
}
/* Chameleon Asynchronous interface */
inline int plrnt(CHAM_desc_t *A, unsigned long long int seed,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_splrnt_Tile_Async(A, seed, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dplrnt_Tile_Async(A, seed, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_plrnt_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int plrnt(CHAM_desc_t *A, unsigned long long int seed) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return plrnt(A, seed, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_splrnt_Tile(A, seed))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dplrnt_Tile(A, seed))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_plrnt_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline int cesca(int center, int scale, cham_store_t axis, int M, int N,
                 float *A, int LDA) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_scesca(center, scale, axis, M, N, A, LDA, NULL, NULL))
}
inline int cesca(int center, int scale, cham_store_t axis, int M, int N,
                 double *A, int LDA) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dcesca(center, scale, axis, M, N, A, LDA, NULL, NULL))
}
inline int cesca(int center, int scale, cham_store_t axis, int M, int N,
                 float *A, int LDA, float *SR, float *SC) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_scesca(center, scale, axis, M, N, A, LDA, SR, SC))
}
inline int cesca(int center, int scale, cham_store_t axis, int M, int N,
                 double *A, int LDA, double *SR, double *SC) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dcesca(center, scale, axis, M, N, A, LDA, SR, SC))
}
/* Chameleon Asynchronous interface */
inline int cesca(int center, int scale, cham_store_t axis, CHAM_desc_t *A,
                 void *ws, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(CHAMELEON_scesca_Tile_Async(center, scale, axis, A, ws,
                                                     sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(CHAMELEON_dcesca_Tile_Async(center, scale, axis, A, ws,
                                                     sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d_cesca_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
struct chameleon_pzcesca_s {
  CHAM_desc_t Wgcol;
  CHAM_desc_t Wgrow;
  CHAM_desc_t Wgelt;
  CHAM_desc_t Wdcol;
  CHAM_desc_t Wdrow;
};
inline int cesca(int center, int scale, cham_store_t axis, CHAM_desc_t *A,
                 float *SR, float *SC, void *ws, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  int err;
  err = CHAMELEON_scesca_Tile_Async(center, scale, axis, A, ws, sequence,
                                    request);
  /* pre-coa case : save the sums over rows and columns */
  struct chameleon_pzcesca_s *wsl = (struct chameleon_pzcesca_s *)ws;
  CHAM_desc_t *descSR = CHAMELEON_Desc_SubMatrix(&(wsl->Wgrow), 0, 0, A->lm, 1);
  CHAM_desc_t *descSC = CHAMELEON_Desc_SubMatrix(&(wsl->Wgcol), 0, 0, 1, A->ln);
  CHAMELEON_sDesc2Lap(ChamUpperLower, descSR, SR, A->lm);
  CHAMELEON_sDesc2Lap(ChamUpperLower, descSC, SC, 1);
  barrier();
  return err;
}
inline int cesca(int center, int scale, cham_store_t axis, CHAM_desc_t *A,
                 double *SR, double *SC, void *ws, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  int err;
  err = CHAMELEON_dcesca_Tile_Async(center, scale, axis, A, ws, sequence,
                                    request);
  /* pre-coa case : save the sums over rows and columns */
  struct chameleon_pzcesca_s *wsl = (struct chameleon_pzcesca_s *)ws;
  CHAM_desc_t *descSR = CHAMELEON_Desc_SubMatrix(&(wsl->Wgrow), 0, 0, A->lm, 1);
  CHAM_desc_t *descSC = CHAMELEON_Desc_SubMatrix(&(wsl->Wgcol), 0, 0, 1, A->ln);
  CHAMELEON_dDesc2Lap(ChamUpperLower, descSR, SR, A->lm);
  CHAMELEON_dDesc2Lap(ChamUpperLower, descSC, SC, 1);
  barrier();
  return err;
}
/* Chameleon interface */
inline int cesca(int center, int scale, cham_store_t axis, CHAM_desc_t *A,
                 void *ws) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return cesca(center, scale, axis, A, ws, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(
          CHAMELEON_scesca_Tile(center, scale, axis, A, NULL, NULL))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(
          CHAMELEON_dcesca_Tile(center, scale, axis, A, NULL, NULL))
    default:
      chameleon_error("CHAMELEON_s|d_cesca_Tile", "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}
inline int cesca(int center, int scale, cham_store_t axis, CHAM_desc_t *A,
                 float *SR, float *SC, void *ws) {
  bool async;
  int err;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    err = cesca(center, scale, axis, A, SR, SC, ws, sequence, request);
  } else {
    err = CHAMELEON_scesca_Tile(center, scale, axis, A, SR, SC);
  }
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
  MPI_Datatype dtype = MPI_FLOAT;
  MPI_Bcast(SR, A->lm, dtype, 0, MPI_COMM_WORLD);
  MPI_Bcast(SC, A->ln, dtype, 0, MPI_COMM_WORLD);
#endif
  barrier();
  return err;
}
inline int cesca(int center, int scale, cham_store_t axis, CHAM_desc_t *A,
                 double *SR, double *SC, void *ws) {
  bool async;
  int err;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    err = cesca(center, scale, axis, A, SR, SC, ws, sequence, request);
  } else {
    err = CHAMELEON_dcesca_Tile(center, scale, axis, A, SR, SC);
  }
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
  MPI_Datatype dtype = MPI_DOUBLE;
  MPI_Bcast(SR, A->lm, dtype, 0, MPI_COMM_WORLD);
  MPI_Bcast(SC, A->ln, dtype, 0, MPI_COMM_WORLD);
#endif
  barrier();
  return err;
}

/* LAPACK interface */
inline int gram(cham_uplo_t uplo, int N, float *A, int LDA) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sgram(uplo, N, A, LDA))
}
inline int gram(cham_uplo_t uplo, int N, double *A, int LDA) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dgram(uplo, N, A, LDA))
}
/* Chameleon Asynchronous interface */
inline int gram(cham_uplo_t uplo, CHAM_desc_t *A, void *ws,
                RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_sgram_Tile_Async(uplo, A, ws, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dgram_Tile_Async(uplo, A, ws, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d_gram_Tile_Async", "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int gram(cham_uplo_t uplo, CHAM_desc_t *A, void *ws) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return gram(uplo, A, ws, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_sgram_Tile(uplo, A))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dgram_Tile(uplo, A))
    default:
      chameleon_error("CHAMELEON_s|d_gram_Tile", "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline int gemm(cham_trans_t transA, cham_trans_t transB, int M, int N, int K,
                float alpha, float *A, int LDA, float *B, int LDB, float beta,
                float *C, int LDC) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sgemm(transA, transB, M, N, K, alpha, A, LDA,
                                       B, LDB, beta, C, LDC))
}
inline int gemm(cham_trans_t transA, cham_trans_t transB, int M, int N, int K,
                double alpha, double *A, int LDA, double *B, int LDB,
                double beta, double *C, int LDC) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dgemm(transA, transB, M, N, K, alpha, A, LDA,
                                       B, LDB, beta, C, LDC))
}
/* Chameleon Asynchronous interface */
inline int gemm(cham_trans_t transA, cham_trans_t transB, float alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, float beta, CHAM_desc_t *C,
                void *ws, RUNTIME_sequence_t *sequence,
                RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sgemm_Tile_Async(
      transA, transB, alpha, A, B, beta, C, ws, sequence, request))
}
inline int gemm(cham_trans_t transA, cham_trans_t transB, double alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, double beta, CHAM_desc_t *C,
                void *ws, RUNTIME_sequence_t *sequence,
                RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dgemm_Tile_Async(
      transA, transB, alpha, A, B, beta, C, ws, sequence, request))
}
/* Chameleon interface */
inline int gemm(cham_trans_t transA, cham_trans_t transB, float alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, float beta, CHAM_desc_t *C,
                void *ws) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return gemm(transA, transB, alpha, A, B, beta, C, ws, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(
        CHAMELEON_sgemm_Tile(transA, transB, alpha, A, B, beta, C))
  }
}
inline int gemm(cham_trans_t transA, cham_trans_t transB, double alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, double beta, CHAM_desc_t *C,
                void *ws) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return gemm(transA, transB, alpha, A, B, beta, C, ws, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dgemm_Tile(transA, transB, alpha, A, B, beta, C))
  }
}

/* LAPACK interface */
inline int symm(cham_side_t side, cham_uplo_t uplo, int M, int N, float alpha,
                float *A, int LDA, float *B, int LDB, float beta, float *C,
                int LDC) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_ssymm(side, uplo, M, N, alpha, A, LDA, B, LDB, beta, C, LDC))
}
inline int symm(cham_side_t side, cham_uplo_t uplo, int M, int N, double alpha,
                double *A, int LDA, double *B, int LDB, double beta, double *C,
                int LDC) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dsymm(side, uplo, M, N, alpha, A, LDA, B, LDB, beta, C, LDC))
}
/* Chameleon Asynchronous interface */
inline int symm(cham_side_t side, cham_uplo_t uplo, float alpha, CHAM_desc_t *A,
                CHAM_desc_t *B, float beta, CHAM_desc_t *C, void *ws,
                RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(CHAMELEON_ssymm_Tile_Async(side, uplo, alpha, A, B, beta,
                                                  C, ws, sequence, request))
}
inline int symm(cham_side_t side, cham_uplo_t uplo, double alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, double beta, CHAM_desc_t *C, void *ws,
                RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dsymm_Tile_Async(side, uplo, alpha, A, B, beta,
                                                  C, ws, sequence, request))
}
/* Chameleon interface */
inline int symm(cham_side_t side, cham_uplo_t uplo, float alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, float beta, CHAM_desc_t *C,
                void *ws) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return symm(side, uplo, alpha, A, B, beta, C, ws, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(CHAMELEON_ssymm_Tile(side, uplo, alpha, A, B, beta, C))
  }
}
inline int symm(cham_side_t side, cham_uplo_t uplo, double alpha,
                CHAM_desc_t *A, CHAM_desc_t *B, double beta, CHAM_desc_t *C,
                void *ws) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return symm(side, uplo, alpha, A, B, beta, C, ws, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(CHAMELEON_dsymm_Tile(side, uplo, alpha, A, B, beta, C))
  }
}

/* LAPACK interface */
inline int geqrf(int M, int N, float *A, int LDA, CHAM_desc_t *descT) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sgeqrf(M, N, A, LDA, descT))
}
inline int geqrf(int M, int N, double *A, int LDA, CHAM_desc_t *descT) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dgeqrf(M, N, A, LDA, descT))
}
/* Chameleon Asynchronous interface */
inline int geqrf(CHAM_desc_t *A, CHAM_desc_t *T, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(CHAMELEON_sgeqrf_Tile_Async(A, T, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(CHAMELEON_dgeqrf_Tile_Async(A, T, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_geqrf_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int geqrf(CHAM_desc_t *A, CHAM_desc_t *T) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return geqrf(A, T, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_sgeqrf_Tile(A, T))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dgeqrf_Tile(A, T))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_geqrf_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

inline int geqrf_alloc_workspace_tile(int M, int N, CHAM_desc_t **T,
                                      cham_flttype_t dtype, int p, int q) {
  switch (dtype) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(CHAMELEON_Alloc_Workspace_sgeqrf_Tile(M, N, T, p, q))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(CHAMELEON_Alloc_Workspace_dgeqrf_Tile(M, N, T, p, q))
  default:
    chameleon_error("CHAMELEON_Alloc_Workspace_s|d|c|zgeqrf_Tile",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}

/* LAPACK interface */
inline int geqrf_param(libhqr_tree_t *qrtree, int M, int N, float *A, int LDA,
                       CHAM_desc_t *TS, CHAM_desc_t *TT) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sgeqrf_param(qrtree, M, N, A, LDA, TS, TT))
}
inline int geqrf_param(libhqr_tree_t *qrtree, int M, int N, double *A, int LDA,
                       CHAM_desc_t *TS, CHAM_desc_t *TT) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dgeqrf_param(qrtree, M, N, A, LDA, TS, TT))
}
/* Chameleon Asynchronous interface */
inline int geqrf_param(libhqr_tree_t *qrtree, CHAM_desc_t *A, CHAM_desc_t *TS,
                       CHAM_desc_t *TT, RUNTIME_sequence_t *sequence,
                       RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_sgeqrf_param_Tile_Async(qrtree, A, TS, TT, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dgeqrf_param_Tile_Async(qrtree, A, TS, TT, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_geqrf_param_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int geqrf_param(libhqr_tree_t *qrtree, CHAM_desc_t *A, CHAM_desc_t *TS,
                       CHAM_desc_t *TT) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return geqrf_param(qrtree, A, TS, TT, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_sgeqrf_param_Tile(qrtree, A, TS, TT))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dgeqrf_param_Tile(qrtree, A, TS, TT))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_geqrf_param_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

inline int gels_alloc_workspace(int M, int N, CHAM_desc_t **T,
                                cham_flttype_t dtype, int p, int q) {
  switch (dtype) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(CHAMELEON_Alloc_Workspace_sgels(M, N, T, p, q))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(CHAMELEON_Alloc_Workspace_dgels(M, N, T, p, q))
  default:
    chameleon_error("CHAMELEON_Alloc_Workspace_s|d|c|zgels",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}

/* LAPACK interface */
inline int orgqr(int M, int N, int K, float *A, int LDA, CHAM_desc_t *descT,
                 float *B, int LDB) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sorgqr(M, N, K, A, LDA, descT, B, LDB))
}
inline int orgqr(int M, int N, int K, double *A, int LDA, CHAM_desc_t *descT,
                 double *B, int LDB) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dorgqr(M, N, K, A, LDA, descT, B, LDB))
}
/* Chameleon Asynchronous interface */
inline int orgqr(CHAM_desc_t *A, CHAM_desc_t *T, CHAM_desc_t *B,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_sorgqr_Tile_Async(A, T, B, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dorgqr_Tile_Async(A, T, B, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_orgqr_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int orgqr(CHAM_desc_t *A, CHAM_desc_t *T, CHAM_desc_t *B) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return orgqr(A, T, B, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_sorgqr_Tile(A, T, B))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dorgqr_Tile(A, T, B))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_orgqr_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline int orgqr(const libhqr_tree_t *qrtree, int M, int N, int K, float *A,
                 int LDA, CHAM_desc_t *descTS, CHAM_desc_t *descTT, float *B,
                 int LDB) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_sorgqr_param(qrtree, M, N, K, A, LDA, descTS, descTT, B, LDB))
}
inline int orgqr(const libhqr_tree_t *qrtree, int M, int N, int K, double *A,
                 int LDA, CHAM_desc_t *descTS, CHAM_desc_t *descTT, double *B,
                 int LDB) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dorgqr_param(qrtree, M, N, K, A, LDA, descTS, descTT, B, LDB))
}
/* Chameleon Asynchronous interface */
inline int orgqr(const libhqr_tree_t *qrtree, CHAM_desc_t *A, CHAM_desc_t *TS,
                 CHAM_desc_t *TT, CHAM_desc_t *B, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(CHAMELEON_sorgqr_param_Tile_Async(qrtree, A, TS, TT, B,
                                                           sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(CHAMELEON_dorgqr_param_Tile_Async(qrtree, A, TS, TT, B,
                                                           sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_orgqr_param_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int orgqr(const libhqr_tree_t *qrtree, CHAM_desc_t *A, CHAM_desc_t *TS,
                 CHAM_desc_t *TT, CHAM_desc_t *B) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return orgqr(qrtree, A, TS, TT, B, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_sorgqr_param_Tile(qrtree, A, TS, TT, B))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dorgqr_param_Tile(qrtree, A, TS, TT, B))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_orgqr_param_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline int ormqr(cham_side_t side, cham_trans_t trans, int M, int N, int K,
                 float *A, int LDA, CHAM_desc_t *descT, float *B, int LDB) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_sormqr(side, trans, M, N, K, A, LDA, descT, B, LDB))
}
inline int ormqr(cham_side_t side, cham_trans_t trans, int M, int N, int K,
                 double *A, int LDA, CHAM_desc_t *descT, double *B, int LDB) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dormqr(side, trans, M, N, K, A, LDA, descT, B, LDB))
}
/* Chameleon Asynchronous interface */
inline int ormqr(cham_side_t side, cham_trans_t trans, CHAM_desc_t *A,
                 CHAM_desc_t *T, CHAM_desc_t *B, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_sormqr_Tile_Async(side, trans, A, T, B, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(
        CHAMELEON_dormqr_Tile_Async(side, trans, A, T, B, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_ormqr_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int ormqr(cham_side_t side, cham_trans_t trans, CHAM_desc_t *A,
                 CHAM_desc_t *T, CHAM_desc_t *B) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return ormqr(side, trans, A, T, B, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(CHAMELEON_sormqr_Tile(side, trans, A, T, B))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(CHAMELEON_dormqr_Tile(side, trans, A, T, B))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_ormqr_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline int ormqr(const libhqr_tree_t *qrtree, cham_side_t side,
                 cham_trans_t trans, int M, int N, int K, float *A, int LDA,
                 CHAM_desc_t *descTS, CHAM_desc_t *descTT, float *B, int LDB) {
  PASTE_CODE_CHAMELEON(CHAMELEON_sormqr_param(qrtree, side, trans, M, N, K, A,
                                              LDA, descTS, descTT, B, LDB))
}
inline int ormqr(const libhqr_tree_t *qrtree, cham_side_t side,
                 cham_trans_t trans, int M, int N, int K, double *A, int LDA,
                 CHAM_desc_t *descTS, CHAM_desc_t *descTT, double *B, int LDB) {
  PASTE_CODE_CHAMELEON(CHAMELEON_dormqr_param(qrtree, side, trans, M, N, K, A,
                                              LDA, descTS, descTT, B, LDB))
}
/* Chameleon Asynchronous interface */
inline int ormqr(const libhqr_tree_t *qrtree, cham_side_t side,
                 cham_trans_t trans, CHAM_desc_t *A, CHAM_desc_t *TS,
                 CHAM_desc_t *TT, CHAM_desc_t *B, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  switch (A->dtyp) {
  case ChamRealFloat:
    PASTE_CODE_CHAMELEON(CHAMELEON_sormqr_param_Tile_Async(
        qrtree, side, trans, A, TS, TT, B, sequence, request))
  case ChamRealDouble:
    PASTE_CODE_CHAMELEON(CHAMELEON_dormqr_param_Tile_Async(
        qrtree, side, trans, A, TS, TT, B, sequence, request))
  default:
    chameleon_error("CHAMELEON_s|d|c|z_ormqr_param_Tile_Async",
                    "data type not supported, must be float or double");
    return CHAMELEON_ERR_NOT_SUPPORTED;
  }
}
/* Chameleon interface */
inline int ormqr(const libhqr_tree_t *qrtree, cham_side_t side,
                 cham_trans_t trans, CHAM_desc_t *A, CHAM_desc_t *TS,
                 CHAM_desc_t *TT, CHAM_desc_t *B) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return ormqr(qrtree, side, trans, A, TS, TT, B, sequence, request);
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON(
          CHAMELEON_sormqr_param_Tile(qrtree, side, trans, A, TS, TT, B))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON(
          CHAMELEON_dormqr_param_Tile(qrtree, side, trans, A, TS, TT, B))
    default:
      chameleon_error("CHAMELEON_s|d|c|z_ormqr_param_Tile",
                      "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline float lange(cham_normtype_t norm, int M, int N, float *A, int LDA) {
  PASTE_CODE_CHAMELEON_FLOAT(CHAMELEON_slange(norm, M, N, A, LDA))
}
inline double lange(cham_normtype_t norm, int M, int N, double *A, int LDA) {
  PASTE_CODE_CHAMELEON_DOUBLE(CHAMELEON_dlange(norm, M, N, A, LDA))
}
/* Chameleon Asynchronous interface */
inline int lange(cham_normtype_t norm, CHAM_desc_t *A, float *value,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_slange_Tile_Async(norm, A, value, sequence, request))
}
inline int lange(cham_normtype_t norm, CHAM_desc_t *A, double *value,
                 RUNTIME_sequence_t *sequence, RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dlange_Tile_Async(norm, A, value, sequence, request))
}
/* Chameleon interface */
template <typename FReal>
inline FReal lange(cham_normtype_t norm, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    FReal result;
    lange(norm, A, &result, sequence, request);
    return result;
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON_FLOAT(CHAMELEON_slange_Tile(norm, A))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON_DOUBLE(CHAMELEON_dlange_Tile(norm, A))
    default:
      chameleon_error("CHAMELEON_s|d_lange_Tile", "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/* LAPACK interface */
inline float lansy(cham_normtype_t norm, cham_uplo_t uplo, int N, float *A,
                   int LDA) {
  PASTE_CODE_CHAMELEON_FLOAT(CHAMELEON_slansy(norm, uplo, N, A, LDA))
}
inline double lansy(cham_normtype_t norm, cham_uplo_t uplo, int N, double *A,
                    int LDA) {
  PASTE_CODE_CHAMELEON_DOUBLE(CHAMELEON_dlansy(norm, uplo, N, A, LDA))
}
/* Chameleon Asynchronous interface */
inline int lansy(cham_normtype_t norm, cham_uplo_t uplo, CHAM_desc_t *A,
                 float *value, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_slansy_Tile_Async(norm, uplo, A, value, sequence, request))
}
inline int lansy(cham_normtype_t norm, cham_uplo_t uplo, CHAM_desc_t *A,
                 double *value, RUNTIME_sequence_t *sequence,
                 RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_dlansy_Tile_Async(norm, uplo, A, value, sequence, request))
}
/* Chameleon interface */
template <typename FReal>
inline FReal lansy(cham_normtype_t norm, cham_uplo_t uplo, CHAM_desc_t *A) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    FReal result;
    lansy(norm, uplo, A, &result, sequence, request);
    return result;
  } else {
    switch (A->dtyp) {
    case ChamRealFloat:
      PASTE_CODE_CHAMELEON_FLOAT(CHAMELEON_slansy_Tile(norm, uplo, A))
    case ChamRealDouble:
      PASTE_CODE_CHAMELEON_DOUBLE(CHAMELEON_dlansy_Tile(norm, uplo, A))
    default:
      chameleon_error("CHAMELEON_s|dlansy_Tile", "data type not supported, must be float or double");
      return CHAMELEON_ERR_NOT_SUPPORTED;
    }
  }
}

/**
 *  Used to fill blocks of the matrix
 */
/* Chameleon Asynchronous interface */
inline int map(cham_uplo_t uplo,
               int ndata,
               cham_map_data_t *data,
               cham_map_operator_t *op_fct,
               void *op_args,
               RUNTIME_sequence_t *sequence,
               RUNTIME_request_t *request) {
  PASTE_CODE_CHAMELEON(
      CHAMELEON_mapv_Tile_Async(uplo, ndata, data, op_fct, op_args, sequence, request))
}
/* Chameleon interface */
inline int map(cham_uplo_t uplo,
               int ndata,
               cham_map_data_t *data,
               cham_map_operator_t *op_fct,
               void *op_args) {
  bool async;
  RUNTIME_sequence_t *sequence = NULL;
  RUNTIME_request_t *request = NULL;
  async_manager(false, false, false, &async, &sequence, &request);
  if (async) {
    return map(uplo, ndata, data, op_fct, op_args, sequence, request);
  } else {
    PASTE_CODE_CHAMELEON(CHAMELEON_mapv_Tile(uplo, ndata, data, op_fct, op_args))
  }
}

/**
 * @brief Chameleon class to handle init/finalize and some global parameters
 */
class Chameleon {

protected:
  int _ncpus;     // number of worker of type CPU used by Chameleon
  int _ncudas;    // number of worker of type CUDA used by Chameleon
  int _handlempi; // whether or not we handle mpi init/finalize
  int _mpirank;   // my mpirank
  int _mpinp;     // number of mpi processus
  int _tilesize;  // size of the tiles (blocks)

public:
  /**
   *  @brief Constructor
   *
   *   Constructor to initialize Chameleon. Called without parameters, the
   *   number of CPUS and CUDAS worker to use will be set using to use
   *   CHAMELEON_NUM_THREADS and CHAMELEON_NUM_CUDAS environment variables or
   *   set to 1 if these variables do not exist.
   *
   */
  Chameleon()
      : _ncpus(0), _ncudas(0), _handlempi(0), _mpirank(0), _mpinp(0),
        _tilesize(320) {
    initialize();
  }

  /**
   *  @brief Constructor
   *
   *   Constructor to initialize Chameleon.
   *   The number of CPUS to use will be set and number of CUDAS is
   *   0.
   *
   *   @param[in] ncpus number of CPUS worker to use
   *
   */
  Chameleon(int ncpus)
      : _ncpus(ncpus), _ncudas(0), _handlempi(0), _mpirank(0), _mpinp(0),
        _tilesize(320) {
    initialize();
  }

  /**
   *  @brief Constructor
   *
   *   Constructor to initialize Chameleon.
   *   The number of CPUS and CUDAS to use will be set.
   *
   *   @param[in] ncpus number of CPUS worker to use
   *   @param[in] ncudas number of CUDAS worker to use
   *
   */
  Chameleon(int ncpus, int ncudas)
      : _ncpus(ncpus), _ncudas(ncudas), _handlempi(0), _mpirank(0), _mpinp(0),
        _tilesize(320) {
    initialize();
  }

  /**
   *  @brief Constructor
   *
   *   Constructor to initialize Chameleon. The number of CPUS and CUDAS and
   *   TILESIZE to use will be set.
   *
   *   @param[in] ncpus number of CPUS worker to use
   *   @param[in] ncudas number of CUDAS worker to use
   *   @param[in] tilesize tile size to use
   *
   */
  Chameleon(int ncpus, int ncudas, int tilesize)
      : _ncpus(ncpus), _ncudas(ncudas), _handlempi(0), _mpirank(0), _mpinp(0),
        _tilesize(tilesize) {
    initialize();
  }

  /** @brief Destructor */
  ~Chameleon() {
    // if async is enabled we wait for completion of tasks and deallocate
    // sequence/request using the destroy argument
    async_manager(false, false, true, NULL, NULL, NULL);
    stop_profiling();
    finalize();
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    if (_handlempi) {
      MPI_Finalize();
    }
#endif
  }

  /** @brief Get number of CPU workers used by Chameleon */
  int getNcpus() { return _ncpus; }

  /** @brief Get number of CUDA workers used by Chameleon */
  int getNcudas() { return _ncudas; }

  /** @brief Get number of MPI processes used by Chameleon */
  int getMpiNp() { return _mpinp; }

  /** @brief Get the current MPI rnak */
  int getMpiRank() { return _mpirank; }

  /** @brief Get tile size internally used by Chameleon */
  int getTileSize() { return _tilesize; }

  /**
   * @brief Set tile size internally used by Chameleon
   * @param[in] tilesize the tile size to use, i.e. number of rows of the tile
   *            (= number of columns)
   */
  void setTileSize(int tilesize) {
    _tilesize = tilesize;
    set(CHAMELEON_TILE_SIZE, _tilesize);
  }

private:
  /** @brief Initialize the class */
  void initialize() {
    // if chameleon already initialized, finalize it
    if (initialized()) {
      finalize();
    }
    // initialize MPI
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (!(mpiinit)) {
      MPI_Init(NULL, NULL);
      _handlempi = 1;
    }
    MPI_Comm_rank(MPI_COMM_WORLD, &_mpirank);
    MPI_Comm_size(MPI_COMM_WORLD, &_mpinp);
#endif
    // env. var. takes precedance over the parameter given in the constructor
    const char *cham_ncpus_s = getenv("FMR_CHAMELEON_NUM_THREADS");
    if (cham_ncpus_s != NULL) {
      _ncpus = std::stoi(cham_ncpus_s);
    }
    const char *cham_ncudas_s = getenv("FMR_CHAMELEON_NUM_CUDAS");
    if (cham_ncudas_s != NULL) {
      _ncudas = std::stoi(cham_ncudas_s);
    }
    // set minimal requirement if user is mad
    if (_ncpus <= 0 && _ncudas <= 0) {
      _ncpus = 1;
      _ncudas = 0;
    }
    const char *cham_tilesize_s = getenv("FMR_CHAMELEON_TILE_SIZE");
    if (cham_tilesize_s != NULL) {
      _tilesize = std::stoi(cham_tilesize_s);
    }
    // initialize chameleon
    init(_ncpus, _ncudas);
    enable(CHAMELEON_PROFILING_MODE);
    disable(CHAMELEON_WARNINGS);
    set(CHAMELEON_TILE_SIZE, _tilesize);
    start_profiling();
  }
};

} // namespace Chameleon

#endif // CHAMELEON_HPP
