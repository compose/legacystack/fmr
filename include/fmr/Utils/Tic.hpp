/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef TIC_HPP
#define TIC_HPP

#include "fmr/Utils/Global.hpp"

#define USE_STD_CHRONO

#if defined(USE_STD_CHRONO)
    #include <chrono>
#elif defined(_OPENMP)
    #include <omp.h>
#elif defined(WINDOWS) // We need an os specific function
    #include <time.h>
    #include <windows.h>
#else
    #ifndef POSIX
        #warning Posix used withoug being explicitly defined
    #endif
    #include <time.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <stdint.h>
#endif


namespace fmr{
namespace tools{

/**
 * \brief Time counter class.
 * \author Berenger Bramas (berenger.bramas@inria.fr)
 *
 * This time counter can be (re)started using tic() and stopped using tac().
 *
 *  - use elapsed() to get the last time interval;
 *  - use cumulated() to get the total running time;
 *  - use reset() to stop and reset the counter.
 *
 * \code
 * Tic timer;
 * timer.tic();
 * //...(1)
 * timer.tac();
 * timer.elapsed();  // time of (1) in s
 * timer.tic();
 * //...(2)
 * timer.tac();
 * timer.elapsed();  // time of (2) in s
 * timer.cumulated() // time of (1) and (2) in s
 * timer.reset()     // reset the object
 * \endcode
 *
 * The special method that uses asm register is based on code by Alexandre DENIS
 * http://dept-info.labri.fr/~denis/Enseignement/2006-SSECPD/timing.h
 */
class Tic {
private:

    double start    = 0;    ///< start time (tic)
    double end      = 0;    ///< stop time (tac)
    double cumulate = 0;    ///< cumulated duration

public:
    /// Constructor
    Tic() {
        this->reset();
    }

    /// Copy constructor
    Tic(const Tic& other) = default;

    /// Move constructor
    Tic(Tic&& other) = default;

    /// Copies an other timer
    Tic& operator=(const Tic& other) {
        start = other.start;
        end = other.end;
        cumulate = other.cumulate;
        return *this;
    }

    /// Adds two timers
    /** The addition is done by keeping :
     *     - the left operand start date
     *     - the left operand end date
     *     - adding the cumulated times
     * \return A new Tic object
     */
    const Tic operator+(const Tic& other) const {
        Tic res(*this);
        res.cumulate += other.cumulate;
        return res;
    }

    /// Resets the timer
    /**\warning Use tic() to restart the timer. */
    void reset() {
        start    = Tic::GetTime();
        end      = start;
        cumulate = 0;
    }

    /// Start measuring time.
    void tic(){
        this->start = Tic::GetTime();
    }

    /// Peek at current elapsed time without stopping timer
    double peek() const {
        return Tic::GetTime() - this->start;;
    }

    /// Stop measuring time and add to cumulated time.
    double tac(){
        this->end = Tic::GetTime();
        auto lapse = this->elapsed();
        cumulate += lapse;
        return lapse;
    }

    /// Elapsed time between the last tic() and tac() (in seconds).
    /** \return the time elapsed between tic() & tac() in second. */
    double elapsed() const {
        return this->end - this->start;
    }

    /// Cumulated tic() - tac() time spans
    /** \return the time elapsed between ALL tic() & tac() in second. */
    double cumulated() const {
        return cumulate;
    }

    /// Combination of tic() and elapsed().
    /**
     * \todo deprecate
     * \return the time elapsed between tic() & tac() in second. */
    double tacAndElapsed() {
        tac();
        return elapsed();
    }

    /// Get system dependent time point.
    /** GetTickCount on windows
     *  gettimeofday on linux or a direct ASM method
     *  \return A system dependent time point.
     */
    static double GetTime(){
#if defined(USE_STD_CHRONO)
        using clock = std::chrono::high_resolution_clock;
        using duration = std::chrono::duration<double>;
        return duration(clock::now().time_since_epoch()).count();
#elif defined(_OPENMP)
        return omp_get_wtime();
#elif defined(WINDOWS)
        return static_cast<double>(GetTickCount())/1000.0;
#else // We are in linux/posix
        timeval t;
        gettimeofday(&t, NULL);
        return double(t.tv_sec) + (double(t.tv_usec)/1000000.0);
#endif
    }
};

}} /* namespace fmr::tools */

#endif // TIC_HPP
