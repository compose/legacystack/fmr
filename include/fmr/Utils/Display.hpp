/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef DISPLAY_HPP
#define DISPLAY_HPP

// std includes
#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>
#include <stdexcept>
#include <string>
#include <typeinfo>

namespace fmr{
/**
 * @brief The Dislay class
 *
 */
class Display {

public:
  template <class ValueType, typename Int_type>
  static void matrix(const Int_type nbRows, const Int_type nbCols,
                     ValueType *inMatrix, const std::string &name,
                     const int nbDisplayedRows, const int nbDisplayedCols,
                     const bool colmajor = true) {

    std::cout << " Display: " << nbRows << "  " << nbCols << " "
              << nbDisplayedCols << "  " << inMatrix << std::endl;
    std::cout << "\n" << name << "=[" << std::endl;
    for (int i = 0; i < nbDisplayedRows; ++i) {
      for (int j = 0; j < nbDisplayedCols; ++j) {
        if (colmajor)
          std::cout << inMatrix[i + j * nbRows] << " ";
        else
          std::cout << inMatrix[i * nbCols + j] << " ";
      }
      std::cout << std::endl;
    }
    std::cout << "]" << std::endl;
  }

  template <class Int_type, class ValueType>
  static void matrix(const Int_type nbRows, const Int_type nbCols,
                     ValueType *inMatrix, const std::string &name,
                     const int displaySize, const bool colmajor = true) {
    matrix(nbRows, nbCols, inMatrix, name, displaySize, displaySize, colmajor);
  }

  template <class ValueType, typename Int_type>
  static void vector(const Int_type size, ValueType *vector,
                     const std::string &name, const int displaySize,
                     const int part = 0) {

    std::cout << name << "=[" << std::endl;
    if (part >= 0) {
      for (int i = 0; i < displaySize; ++i) {
        std::cout << vector[i] << " ";
      }
    }
    if (part == 1) {
      std::cout << " [...] ";
      for (int i = size - 1 - displaySize; (Int_type)i < size; ++i) {
        std::cout << vector[i] << " ";
      }
    }
    std::cout << "]" << std::endl;
  }

  template <class VectorType>
  static void vector(VectorType &Vector, const std::string &name,
                     const int displaySize, const int part = 0) {
    auto size = Vector.size();
    vector(size, Vector.data(), name, displaySize, part);
  }
};

} /* namespace fmr */

#endif // DISPLAY_HPP
