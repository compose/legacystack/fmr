/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXIOCHAMELEON_HPP
#define MATRIXIOCHAMELEON_HPP

#include "fmr/Utils/Tic.hpp"
#include "fmr/Utils/Starpu.hpp"
#include "fmr/Utils/Chameleon.hpp"
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
#include "mpi.h"
#endif
#if defined(FMR_HDF5)
#include "hdf5.h"
#ifdef FMR_DLOPEN_HDF5
#include "fmr/Utils/Dlopen_hdf5.hpp"
#endif
#endif

namespace fmr {
namespace io {


#if defined(FMR_HDF5)

/**
 * @brief To fill one tile from HDF5 dataset
 *
 * This function will be called by chameleon on every tile of the matrix through
 * the Chameleon map function.
 *
 * @param[inout] descA chameleon structure representing the matrix
 * @param[in] uplo pattern: ChamUpperLower, ChamUpper or ChamLower
 * @param[in] m position index of the tile in rows
 * @param[in] n position index of the tile in columns
 * @param[in] cham_tile the tile address given by chameleon
 * @param[in] user_data user's data structure that helps filling the tile with
 * user's data.
 *
 */
template<typename FReal>
static int chamMapHdf5Tile(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                           const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
    // for specific operation on the diagonal (symmetry)
    cham_uplo_t uplot;
    // parameters for tile dimension and position in the global matrix
    int nrows, ncols, ldam, row_min, col_min;
    // parameters to select the subset
    hsize_t offset[2], count[2], stride[2], block[2];
    // we receive data from HDF5 in row major format
    FReal *tilerm;
    // address of the tile to fill
    FReal *T = (FReal *)cham_tile->mat;
    // metadata to copy the dataset blocks (pointer to the dataset)
    hid_t *A = (hid_t *)user_data;

    // uplot is meaningfull only for tiles on the diagonal, else we need to
    // copy the entire tile.
    uplot = ( m == n ) ? uplo : ChamUpperLower;

    // sizes of the tile
    nrows = (m == (descA->mt-1)) ? (descA->m - m * descA->mb) : descA->mb;
    ncols = (n == (descA->nt-1)) ? (descA->n - n * descA->nb) : descA->nb;
    ldam  = descA->get_blkldd( descA, m );
    // starting row index of the block to copy from A
    row_min = m * descA->mb;
    // starting column index of the block to copy from A
    col_min = n * descA->nb;

    // HDF5 parameters for loading a specific block
    if ( uplot == ChamUpperLower ){
        offset[0] = (hsize_t)row_min;
        offset[1] = (hsize_t)col_min;
        count[0] = (hsize_t)nrows;
        count[1] = (hsize_t)ncols;
    } else {
        // read the transpose to get column major format (optimization)
        offset[0] = (hsize_t)col_min;
        offset[1] = (hsize_t)row_min;
        count[0] = (hsize_t)ncols;
        count[1] = (hsize_t)nrows;
    }
    stride[0] = 1;
    stride[1] = 1;
    block[0] = 1;
    block[1] = 1;

    // get dataspace of the dataset
    hid_t memspace = H5Screate_simple (2, count, NULL);
    hid_t dataspace = H5Dget_space(*A);
    // select subset
    H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, offset, stride, count, block);
    // read and copy the block into the tile in row major (HDF5 convention)
    const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE ;
    tilerm = new FReal[nrows*ncols];
    H5Dread (*A, H5_TYPE, memspace, dataspace, H5P_DEFAULT, tilerm);

    if ( uplot == ChamUpperLower ) {
        // Turn in column major
        for (int j = 0; j < ncols; ++j)
        {
            for (int i = 0; i < nrows; ++i)
            {
                T[ j*ldam + i ] = tilerm[ i*ncols + j ];
            }
        }
    } else {
        // simple copy taking care to not copy the entire block on the
        // diagonal
        if ( m == n ) {
            for (int j = 0; j < ncols; ++j)
            {
                int imin = ( uplot == ChamLower ) ? j                    : 0;
                int imax = ( uplot == ChamUpper ) ? std::min(j+1, nrows) : nrows;
                for (int i = imin; i < imax; ++i)
                {
                    T[ j*ldam + i ] = tilerm[ j*ncols + i ];
                }
            }
        } else {
            memcpy(T, tilerm, nrows*ncols*sizeof(FReal));
        }
    }

    delete[] tilerm;
    H5Sclose(dataspace);
    H5Sclose(memspace);
    return 0;
}

/**
 * @brief init copy the element from one HDF5 file in the descriptor
 *
 * @param[out] data the dense wrapper matrix to fill
 * @param[in] filename    HDF5 filename on disk
 * @param[in] datasetname HDF5 dataset to consider (several datasets can be
 *            stored in one file).
 */
template<typename FSize, typename FReal>
void readHDF5Tile(ChameleonDenseWrapper<FSize, FReal> &data, const std::string& filename, const std::string& datasetname)
{
    tools::Tic time;
    int mpirank = Chameleon::mpi_rank();
    bool async;
    RUNTIME_sequence_t *sequence = NULL;
    RUNTIME_request_t *request = NULL;
    Chameleon::async_manager(false, false, false, &async, &sequence, &request);

    // open the HDF5 file
    hid_t file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    // open the specific dataset
    hid_t dataset = H5Dopen2(file, datasetname.c_str(), H5P_DEFAULT);
    // get dataspace of the dataset
    hid_t dspace = H5Dget_space(dataset);
    const int ndims = H5Sget_simple_extent_ndims(dspace);
    // the dimensions given in the constructor and the ones of the file
    // should match
    AssertLF(ndims == 2); // we consider matrices here
    hsize_t dims[2];
    H5Sget_simple_extent_dims(dspace, dims, NULL);
    H5Sclose(dspace);
    // decide the chameleon distribution of tiles over MPI processus
    const char* distrib1d_s = getenv("FMR_CHAMELEON_READH5_BYTILE_DISTRIB_1D");
    bool distrib1d_b = false;
    if ( distrib1d_s != NULL ){
        int distrib1d_i = std::stoi(distrib1d_s);
        if ( distrib1d_i == 1 ){
            distrib1d_b = true;
        }
    }
    /* decide if the chameleon matrix must be considered as symmetric */
    const char* issym_s = getenv("FMR_CHAMELEON_READH5_SYM");
    bool issym_b = data.isSymmetric();
    if ( issym_s != NULL ){
        int issym_i = std::stoi(issym_s);
        if ( issym_i == 1 ){
            issym_b = true;
        }
    }
    /* allocate */
    data.reset(( FSize)dims[0], (FSize)dims[1], issym_b );
    data.allocate(distrib1d_b);

    // copy from dataset into data.getMatrix()
    if (async){
        // we force tasks execution only on the worker id 0 because hdf5 is
        // not threaded and put some synchronization barrier at each call
        // leading to poor performances when calling hdf5 from multiple
        // threads
        Chameleon::request_set(request, CHAMELEON_REQUEST_WORKERID, 0);
    }
    if (mpirank == 0) std::cout << "[fmr] chameleon UpperLower : " << data.getUpperLower() << "\n";
    //time.tic();
    struct cham_map_data_s map_data = {
        .access = ChamRW,
        .desc   = data.getMatrix(),
    };
    struct cham_map_operator_s map_op = {
        .name = "Hdf5Tile",
        .cpufunc = chamMapHdf5Tile<FReal>,
        .cudafunc = NULL,
        .hipfunc = NULL,
    };
    Chameleon::map( data.getUpperLower(), 1, &map_data, &map_op, &dataset );
    //time.tac();
    //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:initH5:map=" << time.elapsed() << std::endl;
    if (async){
        // restore default (use all workers) for next algorithms
        Chameleon::request_set(request, CHAMELEON_REQUEST_WORKERID, -1);
        }
    if (distrib1d_b){
        // we have read the matrix in a 2d no cyclic distribution, migrate data
        // to get a 2dcyclic distribution, more suited to subsequent operations
        // (gram, gemm, qr, etc)
        //time.tic();
        Chameleon::restore_2dcyclic(data.getUpperLower(), data.getMatrix());
        //time.tac();
        //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:initH5:datamigrate=" << time.elapsed() << std::endl;
    }
    // we need to synchronize before to destroy the dataset
    // TODO: change the interface to get an already created dataset ?
    Chameleon::barrier();
    H5Dclose(dataset);
    H5Fclose(file);
}

// template <typename FSize, typename FReal>
// struct Hdf5ReadSubmatrix{
//     std::vector<FSize> rowmin;
//     std::vector<FSize> rowmax;
//     std::vector<FSize> colmin;
//     std::vector<FSize> colmax;
//     std::vector<std::vector<FReal>> submat;
// };

/**
 * @brief To fill one tile from HDF5 dataset
 *
 * This function will be called by chameleon on every tile of the matrix through
 * the Chameleon map function.
 *
 * @param[inout] descA chameleon structure representing the matrix
 * @param[in] uplo pattern: ChamUpperLower, ChamUpper or ChamLower
 * @param[in] m position index of the tile in rows
 * @param[in] n position index of the tile in columns
 * @param[in] cham_tile the tile address given by chameleon
 * @param[in] user_data user's data structure that helps filling the tile with
 * user's data.
 *
 */
template<typename FReal>
static int chamMapHdf5BlocksTile(void *user_data, cham_uplo_t uplo, int m, int n, int ndata,
                                 const CHAM_desc_t *descA, CHAM_tile_t *cham_tile, ... ) {
    // for specific operation on the diagonal (symmetry)
    cham_uplo_t uplot;
    // parameters for tile dimension and position in the global matrix
    int tile_ldam, tile_row_min, tile_col_min, tile_row_max, tile_col_max;
    // parameters to select the subset
    hsize_t offset[2], count[2], stride[2], block[2];
    // we receive data from HDF5 in row major format
    FReal *tile_hdf5;
    // address of the tile to fill
    FReal *tile_cham = (FReal *)cham_tile->mat;
    // metadata to copy the dataset blocks (pointer to the blocks metadata structure)
    Hdf5Blocks *h5blocks = (Hdf5Blocks *)user_data;

    // fixed parameter for h5 memspace selection
    stride[0] = 1;
    stride[1] = 1;
    block[0]  = 1;
    block[1]  = 1;

    // uplot is meaningfull only for tiles on the diagonal, else we need to
    // copy the entire tile.
    uplot = ( m == n ) ? uplo : ChamUpperLower;

    // sizes of the tile
    tile_ldam  = descA->get_blkldd( descA, m );
    // starting row index of the tile in the global matrix
    tile_row_min = m * descA->mb;
    // starting column index of the tile in the global matrix
    tile_col_min = n * descA->nb;
    // ending row index of the tile in the global matrix
    tile_row_max = (m == (descA->mt-1)) ? descA->m : (m+1)*descA->mb;
    // ending column index of the tile in the global matrix
    tile_col_max = (n == (descA->nt-1)) ? descA->n : (n+1)*descA->nb;

    // loop on HDF5 blocks to determine the piece of data to copy and copy it in
    // the right place of the tile buffer
    for (int k=0; k < h5blocks->blockNumber; k++){
        // global indexes of corners of this block (top left and bottom right)
        int blk_row_min = h5blocks->indexStart[k].first;
        int blk_row_max = h5blocks->indexEnd[k].first;
        int blk_col_min = h5blocks->indexStart[k].second;
        int blk_col_max = h5blocks->indexEnd[k].second;
        // global indexes of the intersection between the HDF5 block and the tile
        int64_t int_row_min=-1, int_row_max=-1, int_col_min=-1, int_col_max=-1;
        if ( tile_row_max > blk_row_min && tile_row_min < blk_row_max){
            int_row_min = std::max(tile_row_min, blk_row_min);
            int_row_max = std::min(tile_row_max, blk_row_max);
        }
        if ( tile_col_max > blk_col_min && tile_col_min < blk_col_max){
            int_col_min = std::max(tile_col_min, blk_col_min);
            int_col_max = std::min(tile_col_max, blk_col_max);
        }
        // is the intersection non empty
        int64_t int_min = std::min( {int_row_min, int_row_max, int_col_min, int_col_max} );
        if ( uplot == ChamLower && int_col_min >= int_row_max ){
            int_min=-1;
        }
        if ( uplot == ChamUpper && int_row_min >= int_col_max ){
            int_min=-1;
        }
        // if int_min==-1 then it is empty, else read and copy the intersection
        if ( int_min >= 0 ){
            // compute the starting index of the intersection relative to
            // the HDF5 block, consider the transpose (optimization)
            int intblk_row_min = int_row_min - blk_row_min;
            int intblk_col_min = int_col_min - blk_col_min;
            // size of the intersection
            int intblk_nrows = int_row_max - int_row_min;
            int intblk_ncols = int_col_max - int_col_min;
            // HDF5 parameters for loading a specific sub-block
            offset[0] = (hsize_t)intblk_row_min;
            offset[1] = (hsize_t)intblk_col_min;
            count[0] = (hsize_t)intblk_nrows;
            count[1] = (hsize_t)intblk_ncols;
            // get dataspace of the dataset
            hid_t memspace = H5Screate_simple (2, count, NULL);
            hid_t dataspace = H5Dget_space(h5blocks->dataSets[k]);
            // select subset
            H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, offset, stride, count, block);
            // read the block in row major (HDF5 convention)
            const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE;
            tile_hdf5 = new FReal[intblk_nrows*intblk_ncols];
            H5Dread (h5blocks->dataSets[k], H5_TYPE, memspace, dataspace, H5P_DEFAULT, tile_hdf5);
            // compute the indexes where to start in the chameleon tile in
            // order to copy the data in the proper part which represents the intersection
            int zero = 0;
            int inttile_row_min = std::max(zero, blk_row_min - tile_row_min);
            int inttile_col_min = std::max(zero, blk_col_min - tile_col_min);
            // copy in the chameleon tile
            for (int j = inttile_col_min; j < inttile_col_min+intblk_ncols; ++j)
            {
                int imin = ( uplot == ChamLower ) ? std::max(j, inttile_row_min) : inttile_row_min;
                int imax = ( uplot == ChamUpper ) ? std::min(j+1, inttile_row_min+intblk_nrows) : inttile_row_min+intblk_nrows;
                for (int i = imin; i < imax; ++i)
                {
                    int index_cham = j*tile_ldam + i;
                    int index_hdf5 = (i-inttile_row_min)*intblk_ncols + (j-inttile_col_min);
                    tile_cham[ index_cham ] = tile_hdf5[ index_hdf5 ];
                }
            }
            // free temporary tile buffer
            delete[] tile_hdf5;
            // release access to dataspaces
            H5Sclose(dataspace);
            H5Sclose(memspace);
        }

        // consider the symmetric part if HDF5 files give the global matrix in UPPER/LOWER mode
        // global indexes of the intersection between the HDF5 block and the tile
        int_row_min=-1;
        int_row_max=-1;
        int_col_min=-1;
        int_col_max=-1;
        if ( tile_col_max > blk_row_min && tile_col_min < blk_row_max){
            int_row_min = std::max(tile_col_min, blk_row_min);
            int_row_max = std::min(tile_col_max, blk_row_max);
        }
        if ( tile_row_max > blk_col_min && tile_row_min < blk_col_max){
            int_col_min = std::max(tile_row_min, blk_col_min);
            int_col_max = std::min(tile_row_max, blk_col_max);
        }
        // is the intersection non empty, check it only if intersection is
        // empty in the previous step
        if ( int_min == -1 || uplo == ChamUpperLower ){
            int_min = std::min( {int_row_min, int_row_max, int_col_min, int_col_max} );
        } else {
            int_min = -1;
        }
        // if int_min==-1 then it is empty, else read and copy the intersection
        if ( int_min>=0 ){
            // compute the starting index of the intersection relative to the HDF5 block
            int intblk_row_min = int_row_min - blk_row_min;
            int intblk_col_min = int_col_min - blk_col_min;
            // size of the intersection
            int intblk_nrows = int_row_max - int_row_min;
            int intblk_ncols = int_col_max - int_col_min;
            // HDF5 parameters for loading a specific sub-block
            offset[0] = (hsize_t)intblk_row_min;
            offset[1] = (hsize_t)intblk_col_min;
            count[0] = (hsize_t)intblk_nrows;
            count[1] = (hsize_t)intblk_ncols;
            // get dataspace of the dataset
            hid_t memspace = H5Screate_simple (2, count, NULL);
            hid_t dataspace = H5Dget_space(h5blocks->dataSets[k]);
            // select subset
            H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, offset, stride, count, block);
            // read the block in row major (HDF5 convention)
            const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE;
            tile_hdf5 = new FReal[intblk_nrows*intblk_ncols];
            H5Dread (h5blocks->dataSets[k], H5_TYPE, memspace, dataspace, H5P_DEFAULT, tile_hdf5);
            // compute the indexes where to start in the chameleon tile in
            // order to copy the data in the proper part which represents the intersection
            int zero = 0;
            int inttile_row_min = std::max(zero, blk_col_min - tile_row_min);
            int inttile_col_min = std::max(zero, blk_row_min - tile_col_min);
            // copy in the chameleon tile and turn in column major
            for (int j = inttile_col_min; j < inttile_col_min+intblk_nrows; ++j)
            {
                int imin = ( uplot == ChamLower ) ? std::max(j, inttile_row_min) : inttile_row_min;
                int imax = ( uplot == ChamUpper ) ? std::min(j+1, inttile_row_min+intblk_ncols) : inttile_row_min+intblk_ncols;
                for (int i = imin; i < imax; ++i)
                {
                    int index_cham = j*tile_ldam + i;
                    int index_hdf5 = (j-inttile_col_min)*intblk_ncols + (i-inttile_row_min);
                    tile_cham[ index_cham ] = tile_hdf5[ index_hdf5 ];
                }
            }
            // free temporary tile buffer
            delete[] tile_hdf5;
            // release access to dataspaces
            H5Sclose(dataspace);
            H5Sclose(memspace);
        }
    }
    return 0;
}

/**
 * @brief reads the element from one or several HDF5 files in the descriptor
 *
 * @param[out] data the dense wrapper matrix to fill
 * @param[in] filename    metadata txt file.
 *                        If the global matrix is made of several hdf5 files the
 *                        metadata ASCII file should contain the following information:
 *                        Number of blocks e.g. 3
 *                        triangular part of the matrix given by the blocks e.g. upper
 *                        number line, number column of the block, file name, dataset name e.g.
 *                        0 0 atlas_guyane_trnH_0.h5  distance
 *                        0 1 atlas_guyane_trnH_1.h5  distance
 *                        0 2 atlas_guyane_trnH_2.h5  distance
 *                        1 1 atlas_guyane_trnH_3.h5  distance
 *                        1 2 atlas_guyane_trnH_4.h5  distance
 *                        2 2 atlas_guyane_trnH_5.h5  distance
 * @param[in] dirlocation root directory of the h5 files.
 */
template<class FSize, class FReal>
void readHDF5TileMulti(ChameleonDenseWrapper<FSize, FReal> &data, const std::string& filename, const std::string& dirlocation)
{
    int mpirank = Chameleon::mpi_rank();
    bool async;
    RUNTIME_sequence_t *sequence = NULL;
    RUNTIME_request_t *request = NULL;
    Chameleon::async_manager(false, false, false, &async, &sequence, &request);

    //tools::Tic time;
    //time.tic();

    // structure that wraps the global matrix by blocks information
    Hdf5Blocks *h5blocks = new Hdf5Blocks;

    /* Read the metadata */
    std::fstream dataFile(filename, std::ios::in);

    // number of blocks, in rows and columns, e.g. 3 means the global
    // matrix is made of 3x3 blocks
    dataFile >> h5blocks->blockNumber1D;

    // upper/lower part of the matrix the blocks represent
    dataFile >> h5blocks->format;
    std::transform(h5blocks->format.begin(), h5blocks->format.end(), h5blocks->format.begin(),
                    [](unsigned char c){ return std::toupper(c); }
                    );
    std::cout << "Nb block: " << h5blocks->blockNumber1D << std::endl
              << "Format:   " << h5blocks->format      << std::endl;

    // total number of blocks to read (upper/lower part or the full matrix)
    int nbBlocks = h5blocks->blockNumber1D*(h5blocks->blockNumber1D+1)/2;
    if(h5blocks->format == "FULL"){
        nbBlocks = h5blocks->blockNumber1D*h5blocks->blockNumber1D;
    }
    h5blocks->blockNumber = nbBlocks;

    // for each block store the filename and datasetname
    h5blocks->h5FileNames.resize(nbBlocks);
    h5blocks->dataSetNames.resize(nbBlocks);
    int i, j, pos;
    std::string name, dataset;
    std::vector< std::pair<int,int> > index(nbBlocks);
    for (int l =0 ; l < nbBlocks ; ++l) {
        dataFile >> i >> j >> name >> dataset;
        if (h5blocks->format == "UPPER"){
            pos = i*(h5blocks->blockNumber1D+1)-i*(i+1)/2+j-i;
        } else {
            pos = j*(h5blocks->blockNumber1D+1) - j*(j+1)/2 + i-j;
        }
        index[l].first= i ; index[l].second= j;
        h5blocks->h5FileNames[pos]  = dirlocation + "/" + name;
        h5blocks->dataSetNames[pos] = dataset;
    }

    /* Define the global problems by reading sizes of all the blocks */
    h5blocks->Files.resize(nbBlocks);
    h5blocks->dataSets.resize(nbBlocks);
    h5blocks->dataSpaces.resize(nbBlocks);
    h5blocks->dims.resize(nbBlocks);
    h5blocks->maph5mpi = nullptr;
    h5blocks->handlesH5 = nullptr;
    h5blocks->cl_inittile = nullptr;
    h5blocks->cl_readh5 = nullptr;
    h5blocks->cl_copypaneltotile = nullptr;
    int* rowsSize = new int [h5blocks->blockNumber1D+1] ;
    int* colsSize = new int [h5blocks->blockNumber1D+1] ;
    rowsSize[0] = 0;
    colsSize[0] = 0;

    // get sizes of each block
    for (int l =0 ; l < nbBlocks ; ++l) {
        h5blocks->Files[l] = H5Fopen(h5blocks->h5FileNames[l].c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        h5blocks->dataSets[l] = H5Dopen2(h5blocks->Files[l], h5blocks->dataSetNames[l].c_str(), H5P_DEFAULT);
        h5blocks->dataSpaces[l] = H5Dget_space(h5blocks->dataSets[l]);
        hsize_t dims[2];
        H5Sget_simple_extent_dims(h5blocks->dataSpaces[l], dims, NULL);
        h5blocks->dimsglob[0] = dims[0];
        h5blocks->dimsglob[1] = dims[1];
        rowsSize[index[l].first+1] = h5blocks->dimsglob[0];
        colsSize[index[l].second+1] = h5blocks->dimsglob[1];
    }

    // Define the indexes where the blocks start in the global matrix
    for ( int i = 1 ; i < h5blocks->blockNumber1D+1 ; ++i){
        rowsSize[i] += rowsSize[i-1];
        colsSize[i] += colsSize[i-1];
    }

    // deduce the global size
    h5blocks->dimsglob[0]  = rowsSize[h5blocks->blockNumber1D] ;
    h5blocks->dimsglob[1]  = colsSize[h5blocks->blockNumber1D] ;

    h5blocks->indexEnd.resize(nbBlocks);
    h5blocks->indexStart.resize(nbBlocks);

    // compute the indexes of the first and last elements of a sub block in
    // the global matrix
    if (mpirank==0) {
        std::cout << " Matrix size: " <<  h5blocks->dimsglob[0] << " x " << h5blocks->dimsglob[1] << std::endl;
    }
    for ( int i = 0 ; i < nbBlocks ; ++i){
        h5blocks->indexStart[i].first  = rowsSize[index[i].first] ;
        h5blocks->indexStart[i].second = colsSize[index[i].second] ;
        h5blocks->indexEnd[i].first    = rowsSize[index[i].first+1] ;
        h5blocks->indexEnd[i].second   = colsSize[index[i].second+1] ;
        if (mpirank==0) {
            std::cout << "Bloc " << i << " i " << index[i].first << " j " << index[i].second
                        << " rows: " << h5blocks->indexStart[i].first << " cols: "<< h5blocks->indexStart[i].second
                        << " N: "<< h5blocks->indexEnd[i].first - h5blocks->indexStart[i].first << " M "<< h5blocks->indexEnd[i].second - h5blocks->indexStart[i].second
                        << "  " << h5blocks->h5FileNames[i] << std::endl<<std::flush;
        }
    }
    delete [] rowsSize;
    delete [] colsSize;

    // decide the chameleon distribution of tiles over MPI processus
    const char* distrib1d_s = getenv("FMR_CHAMELEON_READH5_BYTILE_DISTRIB_1D");
    bool distrib1d_b = false;
    if ( distrib1d_s != NULL ){
        int distrib1d_i = std::stoi(distrib1d_s);
        if ( distrib1d_i == 1 ){
            distrib1d_b = true;
        }
    }
    /* decide if the chameleon matrix must be considered as symmetric */
    const char* issym_s = getenv("FMR_CHAMELEON_READH5_SYM");
    bool issym_b = data.isSymmetric();
    if ( issym_s != NULL ){
        int issym_i = std::stoi(issym_s);
        if ( issym_i == 1 ){
            issym_b = true;
        }
    }
    // allocate the chameleon descriptor
    data.reset(h5blocks->dimsglob[0], h5blocks->dimsglob[1], issym_b);
    data.allocate(distrib1d_b);
    /* give h5blocks pointer in the async case, in order to be able to
    deallocate it */
    data.setH5Blocks(h5blocks);

    // copy from dataset into data.getMatrix()
    if (async){
        // we force tasks execution only on the worker id 0 because hdf5 is
        // not threaded and put some synchronization barrier at each call
        // leading to poor performances when calling hdf5 from multiple
        // threads
        Chameleon::request_set(request, CHAMELEON_REQUEST_WORKERID, 0);
    }
    if (mpirank == 0){
        std::cout << "[fmr] chameleon UpperLower : " << data.getUpperLower() << "\n";
    }
    struct cham_map_data_s map_data = {
        .access = ChamRW,
        .desc   = data.getMatrix(),
    };
    struct cham_map_operator_s map_op = {
        .name = "Hdf5BlocksTile",
        .cpufunc = chamMapHdf5BlocksTile<FReal>,
        .cudafunc = NULL,
        .hipfunc = NULL,
    };
    Chameleon::map( data.getUpperLower(), 1, &map_data, &map_op, h5blocks );
    //time.tac();
    //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:initH5Blocks:map=" << time.elapsed() << std::endl;
    if (async){
        // restore default (use all workers) for next algorithms
        Chameleon::request_set(request, CHAMELEON_REQUEST_WORKERID, -1);
        }
    if (distrib1d_b){
        // we have read the matrix in a 2d no cyclic distribution, migrate data
        // to get a 2dcyclic distribution, more suited to subsequent operations
        // (gram, gemm, qr, etc)
        //time.tic();
        Chameleon::restore_2dcyclic(data.getUpperLower(), data.getMatrix());
        //time.tac();
        //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:initH5:datamigrate=" << time.elapsed() << std::endl;
    }
    /* Need to add a runtime synchronization barrier in some cases ? */
    //sync();
}

/**
 * @brief reads the element from one (.h5 file) or several (.txt metadata file)
 * HDF5 files to build a global matrix distributed over MPI processes by blocks
 * of rows in a balanced way following the natural order. First rows of the
 * first file is handled by first rank, second block of rows by the second rank
 * etc until all rows have been allocated. Same thing with all the files. We may
 * have for example the 3 first ranks handling the first file, the following 2
 * ranks handling the second file (sub matrices in files have not the same size)
 * and so on. We know how many rows we have to read globally so that we can
 * compute a reference number of rows to allocate per rank to determine when a
 * process is fully loaded. Some processes may straddle between two files. In
 * addition we handle overlaping blocks corresponding to Chameleon's tiles so
 * that an overlaping tile will be handled by a single process, the one handling
 * the starting value of the tile.
 *
 * @param[out] data the dense wrapper matrix to fill
 * @param[in] filename    HDF5 filename on disk
 * @param[in] datasetname HDF5 dataset to consider (several datasets can be
 *            stored in one file).
 */
template<class FSize, class FReal>
void readHDF5Panel(ChameleonDenseWrapper<FSize, FReal> &data, const std::string& filename, const std::string& datasetname)
{
    int mpirank = Chameleon::mpi_rank();
    int mpinp = Chameleon::mpi_nprocs();
    int tilesize;
    Chameleon::get(CHAMELEON_TILE_SIZE, &tilesize);

    //tools::Tic time;
    //time.tic();

    /* structure that wraps the global matrix by blocks information */
    Hdf5Blocks *h5blocks = new Hdf5Blocks;
    InitHdf5Blocks(h5blocks);

    int nbBlocks = 0;

    /* Open Files and compute blocks information */
    if (filename.find(".h5") != std::string::npos) {
        h5blocks->format   = "FULL";
        nbBlocks = 1;
        h5blocks->blockNumber1D = nbBlocks;
        h5blocks->blockNumber   = nbBlocks;
        h5blocks->h5FileNames.resize(nbBlocks);
        h5blocks->Files.resize(nbBlocks);
        h5blocks->dataSetNames.resize(nbBlocks);
        h5blocks->dataSets.resize(nbBlocks);
        h5blocks->dataSpaces.resize(nbBlocks);
        h5blocks->dims.resize(nbBlocks);
        h5blocks->index.resize(nbBlocks);
        h5blocks->indexStart.resize(nbBlocks);
        h5blocks->indexEnd.resize(nbBlocks);
        h5blocks->h5FileNames[0]  = filename;
        h5blocks->dataSetNames[0] = datasetname;
        hsize_t dims[2];
        #ifdef FMR_DLOPEN_HDF5
        // open a new instance of hdf5
        hdf5_dyn_functions_t* HDF5_handler = create_hdf5_instance(mpirank, 100);
        h5blocks->Files[0] = HDF5_handler->dl_H5Fopen(filename.c_str(), DL5_H5F_ACC_RDONLY(HDF5_handler), H5P_DEFAULT);
        h5blocks->dataSets[0] = HDF5_handler->dl_H5Dopen2(h5blocks->Files[0], datasetname.c_str(), H5P_DEFAULT);
        h5blocks->dataSpaces[0] = HDF5_handler->dl_H5Dget_space(h5blocks->dataSets[0]);
        HDF5_handler->dl_H5Sget_simple_extent_dims(h5blocks->dataSpaces[0], dims, NULL);
        close_hdf5_instance(HDF5_handler);
        #else
        h5blocks->Files[0] = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        h5blocks->dataSets[0] = H5Dopen2(h5blocks->Files[0], datasetname.c_str(), H5P_DEFAULT);
        h5blocks->dataSpaces[0] = H5Dget_space(h5blocks->dataSets[0]);
        H5Sget_simple_extent_dims(h5blocks->dataSpaces[0], dims, NULL);
        #endif
        h5blocks->dims[0][0] = dims[0];
        h5blocks->dims[0][1] = dims[1];
        h5blocks->dimsglob[0] = h5blocks->dims[0][0];
        h5blocks->dimsglob[1] = h5blocks->dims[0][1];
        // compute the indexes of the first and last elements of a sub block in
        // the global matrix
        if (mpirank==0) {
            std::cout << "[fmr] Matrix size: " <<  h5blocks->dimsglob[0] << " x " << h5blocks->dimsglob[1] << std::endl;
        }
        h5blocks->index[0].first  = 0 ;
        h5blocks->index[0].second = 0 ;
        h5blocks->indexStart[0].first  = 0 ;
        h5blocks->indexStart[0].second = 0 ;
        h5blocks->indexEnd[0].first    = h5blocks->dims[0][0] ;
        h5blocks->indexEnd[0].second   = h5blocks->dims[0][0] ;
    } else {
        const std::string dirlocation(datasetname);

        /* Read the metadata */
        std::fstream dataFile(filename, std::ios::in);

        // number of blocks, in rows and columns, e.g. 3 means the global
        // matrix is made of 3x3 blocks
        dataFile >> h5blocks->blockNumber1D;

        // upper/lower part of the matrix the blocks represent
        dataFile >> h5blocks->format;
        std::transform(h5blocks->format.begin(), h5blocks->format.end(), h5blocks->format.begin(),
                    [](unsigned char c){ return std::toupper(c); }
                    );

        // total number of blocks to read (upper/lower part or the full matrix)
        nbBlocks = h5blocks->blockNumber1D*(h5blocks->blockNumber1D+1)/2;
        if(h5blocks->format == "FULL"){
            nbBlocks = h5blocks->blockNumber1D*h5blocks->blockNumber1D;
        }
        h5blocks->blockNumber = nbBlocks;

        // for each block store the filename and datasetname
        h5blocks->h5FileNames.resize(nbBlocks);
        h5blocks->dataSetNames.resize(nbBlocks);
        h5blocks->index.resize(nbBlocks);
        int i, j, pos;
        std::string name, dataset;
        for (int l =0 ; l < nbBlocks ; ++l) {
            dataFile >> i >> j >> name >> dataset;
            if (h5blocks->format == "UPPER"){
                pos = i*(h5blocks->blockNumber1D+1) - i*(i+1)/2 + j-i;
            } else {
                pos = j*(h5blocks->blockNumber1D+1) - j*(j+1)/2 + i-j;
            }
            h5blocks->index[l].first = i;
            h5blocks->index[l].second = j;
            h5blocks->h5FileNames[pos]  = dirlocation + "/" + name;
            h5blocks->dataSetNames[pos] = dataset;
        }

        /* Define the global problems by reading sizes of all the blocks */
        h5blocks->Files.resize(nbBlocks);
        h5blocks->dataSets.resize(nbBlocks);
        h5blocks->dataSpaces.resize(nbBlocks);
        h5blocks->dims.resize(nbBlocks);
        int* rowsSize = new int [h5blocks->blockNumber1D+1] ;
        int* colsSize = new int [h5blocks->blockNumber1D+1] ;
        rowsSize[0] = 0;
        colsSize[0] = 0;

        #ifdef FMR_DLOPEN_HDF5
        hdf5_dyn_functions_t* HDF5_handler = create_hdf5_instance(mpirank, 100);
        #endif

        // get sizes of each block
        for (int l=0 ; l < nbBlocks ; ++l) {
            hsize_t dims[2];
            #ifdef FMR_DLOPEN_HDF5
            h5blocks->Files[l] = HDF5_handler->dl_H5Fopen(h5blocks->h5FileNames[l].c_str(), DL5_H5F_ACC_RDONLY(HDF5_handler), H5P_DEFAULT);
            h5blocks->dataSets[l] = HDF5_handler->dl_H5Dopen2(h5blocks->Files[l], h5blocks->dataSetNames[l].c_str(), H5P_DEFAULT);
            h5blocks->dataSpaces[l] = HDF5_handler->dl_H5Dget_space(h5blocks->dataSets[l]);
            HDF5_handler->dl_H5Sget_simple_extent_dims(h5blocks->dataSpaces[l], dims, NULL);
            #else
            h5blocks->Files[l] = H5Fopen(h5blocks->h5FileNames[l].c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
            h5blocks->dataSets[l] = H5Dopen2(h5blocks->Files[l], h5blocks->dataSetNames[l].c_str(), H5P_DEFAULT);
            h5blocks->dataSpaces[l] = H5Dget_space(h5blocks->dataSets[l]);
            H5Sget_simple_extent_dims(h5blocks->dataSpaces[l], dims, NULL);
            #endif
            h5blocks->dims[l][0] = dims[0];
            h5blocks->dims[l][1] = dims[1];
            rowsSize[h5blocks->index[l].first+1] = h5blocks->dims[l][0];
            colsSize[h5blocks->index[l].second+1] = h5blocks->dims[l][1];
        }

        #ifdef FMR_DLOPEN_HDF5
        close_hdf5_instance(HDF5_handler);
        #endif

        // Define the indexes where the blocks start in the global matrix
        for ( int i = 1 ; i < h5blocks->blockNumber1D+1 ; ++i){
            rowsSize[i] += rowsSize[i-1];
            colsSize[i] += colsSize[i-1];
        }

        // deduce the global size
        h5blocks->dimsglob[0]  = rowsSize[h5blocks->blockNumber1D] ;
        h5blocks->dimsglob[1]  = colsSize[h5blocks->blockNumber1D] ;

        h5blocks->indexEnd.resize(nbBlocks);
        h5blocks->indexStart.resize(nbBlocks);

        // compute the indexes of the first and last elements of a sub block in
        // the global matrix
        if (mpirank==0) {
            std::cout << "[fmr] Matrix size: " <<  h5blocks->dimsglob[0] << " x " << h5blocks->dimsglob[1] << std::endl;
        }
        for ( int i = 0 ; i < nbBlocks ; ++i){
            h5blocks->indexStart[i].first  = rowsSize[h5blocks->index[i].first] ;
            h5blocks->indexStart[i].second = colsSize[h5blocks->index[i].second] ;
            h5blocks->indexEnd[i].first    = rowsSize[h5blocks->index[i].first+1] ;
            h5blocks->indexEnd[i].second   = colsSize[h5blocks->index[i].second+1] ;
        }
        delete [] rowsSize;
        delete [] colsSize;
    }

    /* Compute total number of elements over all the blocks. */
    hsize_t dimsglob = 0;
    for (int i=0; i<nbBlocks; i++){
        dimsglob += h5blocks->dims[i][0] * h5blocks->dims[i][1];
    }

    /* Init array of ranks of tiles */
    int nbtilesrows = h5blocks->dimsglob[0]%tilesize == 0 ? h5blocks->dimsglob[0]/tilesize : h5blocks->dimsglob[0]/tilesize +1;
    int nbtilescols = h5blocks->dimsglob[1]%tilesize == 0 ? h5blocks->dimsglob[1]/tilesize : h5blocks->dimsglob[1]/tilesize + 1;
    chameleon_freerankoftiles_h5(nbtilesrows);
    chameleon_rankofTiles_h5 = new int*[nbtilesrows];
    for (int i=0; i < nbtilesrows; i++){
        chameleon_rankofTiles_h5[i] = new int[nbtilescols];
        for (int j=0; j < nbtilescols; j++){
            chameleon_rankofTiles_h5[i][j] = -1;
        }
    }
    chameleon_isAllocatedRankofTiles_h5 = true;

    /* number of values per process so that it is balanced */
    hsize_t nbvalperprocref = dimsglob / mpinp ;
    /* we set an imbalance tolerance of 5 percent */
    hsize_t nbvaltolerance = nbvalperprocref*5/(float)100;
    if (nbvalperprocref == 0){
        printf("[fmr] Matrix is so small that we don't have the minimum of one value per process to read, please decrease the number of processes or consider a larger matrix.\n");
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
        MPI_Finalize();
#endif
        exit(-1);
    }

    /***************************************/
    /* Hdf5 files <-> MPI ranks allocation */
    /***************************************/

    /* Initialize structures */
    h5blocks->maph5mpi = new MapH5Mpi[nbBlocks];
    for (int i=0; i < nbBlocks; i++){
        h5blocks->maph5mpi[i].filename = h5blocks->h5FileNames[i];
        h5blocks->maph5mpi[i].npmax = 1;
        h5blocks->maph5mpi[i].mpirr = (MapH5MpiList*)malloc( sizeof(MapH5MpiList) );
        h5blocks->maph5mpi[i].mpirr->nb = 0;
    }

    /* number of remaining rows considering all files */
    hsize_t nbrowsh5remainglob = 0;
    /* number of remaining rows to handle in each file */
    auto nbrowsh5remain = new hsize_t[nbBlocks];
    /* index of rows, min/max, for allocations considering overlaping tiles */
    auto rowsidxh5 = new hsize_t[nbBlocks][2];
    /* index of cols, min/max, for allocations considering overlaping tiles */
    auto colsidxh5 = new hsize_t[nbBlocks][2];

    /* compute offsets due to not matching sizes between blocks and tiles */
    for (int i=0; i<nbBlocks; i++){
        rowsidxh5[i][0] = 0;
        colsidxh5[i][0] = 0;
        if ( h5blocks->index[i].first > 0 ){
            hsize_t remaindertile = h5blocks->indexStart[i].first % tilesize;
            if ( remaindertile != 0 ){
                rowsidxh5[i][0] = tilesize - remaindertile;
            }
        }
        if ( h5blocks->index[i].second > 0 &&
                h5blocks->index[i].second > h5blocks->index[i].first ){
            hsize_t remaindertile = h5blocks->indexStart[i].second % tilesize;
            if ( remaindertile != 0 ){
                colsidxh5[i][0] = tilesize - remaindertile;
            }
        }
        rowsidxh5[i][1] = rowsidxh5[i][0]-1;
        colsidxh5[i][1] = h5blocks->dims[i][1]-1;
        nbrowsh5remain[i] = h5blocks->dims[i][0] - rowsidxh5[i][1] - 1;
        nbrowsh5remainglob += h5blocks->dims[i][0];
    }

    /* load on each proc */
    auto nbvalproc = new hsize_t[mpinp];
    for (int i=0; i < mpinp; i++){
        nbvalproc[i] = 0;
    }

    /* current process rank to allocate */
    int pidx = 0;

    /* count how many blocks are handled by my rank */
    int nbblocksrank = 0;

    /* Main loop for allocation */
    for (int bidx=0; bidx < nbBlocks; bidx++){

        hsize_t ncolsblock = colsidxh5[bidx][1] - colsidxh5[bidx][0] + 1;

        /* index of rows, min/max, of the subblock in the global matrix distributed over the ranks */
        hsize_t rowsidxmatrix[2];
        rowsidxmatrix[0] = h5blocks->indexStart[bidx].first + rowsidxh5[bidx][0];
        rowsidxmatrix[1] = rowsidxmatrix[0]-1;
        /* index of cols, min/max, of the subblock in the global matrix distributed over the ranks */
        hsize_t colsidxmatrix[2];
        colsidxmatrix[0] = h5blocks->indexStart[bidx].second + colsidxh5[bidx][0];
        colsidxmatrix[1] = colsidxmatrix[0] + ncolsblock - 1;

        while ( nbrowsh5remain[bidx] > 0 ){
            /* load proc with H5 block rows */
            hsize_t nbrowstofillproc = std::max((hsize_t)1, (nbvalperprocref - nbvalproc[pidx]) / ncolsblock + 1);
            hsize_t ntilestofillproc = nbrowstofillproc / tilesize + 1;
            nbrowstofillproc = std::max((hsize_t)tilesize, ntilestofillproc*tilesize);
            /* if last process handle all what remains */
            if ( pidx == mpinp - 1 ){
                nbrowstofillproc = nbrowsh5remain[bidx];
            }
            /* if remaining number of rows is very small we accept to handle
            it to avoid getting new processes handling this file */
            if ( (nbrowsh5remain[bidx] - nbrowstofillproc)*ncolsblock < nbvaltolerance ){
                nbrowstofillproc = nbrowsh5remain[bidx];
            }
            /* compute the max number of rows to add */
            hsize_t nbrowsproctoadd = std::min(nbrowsh5remain[bidx], nbrowstofillproc);
            /* read by block of tilesize rows */
            nbrowsproctoadd = std::min(nbrowsproctoadd, (hsize_t)tilesize);

            nbvalproc[pidx]      += nbrowsproctoadd * ncolsblock;
            nbrowsh5remain[bidx] -= nbrowsproctoadd;
            nbrowsh5remainglob   -= nbrowsproctoadd;
            rowsidxh5[bidx][1]   += nbrowsproctoadd;
            rowsidxmatrix[1]     += nbrowsproctoadd;

            /* new allocation for this H5 block */
            InsertMapH5Mpi(h5blocks->maph5mpi[bidx].mpirr, pidx,
                            rowsidxh5[bidx][0], rowsidxh5[bidx][1], colsidxh5[bidx][0], colsidxh5[bidx][1],
                            rowsidxmatrix[0], rowsidxmatrix[1], colsidxmatrix[0], colsidxmatrix[1]
                            );
            if ( mpirank == pidx ){
                nbblocksrank +=1;
            }

            /* compute rank of tiles */
            hsize_t colidx;
            int remainder = colsidxmatrix[0]%tilesize;
            if ( remainder != 0 ){
                int idxtile = colsidxmatrix[0]/tilesize +1;
                colidx = idxtile*tilesize;
            } else {
                colidx = colsidxmatrix[0];
            }
            for (hsize_t i = rowsidxmatrix[0]; i <= rowsidxmatrix[1]; i += tilesize){
                for (hsize_t j = colidx; j <= colsidxmatrix[1]; j += tilesize){
                    int it = i/tilesize;
                    int jt = j/tilesize;
                    if (chameleon_rankofTiles_h5[it][jt] == -1){
                        chameleon_rankofTiles_h5[it][jt] = pidx;
                        if (h5blocks->format != "FULL"){
                            chameleon_rankofTiles_h5[jt][it] = pidx;
                        }
                    }
                }
            }

            /* allocate the tile overlap over columns on the next H5 file on my right */
            if ( h5blocks->index[bidx].second < h5blocks->blockNumber1D-1 )
            {
                hsize_t nbcolsoverlapint = (h5blocks->indexEnd[bidx].second)%tilesize;
                if (nbcolsoverlapint > 0) {
                    int bidxoverlap = bidx+1;
                    hsize_t nbcolsoverlapext = tilesize - nbcolsoverlapint;
                    hsize_t colsidxglob = h5blocks->indexStart[bidxoverlap].second;
                    InsertMapH5Mpi(h5blocks->maph5mpi[bidxoverlap].mpirr, pidx,
                                    rowsidxh5[bidx][0], rowsidxh5[bidx][1], 0, nbcolsoverlapext-1,
                                    rowsidxmatrix[0], rowsidxmatrix[1], colsidxglob, colsidxglob+nbcolsoverlapext-1);
                    if ( mpirank == pidx ){
                        nbblocksrank +=1;
                    }
                    nbvalproc[pidx] += (rowsidxh5[bidx][1] - rowsidxh5[bidx][0] + 1) * nbcolsoverlapext;
                }
            }

            /* allocate the tile overlap over rows on the next H5 file just below */
            if ( h5blocks->index[bidx].first < h5blocks->blockNumber1D-1 &&
                    h5blocks->index[bidx].second > h5blocks->index[bidx].first &&
                    nbrowsh5remain[bidx] == 0 )
            {
                hsize_t nbrowsoverlapint = (h5blocks->indexEnd[bidx].first)%tilesize;
                if (nbrowsoverlapint > 0) {
                    int bidxoverlap = bidx+h5blocks->blockNumber1D-(h5blocks->index[bidx].first+1);
                    hsize_t nbrowsoverlapext = tilesize - nbrowsoverlapint;
                    hsize_t rowsidxglob = h5blocks->indexStart[bidxoverlap].first;
                    InsertMapH5Mpi(h5blocks->maph5mpi[bidxoverlap].mpirr, pidx,
                                    0, nbrowsoverlapext-1, colsidxh5[bidx][0], colsidxh5[bidx][1],
                                    rowsidxglob, rowsidxglob+nbrowsoverlapext-1, colsidxmatrix[0],  colsidxmatrix[1]);
                    if ( mpirank == pidx ){
                        nbblocksrank +=1;
                    }
                    nbvalproc[pidx] += nbrowsoverlapext *(colsidxh5[bidxoverlap][1] - colsidxh5[bidxoverlap][0] + 1);
                    nbrowsh5remainglob -= nbrowsoverlapext;
                }
            }

            /* allocate the tile overlap on the next H5 file bottom/right corner */
            if ( h5blocks->index[bidx].first < h5blocks->blockNumber1D-1 &&
                    h5blocks->index[bidx].second < h5blocks->blockNumber1D-1 &&
                    nbrowsh5remain[bidx] == 0 )
            {
                hsize_t nbrowsoverlapint = (h5blocks->indexEnd[bidx].first)%tilesize;
                hsize_t nbcolsoverlapint = (h5blocks->indexEnd[bidx].second)%tilesize;
                if (nbrowsoverlapint > 0 && nbcolsoverlapint) {
                    int bidxoverlap = bidx+h5blocks->blockNumber1D-(h5blocks->index[bidx].first+1)+1;
                    hsize_t nbrowsoverlapext = tilesize - nbrowsoverlapint;
                    hsize_t nbcolsoverlapext = tilesize - nbcolsoverlapint;
                    hsize_t rowsidxglob = h5blocks->indexStart[bidxoverlap].first;
                    hsize_t colsidxglob = h5blocks->indexStart[bidxoverlap].second;
                    InsertMapH5Mpi(h5blocks->maph5mpi[bidxoverlap].mpirr, pidx,
                    0, nbrowsoverlapext-1, 0, nbcolsoverlapext-1,
                    rowsidxglob, rowsidxglob+nbrowsoverlapext-1, colsidxglob, colsidxglob+nbcolsoverlapext-1);
                    if ( mpirank == pidx ){
                        nbblocksrank +=1;
                    }
                    nbvalproc[pidx] += nbrowsoverlapext * nbcolsoverlapext;
                }
            }

            rowsidxh5[bidx][0]  += nbrowsproctoadd;
            rowsidxmatrix[0]    += nbrowsproctoadd;

            if ( nbvalproc[pidx] >= nbvalperprocref ){
                pidx  += 1;
            }
        }
    }
    // int nbtileperproc[mpinp];
    // for (int i=0; i < mpinp; i++){
    //     nbtileperproc[i] = 0;
    // }
    // if (mpirank == 0){
    //     for (int i=0; i < nbBlocks; i++){
    //         PrintMapH5Mpi(h5blocks->maph5mpi[i].mpirr);
    //     }
    //     for (int i=0; i < nbtilesrows; i++){
    //         for (int j=0; j < nbtilescols; j++){
    //             nbtileperproc[chameleon_rankofTiles_h5[i][j]] +=1;
    //         }
    //     }
    // }
    //time.tac();
    //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:readHDF5Panel:init=" << time.elapsed() << std::endl;
    delete [] nbrowsh5remain;
    delete [] rowsidxh5;
    delete [] colsidxh5;
    delete [] nbvalproc;

    //time.tic();

    /* decide if the chameleon matrix must be considered as symmetric */
    const char* issym_s = getenv("FMR_CHAMELEON_READH5_SYM");
    bool issym_b = data.isSymmetric();
    if ( issym_s != NULL ){
        int issym_i = std::stoi(issym_s);
        if ( issym_i == 1 ){
            issym_b = true;
        }
    }

    /* allocate the chameleon matrix by panel (set values to 0.) */
    /* set new sizes */
    data.reset(h5blocks->dimsglob[0], h5blocks->dimsglob[1], issym_b);
    /* allocate matrix */
    data.allocate(false, true);
    /* give h5blocks pointer in the async case, in order to be able to
    deallocate it */
    data.setH5Blocks(h5blocks);
    /* get chameleon matrix pointer for what follows */
    CHAM_desc_t *desc = data.getMatrix();

    if (mpirank == 0) std::cout << "[fmr] chameleon UpperLower : " << data.getUpperLower() << "\n";

    /* Read sub blocks with starpu */

    int ih5 = 0;
    MpiRanks *mpiranks = h5blocks->maph5mpi[ih5].mpirr->firstmpiranks;
    IdxInt *rows = h5blocks->maph5mpi[ih5].mpirr->firstrows;
    IdxInt *cols = h5blocks->maph5mpi[ih5].mpirr->firstcols;
    IdxInt *rowsglo = h5blocks->maph5mpi[ih5].mpirr->firstrowsglo;
    IdxInt *colsglo = h5blocks->maph5mpi[ih5].mpirr->firstcolsglo;

    int nsubblocks = 0;
    for (int i=0; i < nbBlocks; i++) {
        nsubblocks += h5blocks->maph5mpi[i].mpirr->nb;
    }
    /* array of starpu handles for H5 sub blocks */
    h5blocks->handlesH5 = new starpu_data_handle_t[nsubblocks]();
    /* id of this handles array to properly manage mpi tags to use. we
    consider here that 100 other handles descriptors may exist. */
    int handlesH5_id = 100;

    /* init the StarPU codelet structures to read H5 blocks and copy from H5
    blocks to chameleon tiles */
    h5blocks->cl_inittile = new struct starpu_codelet;
    starpu_codelet_init(h5blocks->cl_inittile);
    h5blocks->cl_inittile->where = STARPU_CPU;
    h5blocks->cl_inittile->cpu_funcs[0] = cl_inittile_cpu_func<FReal>;
    h5blocks->cl_inittile->nbuffers = 1;
    h5blocks->cl_inittile->name = "inittile";

    #ifdef FMR_DLOPEN_HDF5
    h5blocks->cl_endhdf5 = new struct starpu_codelet;
    starpu_codelet_init(h5blocks->cl_endhdf5);
    h5blocks->cl_endhdf5->where = STARPU_CPU;
    h5blocks->cl_endhdf5->cpu_funcs[0] = cl_endhdf5_cpu_func;
    h5blocks->cl_endhdf5->nbuffers = 1;
    h5blocks->cl_endhdf5->name = "endhdf5";

    int nworker_read = dl_diodon_nworkers_read();

    /* id of this handles array to properly manage mpi tags to use */
    int handlesH5_dlib_id = 200;

    /* create the handles to store the dlopen struct ref
       TODO: handles not used by this mpirank can be optimized out */
    starpu_data_handle_t* handle_hdf5_worker = (starpu_data_handle_t*)malloc(sizeof(starpu_data_handle_t) * nworker_read * mpinp);
    hdf5_dyn_functions_t** HDF5_handler_worker = (hdf5_dyn_functions_t**)malloc(sizeof(hdf5_dyn_functions_t*) * nworker_read * mpinp);
    for(int n=0; n<mpinp; n++){
      int worker_atual = n*nworker_read;
      for(int i=0; i<nworker_read;i++){
        int home_node = -1;
        /* only opens the dlopen struct to handles of this rank */
        if(n==mpirank){
          HDF5_handler_worker[worker_atual + i] = create_hdf5_instance(mpirank, i);
          dl_open_file_read(nbBlocks, HDF5_handler_worker[worker_atual + i], h5blocks->h5FileNames, h5blocks->dataSetNames);
          home_node = STARPU_MAIN_RAM;
        }else{
          HDF5_handler_worker[worker_atual + i] = NULL;
        }
        starpu_vector_data_register(&handle_hdf5_worker[worker_atual + i], home_node, (uintptr_t)HDF5_handler_worker[worker_atual + i], 1, sizeof(hdf5_dyn_functions_t*) );
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
        starpu_mpi_data_register(handle_hdf5_worker[worker_atual + i], (int64_t)((int64_t)handlesH5_dlib_id << (int64_t)32) | (n * nworker_read + i), n);
#endif
        starpu_data_set_name(handle_hdf5_worker[worker_atual + i], "DLOPEN");
      }
    }
    h5blocks->cl_readh5 = new struct starpu_codelet;
    starpu_codelet_init(h5blocks->cl_readh5);
    h5blocks->cl_readh5->where = STARPU_CPU;
    h5blocks->cl_readh5->cpu_funcs[0] = cl_readh5dl_cpu_func<FReal>;
    h5blocks->cl_readh5->nbuffers = 2;
    h5blocks->cl_readh5->name = "readh5";

    /* configure scheduler ctx, each worker that can receive a read will have an
     isolated ctx, non read workers will be isolated as well */
    unsigned int n_non_read_workers = (starpu_cuda_worker_get_count()+starpu_opencl_worker_get_count()+starpu_cpu_worker_get_count()-nworker_read);
    int *workers_read = (int*)malloc(1 * sizeof(int));
    unsigned int *ctx_read_id = (unsigned int*)malloc(nworker_read * sizeof(unsigned int));
    int *workers_nonread = (int*)malloc(n_non_read_workers * sizeof(int));

    for(unsigned int i=0; i < n_non_read_workers; i++){
      workers_nonread[i]=(i>=(starpu_cuda_worker_get_count()+starpu_opencl_worker_get_count()))?i+nworker_read:i;
    }
    unsigned int ctx_noread_id = starpu_sched_ctx_create(workers_nonread, n_non_read_workers, "read", STARPU_SCHED_CTX_POLICY_NAME, "", 0);

    for(int i=0; i<nworker_read; i++){
      workers_read[0] = starpu_cuda_worker_get_count()+starpu_opencl_worker_get_count()+i;
      ctx_read_id[i] = starpu_sched_ctx_create(workers_read, 1, "read", STARPU_SCHED_CTX_POLICY_NAME, "", 0);
      starpu_sched_ctx_set_inheritor(ctx_read_id[i], ctx_noread_id);
    }

    free(workers_read);
    free(workers_nonread);

    int* read_workers_rb = (int*) calloc(mpinp, sizeof(int));
    #else
    h5blocks->cl_readh5 = new struct starpu_codelet;
    starpu_codelet_init(h5blocks->cl_readh5);
    h5blocks->cl_readh5->where = STARPU_CPU;
    h5blocks->cl_readh5->cpu_funcs[0] = cl_readh5_cpu_func<FReal>;
    h5blocks->cl_readh5->nbuffers = 1;
    h5blocks->cl_readh5->name = "readh5";
    #endif

    h5blocks->cl_copypaneltotile = new struct starpu_codelet;
    starpu_codelet_init(h5blocks->cl_copypaneltotile);
    h5blocks->cl_copypaneltotile->where = STARPU_CPU;
    h5blocks->cl_copypaneltotile->cpu_funcs[0] = cl_copypaneltotile_cpu_func<FReal>;
    h5blocks->cl_copypaneltotile->nbuffers = 2;
    h5blocks->cl_copypaneltotile->name = "copypaneltotile";

    /* H5 parameters to handle subset selection in the file */
    hsize_t start[2];
    hsize_t count[2];

    int **chameleon_inittiles;
    chameleon_inittiles = new int*[nbtilesrows];
    for (int i=0; i < nbtilesrows; i++){
        chameleon_inittiles[i] = new int[nbtilescols];
        for (int j=0; j < nbtilescols; j++){
            chameleon_inittiles[i][j] = 0;
        }
    }

    /* main loop over all H5 sub blocks to read */
    for (int igl=0; igl < nsubblocks; igl++){

        /* global tile index on rows */
        int it = rowsglo->idx[0]/tilesize;

        /* global tile index on cols */
        int jt = colsglo->idx[0]/tilesize;
        if ( data.getUpperLower() == ChamUpper ) {
            jt = std::max( it, jt );
        }

        start[0]  = rows->idx[0];
        start[1]  = cols->idx[0];
        count[0]  = rows->idx[1]-rows->idx[0]+1;
        count[1]  = cols->idx[1]-cols->idx[0]+1;
        hsize_t buffersize = count[0]*count[1];

        while ( (size_t)(jt*tilesize) <= colsglo->idx[1] ) {
            if (chameleon_inittiles[it][jt] == 0) {
                /* get the chameleon tile starpu handle */
                starpu_data_handle_t th = (starpu_data_handle_t)Chameleon::runtime_data_getaddr( desc, it, jt );
                /* submit task to init tile */
                insert_inittile( h5blocks->cl_inittile, th);
                chameleon_inittiles[it][jt] = 1;
            }
            jt++;
        }
        jt = colsglo->idx[0]/tilesize;
        if ( data.getUpperLower() == ChamUpper ) {
            jt = std::max( it, jt );
        }

        /* allocate the data if we are the node and register our buffer to starpu
         and get the starpu struct opaque pointer to give to the task */
        FReal* ptr = NULL;
        if(mpirank == mpiranks->rank){
          ptr = (FReal*)malloc(buffersize * sizeof(FReal));
        }
        starpu_data_handle_t ph = get_starpu_handle<FReal>( handlesH5_id, h5blocks->handlesH5, igl, ptr, buffersize, mpirank, mpiranks->rank);


        /* submit the task to read one H5 sub block by panel (range of
        lines) */
        #ifdef FMR_DLOPEN_HDF5
        /* the read tasks are associated with a libhdf5 instance in rb */
        starpu_sched_ctx_set_context(&ctx_read_id[read_workers_rb[mpiranks->rank]]);
        insert_readh5dl( h5blocks->cl_readh5, count[0], count[1], start[0], start[1], ih5, ph, handle_hdf5_worker[mpiranks->rank * nworker_read + read_workers_rb[mpiranks->rank]], it, jt);
        read_workers_rb[mpiranks->rank] = (read_workers_rb[mpiranks->rank] + 1) % nworker_read;
        starpu_sched_ctx_set_context(&ctx_noread_id);
        #else
        insert_readh5( h5blocks->cl_readh5,
                       h5blocks->dataSets[ih5], h5blocks->dataSpaces[ih5],
                       count[0], count[1], start[0], start[1], ph);
        #endif

        /* copy tiles from the panel into the chameleon matrix */
        while ( (size_t)(jt*tilesize) <= colsglo->idx[1] ) {

            /* test whether or not to read the tile and consider symmetry
            depending on the formats of both the H5 data (given in FULL
            format or not) and chameleon (UpperLower or not) */
            int itt = -1;
            int jtt = -1;
            if ( h5blocks->format == "FULL" ) {
                itt = it;
                jtt = jt;
            } else {
                if ( data.getUpperLower() == ChamUpperLower ) {
                    itt = it;
                    jtt = jt;
                } else {
                    if ( data.getUpperLower() == ChamLower && h5blocks->format == "LOWER" && jt <= it ) {
                        itt = it;
                        jtt = jt;
                    }
                    if ( data.getUpperLower() == ChamLower && h5blocks->format == "UPPER" && it <= jt ) {
                        itt = jt;
                        jtt = it;
                    }
                    if ( data.getUpperLower() == ChamUpper && h5blocks->format == "UPPER" && it <= jt ) {
                        itt = it;
                        jtt = jt;
                    }
                    if ( data.getUpperLower() == ChamUpper && h5blocks->format == "LOWER" && jt <= it ) {
                        itt = jt;
                        jtt = it;
                    }
                }
            }

            /* fill the chameleon tile */
            if ( itt != -1 && jtt != -1 ) {
                /* get the chameleon tile starpu handle */
                starpu_data_handle_t th = (starpu_data_handle_t)Chameleon::runtime_data_getaddr( desc, itt, jtt );
                /* submit task to copy data from H5 panel to fill one chameleon tile */
                insert_copypaneltotile( h5blocks->cl_copypaneltotile,
                                        rowsglo->idx[0], rowsglo->idx[1], colsglo->idx[0], colsglo->idx[1], ph,
                                        data.getUpperLower(), desc, itt, jtt, th);
            }

            /* fill symmetric part under specific conditions */
            if ( h5blocks->format != "FULL" &&
                    data.getUpperLower() == ChamUpperLower &&
                    h5blocks->index[ih5].first != h5blocks->index[ih5].second &&
                    it != jt ) {
                starpu_data_handle_t th = (starpu_data_handle_t)Chameleon::runtime_data_getaddr( desc, jt, it );
                insert_copypaneltotile( h5blocks->cl_copypaneltotile,
                                        rowsglo->idx[0], rowsglo->idx[1], colsglo->idx[0], colsglo->idx[1], ph,
                                        data.getUpperLower(), desc, jt, it, th);
            }
            jt++;
        }

        /* clean H5 panel and save memory */
        if(mpirank == mpiranks->rank){
          free_unregister_starpu_handle( 1, &ph );
        }else{
          unregister_starpu_handle( 1, &ph );
        }

        /* consider next subblock in the list */
        mpiranks = mpiranks->next;
        rows  = rows->next;
        cols  = cols->next;
        rowsglo = rowsglo->next;
        colsglo = colsglo->next;
        /* if end of the list handling this H5 file consider list of the next H5 file */
        if ( mpiranks == NULL ) {
            if ( ih5+1 < nbBlocks ) {
                ih5++;
                mpiranks = h5blocks->maph5mpi[ih5].mpirr->firstmpiranks;
                rows = h5blocks->maph5mpi[ih5].mpirr->firstrows;
                cols = h5blocks->maph5mpi[ih5].mpirr->firstcols;
                rowsglo = h5blocks->maph5mpi[ih5].mpirr->firstrowsglo;
                colsglo = h5blocks->maph5mpi[ih5].mpirr->firstcolsglo;
            }
        }
    }
    for (int i = 0; i < nbtilesrows; ++i) {
      delete[] chameleon_inittiles[i];
    }
    delete[] chameleon_inittiles;

    #ifdef FMR_DLOPEN_HDF5
    /* unregister the dlopen struct */
    for(int n=0; n<mpinp; n++){
      int worker_atual = n*nworker_read;
      for(int i=0; i<nworker_read;i++){
        if(n==mpirank){
          insert_closeh5dl(h5blocks->cl_endhdf5, handle_hdf5_worker[worker_atual + i]);
        }
      }
    }

    unregister_starpu_handle(mpinp * nworker_read, handle_hdf5_worker);
    /* close ctxs, read workers will be inherited by the other ctx */
    for(int i=0; i<nworker_read; i++){
      starpu_sched_ctx_finished_submit(ctx_read_id[i]);
    }
    free(ctx_read_id);
    free(read_workers_rb);
    free(HDF5_handler_worker);
    free(handle_hdf5_worker);
    #endif

    //time.tac();
    //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:readHDF5Panel:read=" << time.elapsed() << std::endl;

    // we have read the matrix in a 2d no cyclic distribution, migrate data
    // to get a 2dcyclic distribution, more suited to subsequent operations
    // (gram, gemm, qr, etc)
    //time.tic();

    if ( data.isSymmetric() ){
        bool tbc = Chameleon::mapping_tbc();
        int sbcr = Chameleon::mapping_sbc_r();
        if ( tbc > 0 ){
            Chameleon::restore_tbc(data.getUpperLower(), data.getMatrix());
        } else if ( sbcr > 0 ) {
            Chameleon::restore_sbc(data.getUpperLower(), data.getMatrix());
        } else {
            Chameleon::restore_2dcyclic(data.getUpperLower(), data.getMatrix());
        }
    } else {
        Chameleon::restore_2dcyclic(data.getUpperLower(), data.getMatrix());
    }
    //time.tac();
    //if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:readHDF5Panel:datamigrate=" << time.elapsed() << std::endl;

    /* Need to add a runtime synchronization barrier in the synchrone case  */
    bool async;
    Chameleon::async_manager(false, false, false, &async, NULL, NULL);
    if ( !async ){
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
        starpu_mpi_wait_for_all(MPI_COMM_WORLD);
#else
        starpu_task_wait_for_all();
#endif
    }
}

/**
 * @brief copy the element from one or several HDF5 files in the descriptor
 *
 * @param[out] data the dense wrapper matrix to fill
 * @param[in] filename    HDF5 filename on disk or metadata txt file.
 *                        If the global matrix is made of several hdf5 files the
 *                        metadata ASCII file should contain the following information:
 *                        Number of blocks e.g. 3
 *                        triangular part of the matrix given by the blocks e.g. upper
 *                        number line, number column of the block, file name, dataset name e.g.
 *                        0 0 atlas_guyane_trnH_0.h5  distance
 *                        0 1 atlas_guyane_trnH_1.h5  distance
 *                        0 2 atlas_guyane_trnH_2.h5  distance
 *                        1 1 atlas_guyane_trnH_3.h5  distance
 *                        1 2 atlas_guyane_trnH_4.h5  distance
 *                        2 2 atlas_guyane_trnH_5.h5  distance
 * @param[in] datasetname HDF5 dataset to consider (several datasets can be
 *            stored in one file) or root directory of the h5 files.
 */
template<class FSize, class FReal>
void readHDF5(ChameleonDenseWrapper<FSize, FReal> &data, const std::string& filename, const std::string& datasetname)
{
    // decide the H5 reading distribution over MPI processus
    const char* bytile_s = getenv("FMR_CHAMELEON_READH5_BYTILE");
    bool bytile_b = false;
    if ( bytile_s != NULL ){
        int bytile_i = std::stoi(bytile_s);
        if ( bytile_i == 1 ){
            bytile_b = true;
        }
    }
    const char* bytile1d_s = getenv("FMR_CHAMELEON_READH5_BYTILE_DISTRIB_1D");
    bool bytile1d_b = false;
    if ( bytile1d_s != NULL ){
        int bytile_i = std::stoi(bytile1d_s);
        if ( bytile_i == 1 ){
            bytile1d_b = true;
        }
    }

    /* limit the number of submitted tasks to avoid memory over consumption */
    const char* maxtasks_s = getenv("FMR_CHAMELEON_READH5_MAXTASKS");
    bool window_b = false;
    unsigned maxtasks;
    if ( maxtasks_s != NULL ){
        maxtasks = static_cast<unsigned>(std::stoi(maxtasks_s));
        if ( maxtasks > 1000 ) {
            window_b = true;
            RUNTIME_set_minmax_submitted_tasks( maxtasks-1000, maxtasks );
        }
    }

    if ( bytile_b || bytile1d_b ){
        if (filename.find(".h5") != std::string::npos) {
            readHDF5Tile(data, filename, datasetname);
        } else {
            readHDF5TileMulti(data, filename, datasetname);
        }
    } else {
        readHDF5Panel(data, filename, datasetname);
    }

    /* restore a reasonnable window */
    if ( window_b ) {
        RUNTIME_set_minmax_submitted_tasks( 9000, 10000 );
    }
}

/**
 * @brief saves the matrix in HDF5 file
 *
 * @param[in] data        matrix to save.
 * @param[in] filename    HDF5 file name.
 * @param[in] datasetname HDF5 dataset name.
 * @param[in] ncolstosave number of columns to save if lower than number of
 * columns of the matrix. If set to 0 (default) we save all columns.
 */
template<class FSize, class FReal>
void writeHDF5(ChameleonDenseWrapper<FSize, FReal> &data, const std::string& filename, const std::string& datasetname, size_t ncolstosave=0)
{
    /* need to add a runtime barrier here because we will call MPI on our own just after */
    Chameleon::barrier();

    bool mpiinit = Chameleon::mpi_isinit();
    int mpirank = Chameleon::mpi_rank();
    int mpinp = Chameleon::mpi_nprocs();

    int tilesize;
    Chameleon::get(CHAMELEON_TILE_SIZE, &tilesize);

    bool isTimed = false;
    tools::Tic time;
    const char* envTime_s = getenv("FMR_TIME");
    if ( envTime_s != NULL ){
        int envTime_i = std::stoi(envTime_s);
        if ( envTime_i == 1 ){
            isTimed = true;
        }
    }
    if ( isTimed ) {
        time.tic();
    }

#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;
    /*
     * Set up file access property list with parallel I/O access
     */
    hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm, info);
#else
    hid_t plist_id = H5P_DEFAULT;
#endif

    /* Open or Create File*/
    hid_t file_id;
    if ( mpirank == 0 ) {
        H5E_BEGIN_TRY
        /* if file already exists open it instead of creating it */
        file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        H5E_END_TRY
        if ( file_id >= 0 ) {
            /* if dataset already exist delete it */
            htri_t datasetexist = -1;
            H5E_BEGIN_TRY
            datasetexist = H5Lexists(file_id, datasetname.c_str(), H5P_DEFAULT);
            H5E_END_TRY
            if ( datasetexist > 0 ) {
                H5Ldelete(file_id, datasetname.c_str(), H5P_DEFAULT);
            }
            H5Fclose(file_id);
        }
    }
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
       if ( mpiinit ){
           MPI_Bcast(&file_id, 1, MPI_INT64_T, 0, MPI_COMM_WORLD);
       }
#endif

    if ( file_id < 0 ) {
        /* create a new file collectively */
        file_id = H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    } else {
        /* open file collectively */
        file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, plist_id);
    }
    H5Pclose(plist_id);

    FSize nrows = data.getNbRows();
    FSize ncols = (ncolstosave==0) ? data.getNbCols() : (FSize)ncolstosave;

    tools::Tic timeWrite;
    if (isTimed) {
        Chameleon::barrier();
        timeWrite.tic();
    }
    /* Create datatype. */
    const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE ;
    /*
     * Create the dataspace for the dataset.
     */
    hsize_t fdim[2] = {(hsize_t)nrows, (hsize_t)ncols};
    hid_t filespace = H5Screate_simple(2, fdim, NULL);
    /*
     * Create chunked dataset.
     */
    FSize chunkrow = std::min((FSize)tilesize, nrows);
    FSize chunkcol = std::min((FSize)tilesize, ncols);
    hsize_t cdim[2] = {(hsize_t)chunkrow, (hsize_t)chunkcol};
    plist_id = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(plist_id, 2, cdim);
    hid_t dset_id = H5Dcreate(file_id, datasetname.c_str(), H5_TYPE, filespace,
                                H5P_DEFAULT, plist_id, H5P_DEFAULT);
    H5Pclose(plist_id);
    H5Sclose(filespace);
    if (isTimed) {
        Chameleon::barrier();
        timeWrite.tac();
        if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:writeH5:DATASET:INIT=" << timeWrite.elapsed() << " s" << "\n";
    }

    /* perform the selection over blocks of (data.getMatrix()->mt/mpinp) tile rows */
    CHAM_desc_t *desc = data.getMatrix();
    FSize nBlocks;
    FSize nTilePerProc = desc->mt / mpinp;
    if (nTilePerProc == 0){
        nTilePerProc = 1;
        nBlocks = desc->mt;
    } else {
        nBlocks = mpinp;
    }
    FSize offsetMPI = 0;
    FSize nrowsMPI = 0;
    std::vector<FReal> submat;

    if (isTimed) {
        Chameleon::barrier();
        timeWrite.tic();
    }

    /* array of starpu handles for H5 sub blocks */
    starpu_data_handle_t *handlesH5 = new starpu_data_handle_t[nBlocks]();
    /* id of this handles array to properly manage mpi tags to use. we
    consider here that 101 other handles descriptors may exist. */
    int handlesH5_id = 101;

    struct starpu_codelet cl_copytiletopanel;
    starpu_codelet_init(&cl_copytiletopanel);
    cl_copytiletopanel.where = STARPU_CPU;
    cl_copytiletopanel.cpu_funcs[0] = cl_copytiletopanel_cpu_func<FReal>;
    cl_copytiletopanel.nbuffers = 2;
    cl_copytiletopanel.name = "copytiletopanel";

    /* loop to collect contiguous blocks and send to the right MPI process */
    for (FSize k=0; k < nBlocks ; ++k){

        /* offset and size of the block */
        FSize rowIdx = k*nTilePerProc*desc->mb;
        FSize nrowsloc = (k == nBlocks-1) ? desc->lm - rowIdx : nTilePerProc*desc->mb ;
        hsize_t buffersize = nrowsloc*ncols;
        //if (mpirank == 0) printf("[fmr] Chameleon writeH5 rowIdx %llu nrows %llu\n", rowIdx, nrowsloc);

        /* each rank will handle a block during H5 writing */
        FReal *panel = NULL;
        if ( (FSize)mpirank == k ){
            offsetMPI = rowIdx;
            nrowsMPI = nrowsloc;
            submat.resize(buffersize);
            panel = submat.data();
        }

        /* register our buffer to starpu and get the starpu struct opaque
        pointer to give to the task */
        starpu_data_handle_t ph = get_starpu_handle<FReal>( handlesH5_id, handlesH5, k, panel, buffersize, mpirank, k);

        /* global tile index on rows */
        FSize it = rowIdx/tilesize;

        /* copy tiles from the chameleon matrix into the panel */
        while ( it*tilesize < rowIdx + nrowsloc ) {

            /* global tile index on cols */
            int jt = 0;

            while ( (FSize)jt*tilesize < ncols ) {
                /* get the chameleon tile starpu handle */
                starpu_data_handle_t th = (starpu_data_handle_t)Chameleon::runtime_data_getaddr( desc, it, jt );
                /* submit task to copy data from H5 panel to fill one chameleon tile */
                insert_copytiletopanel( &cl_copytiletopanel,
                                        data.getUpperLower(), desc, it, jt, th,
                                        rowIdx, rowIdx+nrowsloc-1, 0, ncols-1, ph);
                jt++;
            }
            it++;
        }
    }

    /* Let's submit unregistration of all data handlers */
    unregister_starpu_handle( nBlocks, handlesH5 );

    /* Let's wait for the end of all the tasks */
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    starpu_mpi_wait_for_all(MPI_COMM_WORLD);
#else
    starpu_task_wait_for_all();
#endif
    delete [] handlesH5 ;

    if (isTimed) {
        timeWrite.tac();
        if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:writeH5:SCATTER=" << timeWrite.elapsed() << " s" << "\n";
    }

    /*
     * Each process defines dataset in memory and writes it to the hyperslab
     * in the file.
     */
    if (isTimed) {
        Chameleon::barrier();
        timeWrite.tic();
    }
    hsize_t offset[2], stride[2], count[2], block[2];
    offset[0] = (hsize_t)offsetMPI;
    offset[1] = 0;
    stride[0] = 1;
    stride[1] = 1;
    count[0] = 1;
    count[1] = 1;
    block[0] = (hsize_t)nrowsMPI;
    block[1] = (hsize_t)ncols;
    /* create memory space */
    hid_t memspace  = H5Screate_simple(2, block, NULL);
    /* select hyperslab in the file */
    filespace = H5Dget_space(dset_id);
    H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, stride, count, block);
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
    /*
     * Create property list for collective dataset write.
     */
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
#else
    plist_id = H5P_DEFAULT;
#endif

    /* write in H5 file */
    H5Dwrite(dset_id, H5_TYPE, memspace, filespace, plist_id, submat.data());

    /*
     * Close/release resources.
     */
    H5Dclose(dset_id);
    H5Sclose(filespace);
    H5Sclose(memspace);
    H5Pclose(plist_id);
    H5Fclose(file_id);
    Chameleon::barrier();
    if (isTimed) {
        timeWrite.tac();
        if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:writeH5:WRITE=" << timeWrite.elapsed() << " s" << "\n";
        time.tac();
        if (mpirank == 0) std::cout << "[fmr] TIME:CHAMELEON:writeH5=" << time.elapsed() << std::endl;
    }
}

#endif

}} /* namespace fmr::io */

#endif // MATRIXIOCHAMELEON_HPP
