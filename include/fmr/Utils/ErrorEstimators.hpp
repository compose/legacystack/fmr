/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef ERRORESTIMATORS_HPP
#define ERRORESTIMATORS_HPP


// Utilities
#include "fmr/Utils/Global.hpp"
#include "fmr/Utils/Parameters.hpp"
#include "fmr/Utils/Math.hpp"

namespace fmr{

/**
 * @brief The Error Estimators class
 */
template<class Real>
class ErrorEstimators {

public:

    /*
     * Computes and returns the lower bound for any Randomized Range Finder
     * and both Frobenius and Spectral norms
     */
    static const Real lowerBound() {
        return Real(0.);
    }

    /*
     * Computes and returns the upper bound for any Randomized Range Finder
     * in Frobenius norm
     * TODO add expression in latex
     */
    static Real upperAverageBoundFrobenius(const int qRSI, const Size k, const int p) {
        if(qRSI==0) // RRF
            return Real(Math::Sqrt(Real(1.)+Real(k)/Real(p-1))) - Real(1.);
        else { //RSI
            //BEWARE! The upper average bound when q>0 is not given by Halko,
            // because no proof yet!! Therefore we use the bound for q=0.
            return Real(Math::Sqrt(Real(1.)+Real(k)/Real(p-1))) - Real(1.);
        }
    }

    /*
     * Computes the error estimator for any Randomized Range Finder
     * in Spectral norm
     * TODO add expression in latex
     *
     * \param residual_energy contains value of $(\sum_{j>k} \sigma_j(A)^2)^{1/2}$ if known otherwise replace by its upper bound, namely $\sqrt{min(m,n)-k}$.
     */
    static Real upperAverageBoundSpectral(const int qRSI, const Size k, const int p, const Real residual) {
        if(qRSI==0) // RRF
            return Real(Real(1.)+Math::Sqrt(Real(k)/Real(p-1))+Math::Exp(Real(0.))*Math::Sqrt(Real(k+p))/Real(p)*residual) - Real(1.);
        else { //RSI
            // Halko p.277
            return Math::pow(Real(Real(1.)+Math::Sqrt(Real(k)/Real(p-1))+Math::Exp(Real(0.))*Math::Sqrt(Real(k+p))/Real(p)*residual),Real(1.)/Real(2*qRSI+1)) - Real(1.);
        }
    }

};

} /* namespace fmr */

#endif // ERRORESTIMATORS_HPP
