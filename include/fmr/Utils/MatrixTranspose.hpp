/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXTRANSPOSE_HPP
#define MATRIXTRANSPOSE_HPP

// FMR includes
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/Utils/Starpu.hpp"
#include "fmr/Utils/Chameleon.hpp"
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif

namespace fmr {

/**
 * @brief Computes the transpose of a general matrix
 * @param[in] matrixin input dense wrapper to transpose
 * @param[out] matrixout the transposed matrix
 */
template<class FSize, class FReal>
void computeTranspose(BlasDenseWrapper<FSize, FReal> &matrixin, BlasDenseWrapper<FSize, FReal> &matrixout) {
  FSize nbrows = matrixin.getNbRows();
  FSize nbcols = matrixin.getNbCols();
  FReal *matin = matrixin.getMatrix();
  FReal *matout = matrixout.getMatrix();
  for (FSize i = 0; i < nbrows; ++i) {
    for (FSize j = 0; j < nbcols; ++j) {
      matout[ j + i*nbcols ] = matin[ i + j*nbrows ];
    }
  }
}

#if defined(FMR_CHAMELEON)
/**
 * @brief Computes the transpose of a general matrix
 * @param[in] matrixin input dense wrapper to transpose
 * @param[out] matrixout the transposed matrix
 */
template<class FSize, class FReal>
void computeTranspose(ChameleonDenseWrapper<FSize, FReal> &matrixin, ChameleonDenseWrapper<FSize, FReal> &matrixout) {

  CHAM_desc_t* descin = matrixin.getMatrix();
  CHAM_desc_t* descout = matrixout.getMatrix();

  struct starpu_codelet cl_transpose;
  starpu_codelet_init(&cl_transpose);
  cl_transpose.where = STARPU_CPU;
  cl_transpose.cpu_funcs[0] = cl_transpose_cpu_func<FReal>;
  cl_transpose.nbuffers = 2;
  cl_transpose.name = "transpose";

  for (int jt = 0; jt < descin->nt; jt++) {
    for (int it = 0; it < descin->mt; it++) {
      starpu_data_handle_t tilein = (starpu_data_handle_t)Chameleon::runtime_data_getaddr( descin, it, jt );
      starpu_data_handle_t tileout = (starpu_data_handle_t)Chameleon::runtime_data_getaddr( descout, jt, it );
      /* submit task to transpose a matrix */
      insert_transpose( &cl_transpose, it, jt, descin, descout, tilein, tileout);
    }
  }

  /* This is a synchronous funtion, need to add a runtime synchronization barrier */
#if defined(FMR_USE_MPI) && defined(CHAMELEON_USE_MPI)
  starpu_mpi_wait_for_all(MPI_COMM_WORLD);
#else
  starpu_task_wait_for_all();
#endif

}
#endif

} /* namespace fmr */

#endif // MATRIXTRANSPOSE_HPP
