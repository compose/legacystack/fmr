/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef QRF_HPP
#define QRF_HPP

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#include "fmr/MatrixWrappers/MatrixWrapperTraits.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#include "fmr/Utils/Global.hpp"

namespace fmr {
/**
 * @brief The QRF class
 *
 *  QR Factorization class
 *
 *  How to use it ?
 *  Let A a MatrixWrapper Object and you want to perform a QR factorization
 *  We procced as follow
 *
 *  \code
 *  // Creating a QR wrapper and set Q in A
 *  // WARNING Q object of the class is an alias on A
 *  QRF<MatrixWrapper> QR(A);
 *  // perform the QR facttorization
 *  QR.factorize() ;
 * // to obtain Q in object A
 *  auto R = QR.makeR()
 *  // to build Q and put it in A
 *  QR.makeQ()
 *  \endcode
 * \warning after the factorization, the matrix passed in the constructeur is no
 * longer valid
 *
 */
template <class MatrixWrapper> class QRF {
protected:
  using int_type = typename MatrixWrapper::int_type;
  using value_type = typename MatrixWrapper::value_type;
  using TDesc =
      typename MatrixWrapper::Tau_type; ///< a pointer type on the tau data type
  using TTree =
      typename MatrixWrapper::Tree_type; ///< a pointer type on the tree
                                         ///< structure (if exists)
  using matrix_type = MatrixWrapper;

  bool _isFactorized;
  bool _isBuiltQ;
  bool _isBuiltR;

  MatrixWrapper &_Q; ///< A reference on the matrix we factorize
  MatrixWrapper _R;  ///< The R part of the factorization

  TDesc _TS; // TS auxiliary factorization data  - Tau structure that  contains
             // elementary reflectors
  TDesc _TT; // TT auxiliary factorization data
  TTree _QRTREE; // tree for QR with chameleon+hqr

public:
  /**
   * @brief Constructor for Blas
   *
   * @param[inout] A : matrix to factorize.
   * The matrix Q of the class is a reference on the matrix A;
   *
   * @warning The matrix A will be overwritten by the factorize method of this
   * class.
   */
  QRF(MatrixWrapper &A)
      : _isFactorized(false), _isBuiltQ(false), _isBuiltR(false), _Q(A),
        _R(std::min(A.getNbRows(), A.getNbCols()), A.getNbCols()), _TS(nullptr),
        _TT(nullptr), _QRTREE(nullptr) {}
  /**
   * @brief set Matrix to the class
   *
   * @param[in] Mat the matrix to factorize
   */
  void setMatrix(MatrixWrapper &Mat) {
    if (&Mat == &_Q) {
      std::cerr << "QRF<MatrixWrapper> already points on this matrix\n";
      throw std::invalid_argument(
          "QRF<MatrixWrapper>  already points on this matrix");
    }
    // std::clog << " setMatrix::mat(Y);" << &Mat << " _Q " << &_Q <<
    // std::endl;
    this->free();
    // std::clog << " setMatrix::   end free step\n";

    _isFactorized = false;
    _isBuiltQ = false;
    _isBuiltR = false;
    _Q.reset(Mat);
    // std::clog << "    setMatrix:: set S" << std::endl;
    //    std::clog << " setMatrix::_Q.data()" << _Q.getMatrix() << std::endl;

    //
    _R.initAndAllocate(std::min(Mat.getNbRows(), Mat.getNbCols()),
                       Mat.getNbCols());
    _TS = nullptr;
    _TT = nullptr;
    _QRTREE = nullptr;
  }

  /**
   * @brief factorize computes the QR factorization of A by calling the QRD
   * decomposition corresponding to the wrapper.
   *
   * The QR decomposition follows the LaPACK QR factorization.
   * The upper triangle matrix, R,  is stored in Q then befor using Q or R
   * you should do makeR() to build then makeQ() for Q.
   *
   * @param[in] force if true we force the factorization even if _isFactorized is at true
   * @return 0 if it factorized the matrix, a negative value if an error
   * occured
   */
  int factorize(const bool force = false) {
    if (force) {
      _isFactorized = false;
    }
    else if ( _isFactorized ) {
      std::clog << "Warning QRD::factorize() matrix already Factorized\n" << std::endl;
      return -1;
    }

    this->computeQRD(this->_Q); // To call the right method
    _isFactorized = true;
    return 0;
  }

  int_type getNbRows() const { return _Q.getNbRows(); }

  int_type getNbCols() const { return _Q.getNbCols(); }

  ///
  /// \brief chek is the matrix isFactorizeditherise the matrix contains
  /// reflectors \return a boolean to say if the matrix is factorized
  ///
  bool isQBuilt() const { return _isBuiltQ; }

  ///
  /// \brief chek is the matrix Q is built
  /// \return a boolean to say if the matrix Q is built
  ///
  bool isFactorized() const { return _isFactorized; }
  /**
   * @brief makeQ will build the matrix Q corresponding to the QR
   * factorization of A.
   *
   * @param[in] saveR : save coefficicents of R in a member variable of this
   * class before building Q
   *
   * @return 0 On success, a negative value otherwise.
   */
  int makeQ(const bool saveR = false) {
    if (!_isFactorized) {
      int err = factorize();
      if (err != 0) {
        std::cerr << "Error in QRF::makeQ factorize() has failed" << std::endl;
        return -1;
      }
    }
    if (saveR) {
      int err = this->makeR();
      if (err != 0) {
        std::cerr << "Error in QRF::makeQ makeR() has failed" << std::endl;
        return -2;
      }
    }
    if (!_isBuiltQ) {
      this->buildQ(this->_Q); // Q is here to specialize the function to call
    }
    return 0;
  }
  ///
  /// \brief Return a reference on the Q  MatrixWrapper object of the class
  ///
  /// \return a reference on either the original matrix or the orthogonal
  /// matrix
  /// Q
  ///
  MatrixWrapper &getQ() {
    if (!_isBuiltQ) {
      this->makeQ();
    }
    return _Q;
  }
  /**
   * @brief makeR copies the matrix upper triangle from the QR decomposition
   * (data in Q) into R
   *
   * @return 0 On success, a negative value otherwise.
   */
  int makeR() {
    if (!_isFactorized) {
      int err = factorize();
      if (err != 0) {
        std::cerr << "Error in QRF::makeR factorize has failed" << std::endl;
        return -1;
      }
    }
    if (_isBuiltQ && !_isBuiltR) {
      std::cerr << "Error in QRF::makeR data R has been lost because Q has been "
                   "assembled before"
                << std::endl;
      return -2;
    }
    if (!_isBuiltR) {
      // buildR(Q, R);
      this->buildR(_R);
    }
    return 0;
  }

  MatrixWrapper *getR() {
    if (!_isBuiltR) {
      this->makeR();
    }
    return &_R;
  }

  /**
   * @brief multiplyQ will perform Q*C, Q*C^t, C*Q or C^t*Q
   *
   * Factorize needs to have been called before the method.
   * Q is not rebuilt, the multiplication is made from the householder form of
   * Q
   *
   *
   * @param side "L" (by default) or "R" to specify wether C must be on the
   * left or right of Q
   * @param trans "Y" or "N" to specify if C must be transposed
   * @param[out] C : Will be overwritten by the product
   * @return 0 On success, a negative value otherwise.
   */
  int multiplyQ(const char *trans, MatrixWrapper &C, const char *side = "L") {
    if (!_isFactorized) {
      std::cerr << "Error in QRF::multiplyQ matrix not factorized" << std::endl;
      return -1;
    }
    // TODO: handle this case with fmr::gemm inside this class
    if (_isBuiltQ) {
      std::cerr << "Error in QRF::multiplyQ matrix Q has been assembled, "
                   "please use Q to perform the multiplication yourself"
                << std::endl;
      return -1;
    }
    return this->multiplyQ(trans, C, _TS, side);
  }

  ///
  ///
  /// \brief build the matrix Q
  ///
  /// The routine generates the orthogonal matrix Q of the QR factorization
  /// formed by method factorize()
  ///
  /// @param[in] Q the orthogonal matrix used only to use the good method
  ///
  int buildQ(BlasDenseWrapper<int_type, value_type> &) {
    //    std::clog << "_Q " << &_Q << std::endl;
    //    std::clog << "_TS " << _TS << std::endl;
    //    std::clog << "_TT " << _TT << std::endl;
    const int_type nbRows = _Q.getNbRows();
    const int_type nbCols = _Q.getNbCols();
    //
    auto *q = _Q.getMatrix();
    // Blas::copy(nbRows*nbCols, qin, qout);
    // std::clog << "_TS" << _TS << std::endl;
    int_type c = std::min(nbRows, nbCols);
    int_type lwork = -1;
    value_type wwork;
    int_type INFO;
    // First call is to compute lork
    INFO = Blas::orgqr(int(nbRows), int(nbCols), c, q, _TS, lwork, &wwork);
    lwork = wwork + 0.1;
    value_type *work = new value_type[lwork];
    //    std::clog << "blas::orgqr   Q shape " << _Q.getNbRows() << " "
    //              << _Q.getNbCols() << " tmpshape " << lwork << "\n";
    INFO = Blas::orgqr(int(nbRows), int(nbCols), c, q, _TS, int(lwork), work);
    delete[] work;
    if (INFO != 0) {
      std::stringstream stream;
      stream << INFO;
      std::cerr << "Error in QRF::Blas::orgqr failed! INFO=" + stream.str() << std::endl;
      return -1;
    }
    _isBuiltQ = true;
    return 0;
  }

  int buildR(BlasDenseWrapper<int_type, value_type> &) {
    _R.init(0.);
    for (int_type j(0); j < _Q.getNbCols(); ++j) {
      for (int_type i(0); i <= std::min(j, _Q.getNbRows() - 1); ++i) {
        _R.setVal(i, j, _Q.getVal(i, j));
      }
    }
    _isBuiltR = true;
    return 0;
  }

#if defined(FMR_CHAMELEON)
  // Chameleon Subroutines :
  int buildQ(ChameleonDenseWrapper<int_type, value_type> &) {
    //    std::clog << "_Q " << &_Q << std::endl;
    //    std::clog << "_TS " << _TS << std::endl;
    //    std::clog << "_TT " << _TT << std::endl;
    ChameleonDenseWrapper<int_type, value_type> tmp(_Q.getNbRows(),
                                                    _Q.getNbCols());
    //    std::clog << " step Chameleon::orgqr\n";
    //    std::clog << "Chameleon::orgqr   Q shape " << _Q.getNbRows() << " "
    //              << _Q.getNbCols() << " tmpshape " << tmp.getNbRows() << "
    //              "
    //              << tmp.getNbCols() << "\n";

    int err = Chameleon::orgqr(_QRTREE, _Q.getMatrix(), _TS, _TT, tmp.getMatrix());
    if ( err != 0 ){
      std::cerr << "Error in QRF::Chameleon::orgqr failed! INFO=" << err << std::endl;
      return -1;
    }

    // std::clog << " step Chameleon::lacpy\n";
    err = Chameleon::lacpy(ChamUpperLower, tmp.getMatrix(), _Q.getMatrix());
    if ( err != 0 ){
      std::cerr << "Error in QRF::Chameleon::lacpy failed! INFO=" << err << std::endl;
      return -1;
    }

    _isBuiltQ = true;

    //    std::clog << " end  buildQ(ChameleonDenseWrapper\n";
    return 0;
  }

  int buildR(ChameleonDenseWrapper<int_type, value_type> &) {
    _R.init(0.);
    int err = Chameleon::lacpy(ChamUpper, _Q.getMatrix(), _R.getMatrix());
    if ( err != 0 ){
      std::cerr << "Error in QRF::Chameleon::lacpy failed! INFO=" << err << std::endl;
      return -1;
    }
    _isBuiltR = true;
    return 0;
  }

#endif

private:
  ///
  /// \brief computeQRD_lapack the lapacak implementation of the QR
  /// facorization \param size number of rows \param rank number of column
  /// \param pQ a pointer on the elemnts to foactorize
  /// \param tau contains elementary reflectors for the matrix Q
  ///
  /// \todo Write the documentation
  int computeQRD_lapack(/*const int_type size, const int_type rank,
                         value_type *&pQ, value_type *&tau*/) {

    // init QRD
    auto size = _Q.getNbRows();
    auto rank = _Q.getNbCols();
    auto pQ = _Q.getMatrix();
    int_type INFO;
    int_type LWORK = -1;
    const int_type nbRowsR = std::min(size, rank);
    value_type wwork;
    if (_TS == nullptr) {
      _TS = new value_type[nbRowsR];
    }
    // QRD of Y  : QOUT TAU   Q ( (I - V TAU VT)  QOUT = V (HousHolder vector)
    // std::clog << "Blas::geqrf \n";

    Blas::geqrf(int_type(size), int_type(rank), pQ, _TS, LWORK, &wwork);
    LWORK = wwork + 0.1;
    value_type *WORK = new value_type[LWORK];
    //    std::clog << "Blas::geqrf 2\n";
    //    std::clog << "   tau" << _TS << " pQ " << pQ << std::endl;

    INFO = Blas::geqrf(int_type(size), int_type(rank), pQ, _TS, int_type(LWORK), WORK);
    delete[] WORK;
    if (INFO != 0) {
      std::stringstream stream;
      stream << INFO;
      std::cerr << "Error in QRF::Blas::geqrf failed! INFO=" + stream.str() << std::endl;
      return -1;
    }
    return 0;
  }

  /// \brief computeQRD specialization of the QR factorizaton method for
  /// Lapack wrapper
  int computeQRD(BlasDenseWrapper<int_type, value_type> &) {

    return this->computeQRD_lapack(/*_Q.getNbRows(), _Q.getNbCols(), _Q.getMatrix(),
                                   _TS*/);
  }
#if defined(FMR_CHAMELEON)
  // Chameleon Subroutines :
  /// \brief computeQRD specialization of the QR factorizaton method for
  /// Chameleon wrapper
  int computeQRD(ChameleonDenseWrapper<int_type, value_type> &) {
    if (_TS == nullptr) {
      Chameleon::gels_alloc_workspace(_Q.getNbRows(), _Q.getNbCols(), &_TS,
                                      _Q.getDataType(), _Q.get2DBlockCyclicP(),
                                      _Q.get2DBlockCyclicQ());
    }
    if (_TT == nullptr) {
      Chameleon::gels_alloc_workspace(_Q.getNbRows(), _Q.getNbCols(), &_TT,
                                      _Q.getDataType(), _Q.get2DBlockCyclicP(),
                                      _Q.get2DBlockCyclicQ());
    }
    if (_QRTREE == nullptr) {
      _QRTREE = (libhqr_tree_t *)calloc(1, sizeof(libhqr_tree_t));
      /* Initialize matrix */
      libhqr_matrix_t matrix;
      matrix.mt = _TS->mt;
      matrix.nt = _TS->nt;
      matrix.nodes = _Q.get2DBlockCyclicP() * _Q.get2DBlockCyclicQ();
      matrix.p = _Q.get2DBlockCyclicP();
      /* Initialize QRTREE */
      libhqr_init_hqr(
          _QRTREE, (_Q.getNbRows() >= _Q.getNbCols()) ? LIBHQR_QR : LIBHQR_LQ,
          &matrix, -1, -1, -1, -1, -1, 0);
    }

    int err = Chameleon::geqrf_param(_QRTREE, _Q.getMatrix(), _TS, _TT);
    if ( err != 0 ){
      std::cerr << "Error in QRF::Chameleon::geqrf_param failed! INFO=" << err << std::endl;
      return -1;
    }
    return 0;
  }
  ///
  /// \brief multiplyQ specialization of the multiplyQ method for Chameleon
  /// wrapper
  int multiplyQ(const char *trans,
                ChameleonDenseWrapper<int_type, value_type> &C,
                CHAM_desc_t *_TS, const char *side = "L") {
//    std::clog << "multiplyQ " << trans << " " << side << "  Q shape "
//              << _Q.getNbRows() << " " << _Q.getNbCols() << " C shape "
//              << C.getNbRows() << " " << C.getNbCols() << "\n";
    cham_side_t cSide;
    cham_trans_t cTrans;
    if (!strcmp(side, "L")) {
      cSide = ChamLeft;
    } else if (!strcmp(side, "R")) {
      cSide = ChamRight;
    } else {
      std::cerr << "multiplyQ : " << side
                << " is not a valid parameter for side" << std::endl;
      return -1;
    }

    if (!strcmp(trans, "Y")) {
      cTrans = ChamTrans;
    } else if (!strcmp(trans, "N")) {
      cTrans = ChamNoTrans;
    } else {
      std::cerr << "multiplyQ : " << trans
                << " is not a valid parameter for trans" << std::endl;
      return -2;
    }

    Chameleon::ormqr(_QRTREE, cSide, cTrans, _Q.getMatrix(), _TS, _TT,
                     C.getMatrix());

    /* reshape C matrix if necessary */
    int_type new_rows = (cTrans == ChamNoTrans) ? _Q.getNbRows() : _Q.getNbCols();
    int_type new_cols = (cTrans == ChamNoTrans) ? _Q.getNbCols() : _Q.getNbRows();
    if ( cSide == ChamLeft && new_rows != C.getNbRows()){
        C.init(new_rows, C.getNbCols(), C.getMatrix());
    }
    if ( cSide == ChamRight && new_cols != C.getNbCols()){
        C.init(C.getNbRows(), new_cols, C.getMatrix());
    }

    return 0;
  }
#endif
  ///
  /// \brief multiplyQ specialization de la méthode for lapack
  ///
  int multiplyQ(const char *trans, BlasDenseWrapper<int_type, value_type> &C,
                TDesc TS, const char *side = "L") {
    if (strcmp(side, "L") != 0 && strcmp(side, "R") != 0) {
      std::cerr << "Error in QRF::multiplyQ wrong value for side should b L or R and not " + std::string(side) << std::endl;
      return -1;
    }
//    std::clog << "multiplyQ " << trans << " " << side << "  Q shape "
//              << _Q.getNbRows() << " " << _Q.getNbCols() << " C shape "
//              << C.getNbRows() << " " << C.getNbCols() << "\n";
    int INFO;
    int_type nbRows = C.getNbRows();
    int_type nbCols = C.getNbCols();
    int_type nbReflectors = std::min(_Q.getNbRows(), _Q.getNbCols());
    int lwork = -1;
    value_type wwork;
    value_type *work = nullptr;
    const std::string s_side(side);
    const std::string s_trans(trans);
    if (!strcmp(side, "L")) {
      // Perform C := _Q^trans C

      INFO = Blas::left_ormqr(trans, nbRows, nbCols, nbReflectors,
                              _Q.getMatrix(), TS, C.getMatrix(), lwork, &wwork);
      if (INFO != 0) {
        std::stringstream stream;
        stream << INFO;
        std::cerr << "Error in QRF::multiplyQ::Blas::left_ormqr failed! INFO=" + stream.str() << std::endl;
        return -1;
      }
      lwork = wwork + 0.1;
      work = new value_type[lwork];
      INFO = Blas::left_ormqr(trans, nbRows, nbCols, nbReflectors,
                              _Q.getMatrix(), TS, C.getMatrix(), lwork, work);
      delete[] work;
      if (INFO != 0) {
        std::stringstream stream;
        stream << INFO;
        std::cerr << "Error in QRF::multiplyQ::Blas::left_ormqr failed! INFO=" + stream.str() << std::endl;
        return -1;
      }
      int_type new_rows = s_trans == "N" ? _Q.getNbRows() : _Q.getNbCols();
      C.init(new_rows, C.getNbCols(), C.getMatrix());
    }
    if (!strcmp(side, "R")) {
      // Perform C := C _Q^trans
      // FIXME : need to implement nbReflectors in right
      // ormqr too
      Blas::right_ormqr(trans, nbRows, nbCols, nbReflectors, _Q.getMatrix(), TS,
                        C.getMatrix(), lwork, &wwork);
      lwork = wwork + 0.1;
      work = new value_type[lwork];
      INFO = Blas::right_ormqr(trans, nbRows, nbCols, nbReflectors,
                               _Q.getMatrix(), TS, C.getMatrix(), lwork, work);
      delete[] work;
      int_type new_cols = s_trans == "N" ? _Q.getNbCols() : _Q.getNbRows();
      C.init(C.getNbRows(), new_cols, C.getMatrix());
      if (INFO != 0) {
        std::stringstream stream;
        stream << INFO;
        std::cerr << "Error in QRF::multiplyQ::Blas::right_ormqr failed! INFO=" + stream.str() << std::endl;
        return -1;
      }
    }
    return 0;
  }
  void freeTauDesc(TDesc &tau) {
    // std::clog << "    beg QRF::freeTauDesc " << tau << "\n";
    if (tau != nullptr) {
      if constexpr (traits::is_blas_wrapper<MatrixWrapper>::value) {
        delete[] tau; // freeTauDesc_blas(tau);
      } else if constexpr (traits::is_chameleon_wrapper<MatrixWrapper>::value) {
#if defined(FMR_CHAMELEON)
        // std::clog << "    Chameleon delete " << tau << "\n";

        Chameleon::dealloc_workspace(&tau);
#endif
      }
      tau = nullptr;
    }
    //  std::clog << "    end QRF::freeTauDesc " << tau << "\n";
  }
  void freeTreeDesc() {
    // std::clog << "    beg QRF::freeTreeDesc " << _QRTREE << "\n";

    if (_QRTREE != nullptr) {
      if constexpr (traits::is_blas_wrapper<MatrixWrapper>::value) {
        delete[] _QRTREE;
      } else if constexpr (traits::is_chameleon_wrapper<MatrixWrapper>::value) {
#if defined(FMR_CHAMELEON)
        libhqr_finalize(_QRTREE);
#endif
        std::free(_QRTREE);
      }
      _QRTREE = nullptr;
    }
    // std::clog << "    end QRF::freeTreeDesc " << _QRTREE << "\n";
  }

public:
  void free() {
    //    std::clog << "QRF::free() call on this(QRF) " << this
    //              << " &_Q(MatrxWrapper) " << &_Q << std::endl;
    _R.free();
    this->freeTauDesc(_TS);
    this->freeTauDesc(_TT);
    this->freeTreeDesc();
  }

  ~QRF() { this->free(); }
};

#define FMULS_GEQRF(__m, __n)                                                  \
  (((__m) > (__n))                                                             \
       ? ((double)(__n) *                                                      \
          ((double)(__n) * (0.5 - (1. / 3.) * (double)(__n) + (double)(__m)) + \
           (double)(__m) + 29. / 6.))                                          \
       : ((double)(__m) * ((double)(__m) * (-0.5 - (1. / 3.) * (double)(__m) + \
                                            (double)(__n)) +                   \
                           2. * (double)(__n) + 29. / 6.)))
#define FADDS_GEQRF(__m, __n)                                                  \
  (((__m) > (__n))                                                             \
       ? ((double)(__n) * ((double)(__n) * (-0.5 - (1. / 3.) * (double)(__n) + \
                                            (double)(__m)) +                   \
                           (double)(__m) + 5. / 6.))                           \
       : ((double)(__m) *                                                      \
          ((double)(__m) * (0.5 - (1. / 3.) * (double)(__m) + (double)(__n)) + \
           +5. / 6.)))
#define FLOPS_GEQRF(__m, __n) (FMULS_GEQRF(__m, __n) + FADDS_GEQRF(__m, __n))

} // namespace fmr

#endif // QRF_HPP
