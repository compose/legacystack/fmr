# Install dependencies

## Arpack-ng

Here we force the use of the embedded `arpack-ng` package, that is well maintained and provides nice cmake capabilities. However, you can use your own `arpack` implementation by changing arpack root directory in  `../CMakeLists.txt`. Here we provide the command lines to use for installing `arpack-ng`, it basically follows the instruction provided in `arpack-ng/README`:


* Extract `arpack-ng-master` and rename package to `arpack-ng`

```sh
cd Packages/
unzip arpack-ng-master.zip
mv arpack-ng-master arpack-ng
```

* Move to arpack directory and configure

```sh
cd arpack-ng/
sh bootstrap
./configure
```

* Create `build/` dir

```sh
mkdir build
cd build
```

* Generate makefile and shared libs (libarpack.so)

```sh
cmake ../ -DBUILD_SHARED_LIBS=ON
make 
make install
```

* That's it! You can now build all your executables that depend on arpack!


## Scotch

Some methods implemented in FMR (in particular thoses related to the distance matrices used in MDS) rely on graph partitioning through [scotch](http://www.labri.fr/perso/pelegrin/scotch/) library. Please install scotch by one of the following techniques

* on your local computer

```sh
sudo apt-get install libscotch-5.1 libscotch-dev
# then SCOTCH_HOME=/usr/???
```

* on plafrim you can either load scotch from modules ...

```sh
# Load from modules
module add partitioning/scotch/int64/6.0.4
```

* ... or install manually

```sh
cd src/
cp Make.inc/Makefile.inc.x86-64_pc_linux2 Makefile.inc
make scotch
# install scotch in /usr/local/
make install
# install scotch in specified directory
make install --prefix=/path/to/libscotch
```


In any case, don't forget to set the environment variables as follows

```sh
export SCOTCH_HOME=/path/to/scotch_6.0.4
export SCOTCH_LIB=$SCOTCH_HOME/lib
export SCOTCH_INC=$SCOTCH_HOME/include
```


## ScalFMM

__TODO__ Provide guidelines for cloning from gitlab ...

### Why use ScalFMM?

__TODO__

### How?

__TODO__ Update procedure for new release of scalfmm

* Move to Packages directory:

    ```sh
    cd Packages
    ```

* Get ScalFMM from tarball:
    *  using dev package

        ```sh
        tar -zxvf ScalFMM-1.4-dev.tar.gz
        ```

    * or last release

        ```sh
        tar -zxvf SCALFMM-1.4-148.tar.gz
        ```

* Rename directory

    ```sh
    mv SCALFMM-1.4-148 scalfmm
    ```

* Move to newly created scalfmm directory, create then move to Build directory:

    ```sh
    cd scalfmm
    mkdir Build
    cd Build
    ```

* Configure ScalFMM, i.e., set ScalFMM's compile options and define env variables:
    * (MANDATORY) Set the path for install. of ScalFMM to /path/to/fmr/Packages/scalfmm. 
    * (MANDATORY) Set Blas option to ON.
    * (MANDATORY) Set SSE option to OFF.
    * (CONVENIENT) EXAMPLES option to OFF in order to avoid time consuming compilations.

        ```sh
        cmake ../ -DSCALFMM_USE_BLAS=ON -DSCALFMM_USE_FFT=ON -DSCALFMM_USE_SSE=OFF -DSCALFMM_BUILD_EXAMPLES=OFF -DSCALFMM_USE_ADDONS=ON -DSCALFMM_ADDON_HMAT=ON -DCMAKE_INSTALL_PREFIX=/path/to/fmr/Packages/scalfmm
        ```

    * if MKL is used as BLAS library

        ```sh
         cmake ../ -DSCALFMM_USE_BLAS=ON -DSCALFMM_USE_MKL_AS_BLAS=ON -DSCALFMM_USE_FFT=ON -DSCALFMM_USE_MKL_AS_FFTW=ON -DSCALFMM_USE_SSE=OFF -DSCALFMM_BUILD_EXAMPLES=OFF  -DSCALFMM_USE_ADDONS=ON -DSCALFMM_ADDON_HMAT=ON -DCMAKE_INSTALL_PREFIX=/path/to/fmr/Packages/scalfmm
        ```

    * another example with same options but pierre's specific install directory

        ```sh
         cmake ../ -DSCALFMM_USE_BLAS=ON -DSCALFMM_USE_MKL_AS_BLAS=ON -DSCALFMM_USE_FFT=ON -DSCALFMM_USE_MKL_AS_FFTW=ON -DSCALFMM_USE_SSE=OFF -DSCALFMM_BUILD_EXAMPLES=OFF  -DSCALFMM_USE_ADDONS=ON -DSCALFMM_ADDON_HMAT=ON -DCMAKE_INSTALL_PREFIX=/home/pierre/Code/fmr/Packages/scalfmm
        ```

    * You can also do this using cmake GUI

        ```sh
        ccmake ../
        ```